<?php
/**
 * Template Name: Nowosci 
 *
 * @package WordPress
 * 
 * 
 */

get_header(); ?>
<?php get_header(); ?>
<div>
    <main role="main">
        <!-- section -->

        <div style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/green-background.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <section class="container" style="min-height:60vh;margin-top:30px">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class('col-12'); ?>>
                <div class="recommendedProducts container">

                    <div class="wrap-products">
                        <?php
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 12,
        'orderby'=>"date" ,
        'order'=>"desc",
      );

  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    wc_get_template_part( 'content', 'recommendProductsBottomFront' );
    endwhile;
  } else {
    echo __( 'No products found' );
  }
  wp_reset_postdata();
  ?>
                    </div>
                </div>

            </article>
            <!-- /article -->

            <?php endwhile; ?>

            <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

            </article>
            <!-- /article -->

            <?php endif; ?>

        </section>
        <!-- /section -->
    </main>
</div>


<?php get_footer(); ?>