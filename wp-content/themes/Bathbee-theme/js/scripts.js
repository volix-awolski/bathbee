jQuery(document).ready(function() {
  if (window.innerWidth < 1000) {
    const menuChange = () => {
      const menu = document.getElementById("primary-menu");
      const koszyk = document.getElementById("koszyk");
      const loginRegisterLink = document.querySelector(
        ".featured-text .login a"
      );
      const li = document.createElement("li");
      const menuGlowne = document.getElementById("menu-menu-glowne");
      jQuery(li).append(loginRegisterLink);
      menuGlowne.appendChild(li);
      jQuery(koszyk).insertBefore(menu);
    };
    menuChange();
  }

  const infoPrefPrice = () => {
    const products = document.getElementsByClassName("productTwoPrices");
    [...products].forEach(product => {
      const preferencyjna = product.querySelector(".preferencyjna");

      if (preferencyjna) {
        const priceWrapper = product.querySelector(".price");
        const p = document.createElement("p");
        p.classList.add("info-pref-price");
        p.innerText =
          "*Cena preferencyjna tylko dla zarejestrowanych klientów z kuponem rabatowym";
        priceWrapper.parentNode.insertBefore(p, priceWrapper.nextSibling);

        const promo = product.querySelector("ins");
        if (promo) {
          const promo2 = product.querySelector("del");
          promo2.classList.add("wrapp-promotion");
          promo.classList.add("wrapp-promotion");

          jQuery(".wrapp-promotion").wrapAll(
            '<div class="wrapper-promotion"></div>'
          );
        }
      }
    });
  };
  infoPrefPrice();

  const infoPrefPriceSingleProduct = () => {
    const product = document.querySelector(".summary.entry-summary");
    if (product) {
      const preferencyjna = product.querySelector(".preferencyjna");

      if (preferencyjna) {
        const priceWrapper = product.querySelector(".price");
        const p = document.createElement("p");
        p.classList.add("info-pref-price-single-product");
        p.innerText =
          "*Cena preferencyjna tylko dla zarejestrowanych klientów z kuponem rabatowym";
        priceWrapper.parentNode.insertBefore(p, priceWrapper.nextSibling);
      }
    }
  };

  infoPrefPriceSingleProduct();
});

$(document).ready(function() {
  "use strict";

  var products = function() {
    var self = this;

    self.class = {
      item: "js-item",
      box: "js-box",
      close: "home-components__box__close"
    };

    self.dom = $(".home-components");

    self.el = {
      item: self.dom.find("." + self.class.item),
      box: self.dom.find("." + self.class.box),
      close: self.dom.find("." + self.class.close),
      modal: self.dom.find("." + self.class.modal),
      modal__close: self.dom.find("." + self.class.modal__close)
    };

    self.Init();
  };

  products.prototype.Init = function() {
    var self = this;

    self.el.item.on("click", function() {
      var that = $(this);

      self.el.item.removeClass("is-flipped");
      $(this).addClass("is-flipped");

      self.el.box.removeClass("active");
      if ($(window).width() < 767) {
        $("#box_custom36").css("z-index", "9999999999");
      }
      self.el.box.each(function() {
        if ($(this).data("type") == that.data("type")) {
          if ($(window).width() < 767) {
            $("body").addClass("no-scroll");
          }
          $(this).addClass("active");
        }
      });
    });

    self.el.close.on("click", function() {
      self.el.box.removeClass("active");
      if ($(window).width() < 767) {
        $("#box_custom36").css("z-index", "1");
        $("body").removeClass("no-scroll");
      }
    });
  };

  var Products = new products();
});

const loginRegisterSwitch = () => {
  const loginButton = document.querySelector('a[href="#tab_login"]');
  const registerButton = document.querySelector('a[href="#tab_register"]');

  const switchTabs = type => {
    if (type === "login") {
      loginButton.classList.add("active");
      registerButton.classList.remove("active");
    } else {
      loginButton.classList.remove("active");
      registerButton.classList.add("active");
    }
  };

  if (loginButton && registerButton) {
    loginButton.classList.add("active");
    loginButton.addEventListener("click", e => switchTabs("login"));
    registerButton.addEventListener("click", e => switchTabs("register"));
  }
};

const dataAttPreferCart = () => {
  const preferLaber = document.querySelector("td.prefer-price");
  if (preferLaber) {
    preferLaber.setAttribute("data-title", "Cena Preferencyjna");
  }
};

const selectAll = () => {
  const labelSelect = document.getElementById("zaznacz_wszystko");

  if (labelSelect) {
    const parent = labelSelect.parentElement;
    parent.classList.add("zaznacz-wszystko");
  }
};

jQuery(document).ready(function() {
  loginRegisterSwitch();
  dataAttPreferCart();
  selectAll();
});
