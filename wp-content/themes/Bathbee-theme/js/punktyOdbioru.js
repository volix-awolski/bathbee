let openByID;

const offsetCenter = function(dx, dy) {
  return {
    lat: dx,
    lng: dy
  };
};

const renderMap = placements => {
  jQuery(function() {
    var center = {
      lat: 52,
      lng: 18.8867698
    };

    var center;
    if (window.innerWidth > 1000) {
      center = {
        lat: 52,
        lng: 14.6
      };
    } else {
      center = {
        lat: 52,
        lng: 18.8867698
      };
    }

    var mapStyle = [
      {
        featureType: "all",
        elementType: "labels.text",
        stylers: [{ visibility: "on" }]
      },
      {
        featureType: "all",
        elementType: "labels.text.fill",
        stylers: [
          { saturation: 36 },
          { color: "#333333" },
          { lightness: 40 },
          { visibility: "on" }
        ]
      },
      {
        featureType: "all",
        elementType: "labels.text.stroke",
        stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: 16 }]
      },
      {
        featureType: "administrative",
        elementType: "geometry.fill",
        stylers: [{ color: "#808080" }, { lightness: 20 }]
      },
      {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [{ color: "black" }, { lightness: 17 }, { weight: 1.2 }]
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [{ color: "#f5f5f5" }, { lightness: 20 }]
      },
      {
        featureType: "poi",
        elementType: "geometry",
        stylers: [{ color: "#f5f5f5" }, { lightness: 21 }]
      },
      {
        featureType: "poi.park",
        elementType: "geometry",
        stylers: [{ color: "#dedede" }, { lightness: 21 }]
      },
      {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{ color: "#ffffff" }, { lightness: 17 }]
      },
      {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{ color: "#ffffff" }, { lightness: 29 }, { weight: 0.2 }]
      },
      {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [{ color: "#ffffff" }, { lightness: 18 }]
      },
      {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [{ color: "#ffffff" }, { lightness: 16 }]
      },
      {
        featureType: "transit",
        elementType: "geometry",
        stylers: [{ color: "#f2f2f2" }, { lightness: 19 }]
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [{ color: "#46BCEC" }, { lightness: 17 }]
      }
    ];

    let zoom = 6.3;
    if (window.innerWidth < 1000) {
      zoom = 5.5;
    }

    var map = new google.maps.Map($(".map-canvas")[0], {
      zoom: zoom,
      center: center,
      styles: mapStyle
    });

    openByID = id => {
      for (let index = 0; index < placements.length; index++) {
        const e = placements[index];

        // placements.forEach(e => {
        var markerIcon = {
          path:
            "M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z",
          fillColor: "#84be38",
          fillOpacity: 0.95,
          scale: 1.5,
          strokeColor: "#fff",
          strokeWeight: 2,
          anchor: new google.maps.Point(12, 24)
        };

        var marker = new google.maps.Marker({
          map: map,
          draggable: false,
          position: e.LatLng,
          icon: markerIcon
        });
        var info = new SnazzyInfoWindow(
          $.extend(
            {},
            {
              marker: marker,
              placement: e.type,
              content: e.type,
              panOnOpen: false,
              closeWhenOthersOpen: true
            }
          )
        );
        if (index === id) info.open();
      }
    };

    openByID();
  });
};

const placements = points => {
  const readyPoints = [];

  const filters = {
    wojewodztwa: [],
    miasta: [],
    typy: []
  };

  let id = 0;
  for (const [key, value] of Object.entries(points)) {
    points[key].forEach(place => {
      filters.wojewodztwa.push(key);
      filters.miasta.push(place.address.replace(/ .*/, ""));
      filters.typy.push(place.type);

      let img = "";
      if (place.type === "Punkt Odbioru Zamówień internetowych") {
        img = window.RolesImg.punktOdbioruZamowieńinternetowych;
      }

      if (place.type === "Stacjonarna Sprzedaż Produktów") {
        img = window.RolesImg.stacjonarnaSprzedazProduktow;
      }

      if (place.type === "Dystrybutor Sprzedaży Bezpośredniej") {
        img = window.RolesImg.dystrybutorSprzedazyBezposredniej;
      }

      if (place.type === "Mobilny Punkt Odbioru") {
        img = window.RolesImg.mobilnyPunktOdbioru;
      }

      if (place.type === "Usługi na produktach Bathbee") {
        img = window.RolesImg.usługiNaProduktachBathbee;
      }

      if (
        place.type ===
        "Punkt Akceptujący Karty Bathbee Cashback i Karty sieci Cashback World"
      ) {
        img = window.RolesImg.kartyBathbee;
      }

      if (place.type) img = window.RolesImg.usługiNaProduktachBathbee;

      const linkMap = `https://www.google.com/maps/dir/@${Number(
        place.lat
      )},${Number(
        place.len
      )},12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x471644c0354e18d1:0xb46bb6b576478abf!2m2!1d19.9449799!2d50.0646501`;

      const placement = {
        type: `<div class="info-point">
        <p class="name"><img class="ikona-punkt" title="${place.type}" src="${img}"/> <span>${place.name}</span></p> 
        <p class="adress"><a title="Zobacz dojazd" target="_blank" href="${linkMap}"><span>Adres:</span> ${place.address}</a> </p>, 
        <p class="type"><span>Typ:</span> ${place.type}</p>
    
        </div>`,
        LatLng: { lat: Number(place.lat), lng: Number(place.len) },
        typ: place.type,
        wojewodztwo: key,
        miasto: place.address.replace(/ .*/, ""),
        id: id
      };
      ++id;
      readyPoints.push(placement);
    });
  }

  filters.wojewodztwa = [...new Set(filters.wojewodztwa)];
  filters.miasta = [...new Set(filters.miasta)];
  filters.typy = [...new Set(filters.typy)];

  return { readyPoints, filters };
};

const renderFilter_wojewodztwa = names => {
  const div = document.querySelector(".filters .wojewodztwa select");
  names.forEach(name => {
    const option = document.createElement("option");
    option.className = "filter";
    option.innerText = name;
    option.value = name;
    if (window.Filter.wojewodztwo === name) {
      option.selected = true;
    }

    return div.appendChild(option);
  });
};

const renderFilter_miasta = names => {
  const div = document.querySelector(".filters .miasta select");
  names.forEach(name => {
    const option = document.createElement("option");
    option.className = "filter";
    option.innerText = name;
    option.value = name;
    if (window.Filter.miasto === name) {
      option.selected = true;
    }

    return div.appendChild(option);
  });
};

const renderFilter_typy = names => {
  const div = document.querySelector(".filters .typy select");
  names.forEach(name => {
    const option = document.createElement("option");
    option.className = "filter";
    option.innerText = name;
    option.value = name;
    if (window.Filter.typ === name) {
      option.selected = true;
    }

    return div.appendChild(option);
  });
};

const initStateFilter = () => {
  window.Filter = {
    wojewodztwo: "all",
    miasto: "all",
    typ: "all"
  };
  window.filterBy = [];
};

const filterData = (data, keys, values) => {
  return data.filter(function(e) {
    return keys.every(function(a) {
      return values.includes(e[a]);
    });
  });
};

//render result list poczatek

const renderResult = data => {
  let wojewodztwa = [];
  data.forEach(el => wojewodztwa.push(el.wojewodztwo));
  wojewodztwa = [...new Set(wojewodztwa)];

  resultsWrapper = document.querySelector(".results-wrapper");
  resultsWrapper.innerHTML = "";

  wojewodztwa.forEach(wojewodztwo => {
    const div = document.createElement("div"),
      p = document.createElement("p"),
      ul = document.createElement("ul");

    p.className = "wojewodztwo-nazwa";
    p.innerText = wojewodztwo;
    div.className = "col-12 p-0";
    div.appendChild(p);
    ul.className = "punkty-lista";

    const list = data.filter(elem => elem.wojewodztwo === wojewodztwo);

    list.forEach(place => {
      const div = document.createElement("div");
      div.className = "punkt-lista";
      div.innerHTML = place.type;

      const button = document.createElement("button");
      button.className = "zobacznamapie";
      button.setAttribute("onclick", `openByID(${place.id})`);
      button.innerText = "Zobacz na mapie";
      div.appendChild(button);

      ul.appendChild(div);
    });

    div.appendChild(ul);
    resultsWrapper.appendChild(div);
  });
};

//render result list koniec

const handleFilter = () => {
  const selects = document.querySelectorAll(".filters select");
  selects.forEach(select => {
    select.addEventListener("change", e => {
      window.Filter[e.target.parentElement.dataset.filter] = e.target.value;
      if (e.target.value !== "all") {
        window.filterBy.push(e.target.parentElement.dataset.filter);
        window.filterBy = [...new Set(window.filterBy)];
      } else {
        window.filterBy = window.filterBy.filter(
          filter => filter !== e.target.parentElement.dataset.filter
        );
      }

      filterData(placementsExamples, window.filterBy, [
        window.Filter.wojewodztwo,
        window.Filter.miasto,
        window.Filter.typ
      ]);

      renderMap(
        filterData(placementsExamples, window.filterBy, [
          window.Filter.wojewodztwo,
          window.Filter.miasto,
          window.Filter.typ
        ])
      );
      renderResult(
        filterData(placementsExamples, window.filterBy, [
          window.Filter.wojewodztwo,
          window.Filter.miasto,
          window.Filter.typ
        ])
      );
    });
  });
};

const styleSelect = () => {
  const allSelects = document.querySelectorAll(".filters select option");
  allSelects.forEach(select => {
    select.innerText =
      select.innerText.charAt(0).toUpperCase() +
      select.innerText.slice(1).toLowerCase();
  });
};

const punktyOdbioru = () => {
  jQuery(document).ready(() => {
    const points = window.points;
    const places = placements(points);
    initStateFilter();
    // renderMap(places.readyPoints);
    renderMap(placementsExamples);
    renderFilter_wojewodztwa(places.filters.wojewodztwa);
    renderFilter_miasta(places.filters.miasta);
    renderFilter_typy(places.filters.typy);
    handleFilter();
    // renderResult(places.readyPoints);
    renderResult(placementsExamples);
    styleSelect();
  });
};

punktyOdbioru();

const placementsExamples = [
  {
    LatLng: { lat: 51.961389, lng: 15.138333 },
    type: `<div class='info-point'> <p class='name'><img class='ikona-punkt' title='punkt odbioru zamowień internetowych' src='${window.RolesImg.punktOdbioruZamowieninternetowych}'/> <span>Gabinet kosmetyczny Barbara Głąb test dluga nzwa</span></p><p class='adress'><a target='_blank' title='Zobacz dojazd' href='https://www.google.com/maps/dir/@49.961389,12.138333,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x471644c0354e18d1:0xb46bb6b576478abf!2m2!1d19.9449799!2d50.0646501'><span>Adres:</span> Katowice testowa 1/123</a></p><p class='type'><span>Typ:</span> punkt odbioru zamowień internetowych</p></div>`,
    typ: "punkt odbioru zamowień internetowych",
    wojewodztwo: "śląskie",
    miasto: "warszawa",
    id: 0
  },
  {
    LatLng: { lat: 50.561389, lng: 20.238333 },
    type: `<div class='info-point'> <p class='name'><img class='ikona-punkt' title='stacjonarna sprzedaż produktów' src='${window.RolesImg.stacjonarnaSprzedazProduktow}'/> <span> Gabinet kosmetyczny Barbara Głąb</span></p><p class='adress'><a target='_blank' title='Zobacz dojazd' href='https://www.google.com/maps/dir/@49.961389,12.138333,12z/data=!4m8!4m7!1m0!1m5!1m1!1s0x471644c0354e18d1:0xb46bb6b576478abf!2m2!1d19.9449799!2d50.0646501'><span>Adres:</span> Katowice testowa 1/123</a></p><p class='type'><span>Typ:</span> stacjonarna sprzedaż produktów</p></div>`,
    typ: "stacjonarna sprzedaż produktów",
    wojewodztwo: "małopolskie",
    miasto: "mysłowice",
    id: 1
  },
  {
    LatLng: { lat: 50.81389, lng: 19.338333 },
    type: `<div class='info-point'> <p class='name'><img class='ikona-punkt' title='dystrybutor sprzedaży bezpośredniej' src='${window.RolesImg.dystrybutorSprzedazyBezposredniej}'/> <span> Gabinet kosmetyczny Barbara Głąb</span></p><p class='adress'><span>Adres:</span> Katowice testowa 1/123</p><p class='type'><span>Typ:</span> dystrybutor sprzedaży bezpośredniej</p></div>`,
    typ: "dystrybutor sprzedaży bezpośredniej",
    wojewodztwo: "warmińsko-mazurskie",
    miasto: "lidzbark",
    id: 2
  },
  {
    LatLng: { lat: 51.61389, lng: 18.338333 },
    type: `<div class='info-point'> <p class='name'><img class='ikona-punkt' title='mobilny punkt odbioru' src='${window.RolesImg.mobilnyPunktOdbioru}'/> <span> Gabinet kosmetyczny Barbara Głąb</span></p><p class='adress'><span>Adres:</span> Katowice testowa 1/123</p><p class='type'><span>Typ:</span> mobilny punkt odbioru</p></div>`,
    typ: "mobilny punkt odbioru",
    wojewodztwo: "pomorskie",
    miasto: "gdynia",
    id: 3
  }
];
