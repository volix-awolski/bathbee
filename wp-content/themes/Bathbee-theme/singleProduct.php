<?php
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

remove_action('woocommerce_single_product_summary','woocommerce_template_single_title', 5);
add_action('woocommerce_before_single_product_summary','woocommerce_template_single_title', 5);
add_filter( 'woocommerce_product_tabs', 'remove_product_tabs', 98 );
 
function remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}

add_action( 'woocommerce_single_product_summary', 'likeShareProduct', 70);

function likeShareProduct() {
    $link = get_permalink();
    echo '<div class="wrapp-fb-likes"><div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.3&appId=650548902068425&autoLogAppEvents=1"></script>';
    echo '<div class="fb-like" data-href="'.$link.'" data-width="" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div></div>';

}

add_action( 'woocommerce_after_single_product_summary', 'facebookComments', 12);
function facebookComments() {
    $link = get_permalink();
    echo '<div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.3&appId=650548902068425"></script>';
    echo '<div style="width:100%;margin:20px auto" class="fb-comments" data-href="'.$link.'" data-width="100%" data-numposts="5"></div>';
}

add_action( 'woocommerce_single_product_summary', 'my_print_stars', 65);
function my_print_stars(){
    global $wpdb;
    global $post;
    $count = $wpdb->get_var("
    SELECT COUNT(meta_value) FROM $wpdb->commentmeta
    LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
    WHERE meta_key = 'rating'
    AND comment_post_ID = $post->ID
    AND comment_approved = '1'
    AND meta_value > 0
");

$rating = $wpdb->get_var("
    SELECT SUM(meta_value) FROM $wpdb->commentmeta
    LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
    WHERE meta_key = 'rating'
    AND comment_post_ID = $post->ID
    AND comment_approved = '1'
");

if ( $count > 0 ) {

    $average = number_format($rating / $count, 2);

    echo '<div class="starwrapper" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';

    echo '<p><span class="ocena">Ocena:</span> <span class="star-rating" title="'.sprintf(__('Rated %s out of 5', 'woocommerce'), $average).'"><span style="width:'.($average*16).'px"><span itemprop="ratingValue" class="rating">'.$average.'</span> </span></span>';

    echo '</div></p>';
    }

}

// remove_action('woocommerce_before_main_content','wc_print_notices', 10);
// add_action('woocommerce_before_single_product_summary','wc_print_notices', 5);


function spPoints(){
    global $post;
    ?>
<div class="sppoints-wrapper">
    <div class="pointscashback">
        <div class="cashback">
            <span class="numbers"><?=calculateCashback(calculateDiscount($post->ID));?></span> <span
                class="zlotych">zł</span>
            <p>Zwrot cashback</p>

        </div>
        <div class="sp relative">
            <span class="numbers"><?=calculateSP(calculateDiscount($post->ID));?></span> <span
                class="sp-circle">SP</span>
            <p>Punkty SP</p>
        </div>
    </div>
    <img class="cashbackword-img-single-product"
        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/official-cashback-partner-logo-web-cbw_150x102.png"
        alt="">
</div>
<?php
}

add_action( 'woocommerce_single_product_summary', 'spPoints', 70);

add_action( 'woocommerce_single_product_summary', 'imgsGramatura', 75);

function imgsGramatura() {
echo '<div class="imgsGramatura">';
if ( have_rows('obrazki_gramatury_produktu') ) :
while( have_rows('obrazki_gramatury_produktu') ) : the_row();
$img = get_sub_field('obrazek_gramatury');

echo '<img src="'.$img.'" alt="">';

endwhile;
endif;
echo '</div>';
}
?>