<?php get_header(); ?>
<div>
    <main role="main">
        <!-- section -->

        <div style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/green-background.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <section class="container" style="min-height:60vh;margin-top:30px">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class('col-12'); ?>>

                <?php the_content(); ?>

                <?php comments_template( '', true ); // Remove if you don't want comments ?>

                <br class="clear">

                <?php edit_post_link(); ?>

            </article>
            <!-- /article -->

            <?php endwhile; ?>

            <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

            </article>
            <!-- /article -->

            <?php endif; ?>

        </section>
        <!-- /section -->
    </main>
</div>


<?php get_footer(); ?>