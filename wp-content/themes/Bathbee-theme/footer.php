</div>
<!-- /wrapper -->
<div class="footer__wrapper py-5 ">
    <div class="wrapp-menus-footer row container mx-auto text-left">
        <div class="col-md-3 col-sm-12">
            <p class="minimal-heading-bold">Moje konto</p>
            <?php wp_nav_menu( array( 'theme_location' => 'menu_footer_moje_konto' ) ); ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <p class="minimal-heading-bold">Płatności i dostawa</p>
            <?php wp_nav_menu( array( 'theme_location' => 'menu_footer_platnosci' ) ); ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <p class="minimal-heading-bold">Program Cashback</p>
            <?php wp_nav_menu( array( 'theme_location' => 'menu_footer_programcashback' ) ); ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <p class="minimal-heading-bold">Informacje</p>
            <?php wp_nav_menu( array( 'theme_location' => 'menu_footer_informacje' ) ); ?>
        </div>
    </div>

    <img class="cashbackword-img-footer"
        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/official-cashback-partner-logo-web-cbw_150x102.png"
        alt="">
    <div class="footer__copyright text text--tiny">
        © Bathebee All Rights Reserved.
    </div>

</div>

<a href="javascript:" id="return-to-top"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
<!-- /footer -->



<?php wp_footer(); ?>

<script src="https://kit.fontawesome.com/8ca7b7472e.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/slick/slick-theme.css" />


<script src="<?php echo get_template_directory_uri() ?>/jquery.slimmenu.min.js"></script>
<script>
jQuery("#menu-menu-glowne").addClass("slimmenu");
</script>
<script>
jQuery('#menu-menu-glowne').slimmenu({
    resizeWidth: '800',
    collapserTitle: '',
    animSpeed: 'fast',
    easingEffect: null,
    indentChildren: false,
    childrenIndenter: '&nbsp;',
    expandIcon: '<i class="fa fa-sort-desc" aria-hidden="true"></i>',
    collapseIcon: '<i class="fa fa-minus" aria-hidden="true"></i>',
});
</script>

<?php require('popup.php');?>
</body>

</html>