<?php
/**
 * Template Name: punktyOdbioru
 *
 * @package WordPress
 * 
 * 
 */

get_header(); ?>
<?php get_header(); ?>
<?php echo do_shortcode( '[generateGmaps]');?>


<div>
    <main role="main" class="mb-5">
        <!-- section -->

        <div style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/green-background.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <section class="container " style="min-height:60vh;margin-top:30px;position:relative">



            <div class="map-canvas map-relative" style="width:100%;height:650px;margin:20px 0"></div>



            <div class="wrapp-div-on-map">

                <div class="filters col-12 row mx-auto p-0">
                    <div data-filter="wojewodztwo" class="wojewodztwa col-6">
                        <p><b>Województwo</b> </p>
                        <select class="col-12">
                            <option value="all" selected>Wszystkie</option>
                        </select>
                    </div>

                    <div data-filter="miasto" class="miasta col-6">
                        <p><b>Miasto</b> </p>
                        <select class="col-12">
                            <option value="all" selected>Wszystkie</option>
                        </select>
                    </div>

                    <div data-filter="typ" class="typy col-12 mt-2">
                        <p><b>Typ</b> </p>
                        <select class="col-12">
                            <option value="all" selected>Wszystkie</option>
                        </select>
                    </div>
                </div>



                <div id="list-localizations"
                    class="list-localizations row d-flex flex-wrap justify-content-center mb-1 mt-3">
                    <!-- <p class="text-center heading col-12">Lokalizacje</p> -->
                    <div class="results-wrapper  container ">

                    </div>
                </div>

            </div>


        </section>
        <!-- /section -->
    </main>
</div>

<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/maps/snazzy-info-window.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
window.RolesImg = {
    punktOdbioruZamowieninternetowych: '<?php echo home_url()?>/wp-content/uploads/2019/08/Wydawanie-Honey.png ',
    stacjonarnaSprzedazProduktow: '<?php echo home_url()?>/wp-content/uploads/2019/08/Sprzedaz-w-punkcie-Honey.png',
    dystrybutorSprzedazyBezposredniej: '<?php echo home_url()?>/wp-content/uploads/2019/08/Dystrybutor-Honey.png',
    mobilnyPunktOdbioru: '<?php echo home_url()?>/wp-content/uploads/2019/08/Mobilny-Honey.png',
    usługiNaProduktachBathbee: '<?php echo home_url()?>/wp-content/uploads/2019/08/Zabiegi-Honey.png',
    kartyBathbee: '<?php echo home_url()?>/wp-content/uploads/2019/08/cb.png',
}
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD-cqZLizz3ReXy2M1B6ZWbj5NsxVe9BY"></script>
<script src="<?php echo get_template_directory_uri() ?>/maps/snazzy-info-window.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/js/punktyOdbioru.js"></script>
<!-- <script src="<?php echo get_template_directory_uri() ?>/maps/scripts.js"></script>
 -->



<?php get_footer(); ?>