<div class="home-components">
    <div class="container">

        <div class="home-components__wrapper">
            <div class="home-components__view">
                <div class="js-box js-box home-components__box" data-type="default">
                    <div class="js-box home-components__box__wrapper home-components__box__wrapper--center">
                        <div class="heading-roboto-small after-zigzag">Surowce</div>
                        <div class="theme__description">Wybierz jeden ze surówców i zobacz
                            jak korzystnie wypływa na Twoje ciało.</div>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="1">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <p>Miód swoje wyjątkowe właściwości zawdzięcza pszczołom i unikalnym składnikom
                            tworzącym jego bogatą oraz różnorodną recepturę. Jest bogaty w cukry,
                            kwasy tłuszczowe, enzymy, karotenoidy, sole mineralne oraz witaminy<br>- A, B,
                            C, D, E, K, P Jego cudowne właściwości odkryły już królowe Egiptu Kleopatra
                            i Penelopa.</p>
                        <ul>
                            <li>Znane są właściwości antybakteryjne miodu który, oczyszcza pory, odkaża
                                i skutecznie oczyszcza wszystkie typy cery, w tym cerę wrażliwą
                                oraz trądzikową likwidując efekty łupieżu,</li>
                            <li>doskonale pielęgnuje cerę wrażliwą a także trądzikową.</li>
                            <li>Miód, który jest głównym składnikiem każdego z naszych produktów jest
                                dobrze przyswajalny, sprawia, że skóra jest odpowiednio zregenerowana,
                                nawilżona i ma zapewnioną ochronę przed czynnikami zewnętrznymi</li>
                            <li>Miód poprawia elastyczność skóry, zmniejsza ryzyko powstania blizn
                                i przyspiesza gojenie ran</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="2">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <p>Rokitnik syberyjski uważany za „złoto Syberii’ to bogate źródło
                            antyoksydantów, flawonoidów, aminokwasów, mikroelementów, witamin (A, C, D, E,
                            K, P, B) oraz prowitamin, które przyspieszają procesy regeneracyjne skóry,</p>
                        <ul>
                            <li>rokitnik to antyoksydant, który chroni skórę przed promieniowaniem UVB
                                a także poprawia kolor opalenizny</li>
                            <li>wyciąg z rokitnika syberyjskiego hamuje proces wypadania włosów</li>
                            <li>rokitnik ma również działanie przeciwzapalne, antybakteryjne
                                i antywirusowe</li>
                        </ul>
                        <p> </p>
                        <ul>
                            <li>wyciąg z rokitnika opóźnia starzenie się naskórka, wspomaga
                                gojenie ran, łagodzi podrażnienia</li>
                            <li>rokitnik ma również działanie przeciwzapalne, antybakteryjne
                                i antywirusowe</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="3">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <p>Wyciąg z krokosza barwierskiego cenionego już w starożytnym
                            Egipcie i zwanego „ skarbem faraona”, działa antyoksydacyjnie,</p>
                        <ul>
                            <li>nawilża skórę</li>
                            <li>łagodzi objawy trądziku i łuszczycy, nadaje piękny koloryt skórze.</li>
                            <li>wyciąg z krokosza barwiarskiego spowalnia procesy starzenia
                                się skóry, przywraca elastyczność, jędrność i sprężystość skóry
                            </li>
                            <li>redukuje plamy i przebarwienia skór, a także przyspiesza odnowę
                                komórkową skóry spłycając lub likwidując zmarszczki,</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="4">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <ul>
                            <li>Olej rzepakowy bogaty jest w witaminy, szczególnie E zwaną „witaminą
                                młodości”, kwasy Omega wraz z przeciwutleniaczami i fitosterolami.
                            </li>
                            <li>bogactwo składników oleju rzepakowego pozytywnie oddziałuje na komórki
                                skóry scalając je i ujędrniając ją, utrzymując jej
                                odpowiednią elastyczność oraz gładkość tym samym chroniąc skórę przed
                                utratą wilgoci oraz negatywnym działaniem wolnych rodników.</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="5">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <ul>
                            <li>Wosk pszczeli zawiera kwasy takie jak palmitynowy, melisowy, octowy,
                                masłowy czy walerianowy oraz proteiny, lipidy, witaminy i cenne
                                sole mineralne</li>
                            <li>Wosk pszczeli dzięki silnym substancjom odżywczym chroni skórę przed
                                wysuszeniem, koi podrażnienia, głęboko pielęgnuje, a zawarta w nim
                                wit. A jednocześnie niszczy wolne rodniki i pobudza skórę
                                do produkcji kolagenu, odbudowując płaszcz lipidowy umożliwiający
                                zatrzymanie w niej wody, nie zatykając przy tym porów.</li>
                            <li>pszczeli wosk jest świetnym emolientem okrywającym skórę warstwą ochronną,
                                nadaje skórze jedwabisty blask, chroniąc ją przed wysuszeniem</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="6">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <ul>
                            <li>Ryż to popularny w krajach azjatyckich produkt kosmetyczny, który
                                oczyszcza i regeneruje skórę</li>
                        </ul>
                        <ul>
                            <li>Ryż zawarty w naszym 100% naturalnym peelingu, dzięki swoim
                                właściwościom, korzystnie wpływa na naturalne funkcje ochronne
                                naskórka. Powoduje skuteczne zatrzymywanie wewnątrz niego wody, dostarczając
                                witamin<br>- A, B, C, D, E, K, minerałów<br>- potas, magnez, cynk oraz
                                niezbędnych nienasyconych kwasów tłuszczowych, które odbudowują płaszcz
                                lipidowy naskórka równocześnie zwiększając produkcję kolagenu,
                                co powoduje, że skóra staje się młodsza i bardziej
                                zdrowa.</li>
                            <li>Ryż ma działanie nawilżające oraz wygładzające naskórek, chroni skórę
                                przed utratą wody</li>
                            <li>Peeling ryżowy poprawia krążenie krwi</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="7">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <ul>
                            <li>Kawa zawarta w naszym 100% naturalnym peelingu korzystnie wpływa
                                na ujednolicenie koloru skóry, ma działanie antycellulitowe oraz
                                przeciwzmarszczkowe</li>
                            <li>Kawa wygładza i ujędrnia skórę</li>
                            <li>Kofeina zawarta w kawie wpływa na mikrokrążenie, rozszerza
                                naczynka włosowate dzięki czemu ujednolica koloryt skóry a regularnie
                                stosowana w pielęgnacji ma działanie wyszczuplające.</li>
                            <li>Peeling usuwa zanieczyszczenia oraz obumarłe komórki naskórka
                                pobudzając go do wytwarzania nowych. Skóra jest bardziej gładka,
                                elastyczna oraz bardziej ukrwiona.</li>
                            <li>Peeling kawowy stosowany do oczyszczania twarzy przyczynia
                                się do zmniejszenia ciemnych sińców pod oczami</li>
                        </ul>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="8">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <p>Kwasek cytrynowy poprawia koloryt skóry, redukuje przebarwienia
                            oraz wygładza zmarszczki i cienkie linie oraz minimalizuje
                            niekorzystny wpływ na skórę promieni UV i zanieczyszczeń
                            powietrza.</p>
                    </div>
                </div>
                <div class="js-box home-components__box" data-type="9">
                    <div class="home-components__box__wrapper">
                        <div class="home-components__box__close"> </div>
                        <ul>
                            <li>olejek cytrynowy energetyzuje, odstrasza owady oraz ma właściwości
                                antyreumatyczne</li>
                            <li>olejki pobudzają również zmysły</li>
                            <li>Olejek pomarańczowy wzmacnia tkankę łączną, ma działanie ujędrniające
                                i antycellulitowe, dodatkowo poprawia samopoczucie, relaksuje,
                                regeneruje ciało i duszę.</li>
                            <li>Olejek lawendowy reguluje wydzielanie sebum, redukuje obrzęki a zawarte
                                w nim antyoksydanty działają ochronnie i przeciwstarzeniowo.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="js-item home-components__item" data-type="1">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Miód</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/miod.png" alt="miód" width="160"
                        height="179">
                </div>
            </div>
            <div class="js-item home-components__item" data-type="2">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Rokitnik</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/rozkotnik.png" alt="rozkitnik"
                        width="160" height="180"></div>
            </div>
            <div class="js-item home-components__item" data-type="3">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Krokosz</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/krokosz.png" alt="krokosz"
                        width="160" height="180"></div>
            </div>
            <div class="js-item home-components__item" data-type="4">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Rzepak</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/rzepak.png" alt="rzepak" width="160"
                        height="180"></div>
            </div>
            <div class="js-item home-components__item" data-type="5">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Wosk</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/wosk.png" alt="wosk" width="160"
                        height="180">
                </div>
            </div>
            <div class="js-item home-components__item" data-type="6">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Ryż</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ryz.png" alt="ryż" width="160"
                        height="180">
                </div>
            </div>
            <div class="js-item home-components__item" data-type="7">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description">Kawa</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/kawa.png" alt="Kawa" width="160"
                        height="180">
                </div>
            </div>
            <div class="js-item home-components__item" data-type="8">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description home-components__item__description-lg">
                        Kwasek<br>cytrynowy</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/kwasek.png" alt="Kwasek cytrynowy"
                        width="160" height="180"></div>
            </div>
            <div class="js-item home-components__item" data-type="9">
                <div class="home-components__item__face home-components__item__front"><img
                        class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/ramka-plaster-miodu.png" alt="miód"
                        width="160" height="179">
                    <p class="home-components__item__description home-components__item__description-lg">
                        Olejki<br>eteryczne</p>
                </div>
                <div class="home-components__item__face home-components__item__back"><img class="home-components__image"
                        src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/olejek.png" alt="Olejki eteryczne"
                        width="168" height="176"></div>
            </div>
        </div>
    </div>
</div>