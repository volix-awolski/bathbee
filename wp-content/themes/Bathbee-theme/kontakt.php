<?php
/**
 * Template Name: Kontakt
 *
 * @package WordPress
 * 
 * 
 */

get_header(); ?>
<?php get_header(); ?>
<div>
    <main role="main">
        <!-- section -->

        <div style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/green-background.png)"
            class="container-fluid bread-header-archive">
            <h1 class="d-block col-12 text-center"><?php the_title(); ?></h1>
        </div>
        <section class="container" style="min-height:60vh;margin-top:30px">
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class('col-12'); ?>>

                <div class="formularzkontakt">
                    <p class="naglowekstrona">SKONTAKTUJ SIĘ Z NAMI</p>
                    <div class="formularz">
                        <?php echo do_shortcode('[contact-form-7 id="6288" title="Formularz Kontaktowy"]'); ?>
                    </div>

            </article>
            <!-- /article -->

            <?php endwhile; ?>

            <?php else: ?>

            <!-- article -->
            <article>

                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

            </article>
            <!-- /article -->

            <?php endif; ?>

        </section>
        <!-- /section -->
    </main>
</div>


<?php get_footer(); ?>