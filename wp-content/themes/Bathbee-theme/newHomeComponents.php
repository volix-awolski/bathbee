<div class="newHomeComponents row mx-auto py-5" style="max-width:1400px;padding:50px 10px">
    <div class="col-md-6 col-12"><img src="<?php echo get_template_directory_uri(  )?>/img/skladniki.png"
            alt="skladniki">
    </div>
    <div class="col-md-6 col-12">
        <div class="accordion home-accordion" id="home-accordion">

            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#miod"
                            aria-expanded="true" aria-controls="miod">
                            Miód
                        </button>
                    </h2>
                </div>
                <div id="miod" class="collapse show" aria-labelledby="headingTwo" data-parent="#home-accordion">
                    <div class="card-body">
                        <p>Miód swoje wyjątkowe właściwości zawdzięcza pszczołom i unikalnym składnikom
                            tworzącym jego bogatą oraz różnorodną recepturę. Jest bogaty w cukry,
                            kwasy tłuszczowe, enzymy, karotenoidy, sole mineralne oraz witaminy<br>- A, B,
                            C, D, E, K, P Jego cudowne właściwości odkryły już królowe Egiptu Kleopatra
                            i Penelopa.</p>
                        <ul>
                            <li>Znane są właściwości antybakteryjne miodu który, oczyszcza pory, odkaża
                                i skutecznie oczyszcza wszystkie typy cery, w tym cerę wrażliwą
                                oraz trądzikową likwidując efekty łupieżu,</li>
                            <li>doskonale pielęgnuje cerę wrażliwą a także trądzikową.</li>
                            <li>Miód, który jest głównym składnikiem każdego z naszych produktów jest
                                dobrze przyswajalny, sprawia, że skóra jest odpowiednio zregenerowana,
                                nawilżona i ma zapewnioną ochronę przed czynnikami zewnętrznymi</li>
                            <li>Miód poprawia elastyczność skóry, zmniejsza ryzyko powstania blizn
                                i przyspiesza gojenie ran</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#rokitnik" aria-expanded="false" aria-controls="rokitnik">
                            Rokitnik
                        </button>
                    </h2>
                </div>
                <div id="rokitnik" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <p>Rokitnik syberyjski uważany za „złoto Syberii’ to bogate źródło
                            antyoksydantów, flawonoidów, aminokwasów, mikroelementów, witamin (A, C, D, E,
                            K, P, B) oraz prowitamin, które przyspieszają procesy regeneracyjne skóry,</p>
                        <ul>
                            <li>rokitnik to antyoksydant, który chroni skórę przed promieniowaniem UVB
                                a także poprawia kolor opalenizny</li>
                            <li>wyciąg z rokitnika syberyjskiego hamuje proces wypadania włosów</li>
                            <li>rokitnik ma również działanie przeciwzapalne, antybakteryjne
                                i antywirusowe</li>
                        </ul>
                        <p> </p>
                        <ul>
                            <li>wyciąg z rokitnika opóźnia starzenie się naskórka, wspomaga
                                gojenie ran, łagodzi podrażnienia</li>
                            <li>rokitnik ma również działanie przeciwzapalne, antybakteryjne
                                i antywirusowe</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#wosk"
                            aria-expanded="false" aria-controls="wosk">
                            Wosk
                        </button>
                    </h2>
                </div>
                <div id="wosk" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <ul>
                            <li>Wosk pszczeli zawiera kwasy takie jak palmitynowy, melisowy, octowy,
                                masłowy czy walerianowy oraz proteiny, lipidy, witaminy i cenne
                                sole mineralne</li>
                            <li>Wosk pszczeli dzięki silnym substancjom odżywczym chroni skórę przed
                                wysuszeniem, koi podrażnienia, głęboko pielęgnuje, a zawarta w nim
                                wit. A jednocześnie niszczy wolne rodniki i pobudza skórę
                                do produkcji kolagenu, odbudowując płaszcz lipidowy umożliwiający
                                zatrzymanie w niej wody, nie zatykając przy tym porów.</li>
                            <li>pszczeli wosk jest świetnym emolientem okrywającym skórę warstwą ochronną,
                                nadaje skórze jedwabisty blask, chroniąc ją przed wysuszeniem</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#kawa"
                            aria-expanded="false" aria-controls="kawa">
                            Kawa
                        </button>
                    </h2>
                </div>
                <div id="kawa" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <ul>
                            <li>Kawa zawarta w naszym 100% naturalnym peelingu korzystnie wpływa
                                na ujednolicenie koloru skóry, ma działanie antycellulitowe oraz
                                przeciwzmarszczkowe</li>
                            <li>Kawa wygładza i ujędrnia skórę</li>
                            <li>Kofeina zawarta w kawie wpływa na mikrokrążenie, rozszerza
                                naczynka włosowate dzięki czemu ujednolica koloryt skóry a regularnie
                                stosowana w pielęgnacji ma działanie wyszczuplające.</li>
                            <li>Peeling usuwa zanieczyszczenia oraz obumarłe komórki naskórka
                                pobudzając go do wytwarzania nowych. Skóra jest bardziej gładka,
                                elastyczna oraz bardziej ukrwiona.</li>
                            <li>Peeling kawowy stosowany do oczyszczania twarzy przyczynia
                                się do zmniejszenia ciemnych sińców pod oczami</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#olejki" aria-expanded="false" aria-controls="olejki">
                            Olejki eteryczne
                        </button>
                    </h2>
                </div>
                <div id="olejki" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <ul>
                            <li>olejek cytrynowy energetyzuje, odstrasza owady oraz ma właściwości
                                antyreumatyczne</li>
                            <li>olejki pobudzają również zmysły</li>
                            <li>Olejek pomarańczowy wzmacnia tkankę łączną, ma działanie ujędrniające
                                i antycellulitowe, dodatkowo poprawia samopoczucie, relaksuje,
                                regeneruje ciało i duszę.</li>
                            <li>Olejek lawendowy reguluje wydzielanie sebum, redukuje obrzęki a zawarte
                                w nim antyoksydanty działają ochronnie i przeciwstarzeniowo.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#Kwasek" aria-expanded="false" aria-controls="Kwasek">
                            Kwasek cytrynowy
                        </button>
                    </h2>
                </div>
                <div id="Kwasek" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <p>Kwasek cytrynowy poprawia koloryt skóry, redukuje przebarwienia
                            oraz wygładza zmarszczki i cienkie linie oraz minimalizuje
                            niekorzystny wpływ na skórę promieni UV i zanieczyszczeń
                            powietrza.</p>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#ryz"
                            aria-expanded="false" aria-controls="ryz">
                            Ryż
                        </button>
                    </h2>
                </div>
                <div id="ryz" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <ul>
                            <li>Ryż to popularny w krajach azjatyckich produkt kosmetyczny, który
                                oczyszcza i regeneruje skórę</li>
                        </ul>
                        <ul>
                            <li>Ryż zawarty w naszym 100% naturalnym peelingu, dzięki swoim
                                właściwościom, korzystnie wpływa na naturalne funkcje ochronne
                                naskórka. Powoduje skuteczne zatrzymywanie wewnątrz niego wody, dostarczając
                                witamin<br>- A, B, C, D, E, K, minerałów<br>- potas, magnez, cynk oraz
                                niezbędnych nienasyconych kwasów tłuszczowych, które odbudowują płaszcz
                                lipidowy naskórka równocześnie zwiększając produkcję kolagenu,
                                co powoduje, że skóra staje się młodsza i bardziej
                                zdrowa.</li>
                            <li>Ryż ma działanie nawilżające oraz wygładzające naskórek, chroni skórę
                                przed utratą wody</li>
                            <li>Peeling ryżowy poprawia krążenie krwi</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#RZEPAK" aria-expanded="false" aria-controls="RZEPAK">
                            RZEPAK
                        </button>
                    </h2>
                </div>
                <div id="RZEPAK" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <ul>
                            <li>Olej rzepakowy bogaty jest w witaminy, szczególnie E zwaną „witaminą
                                młodości”, kwasy Omega wraz z przeciwutleniaczami i fitosterolami.
                            </li>
                            <li>bogactwo składników oleju rzepakowego pozytywnie oddziałuje na komórki
                                skóry scalając je i ujędrniając ją, utrzymując jej
                                odpowiednią elastyczność oraz gładkość tym samym chroniąc skórę przed
                                utratą wilgoci oraz negatywnym działaniem wolnych rodników.</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                            data-target="#KROKOSZ" aria-expanded="false" aria-controls="KROKOSZ">
                            KROKOSZ
                        </button>
                    </h2>
                </div>
                <div id="KROKOSZ" class="collapse" aria-labelledby="headingThree" data-parent="#home-accordion">
                    <div class="card-body">
                        <p>Wyciąg z krokosza barwierskiego cenionego już w starożytnym
                            Egipcie i zwanego „ skarbem faraona”, działa antyoksydacyjnie,</p>
                        <ul>
                            <li>nawilża skórę</li>
                            <li>łagodzi objawy trądziku i łuszczycy, nadaje piękny koloryt skórze.</li>
                            <li>wyciąg z krokosza barwiarskiego spowalnia procesy starzenia
                                się skóry, przywraca elastyczność, jędrność i sprężystość skóry
                            </li>
                            <li>redukuje plamy i przebarwienia skór, a także przyspiesza odnowę
                                komórkową skóry spłycając lub likwidując zmarszczki,</li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>