<?php get_header(); ?>

<div class="container-fluid front">
    <img class="hero-img" src="<?php echo content_url(); ?>/uploads/2019/07/slider2.jpg" alt="">

    <div class="row">
        <div class="col-md-6 col-12 m-auto">
            <div class="home-about__box">
                <p class="heading-caveat">100% naturalne składniki na bazie
                    miodu</p>
                <p class="heading-roboto">Czym jest Bathbee?</p>

                <p class="p-desc">Bathbee to unikalna na światową skalę, bezwodna
                    linia kosmetyków pod prysznic i do kąpieli zawierająca wyłącznie naturalne składniki
                    z ponad 50% zawartością miodu. To kosmetyki przeznaczone do pielęgnacji skóry
                    i włosów.<br><br>Linia kosmetyków bathbee w chwili obecnej składa się z 3
                    rodzajów produktów dopuszczonych do sprzedaży: miodowej maski do ciała pakowanej
                    w tuby i wodorozpuszczalne saszetki, a także peelingu kawowego oraz peelingu ryżowego
                    pakowanych w tuby.</p>
            </div>
        </div>

        <?php require('productsFeaturedonFrontPageTop.php')?>
    </div>

    <div class="container d-flex justify-content-center home-effect-container">
        <div class="home-effect__wrapper col-12 col-md-4">
            <img src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/odzywaianie-1.jpg" alt="">
            <p>Odżywianie i nawilżenie</p>

            <ul>
                <li>nawilża skórę i likwiduje zmarszczki</li>
                <li>działa przeciwstarzeniowo a antyoksydanty zawarte w składnikach chronią skórę przed tym procesem
                </li>
                <li>odżywia skórę dostarczając jej witamin, które wnikają w głąb skóry powodując zwiększenie jej
                    elastyczności</li>
            </ul>
        </div>



        <div class="home-effect__wrapper col-12 col-md-4">
            <img src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/woda.jpg" alt="">
            <p>Oczyszczenie</p>

            <ul>
                <li>złuszcza naskórek- peeling</li>
                <li>działa bakteriobójczo usuwa nadmiaru sebum
                </li>
            </ul>
            <p class="normal-text">Po zabiegu wystarczy spłukać ciepłą wodą pozostałość i osuszyć delikatnie skórę a
                ciało na długo będzie pamiętało ten zabieg.</p>
        </div>

        <div class="home-effect__wrapper col-12 col-md-4">
            <img src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/serce.jpg" alt="">
            <p>Ochrona</p>

            <ul>
                <li>odżywia skórę dostarczając jej witamin, które wnikają w głąb skóry powodując zwiększenie jej
                    elastyczności</li>
                <li>dwosk zawarty w kosmetykach przywraca skórze warstwę ochronną zmytą podczas zabiegów pielęgnacyjnych
                </li>

            </ul>
        </div>

        <div class="home-effect__wrapper col-12 col-md-4">
            <img src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/kwiatek-1.jpg" alt="">
            <p>Przyjemny naturalny zapach</p>

            <p class="normal-text">zmysłowe olejki eteryczne</p>
        </div>

        <div class="home-effect__wrapper col-12 col-md-4">
            <img src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/natura-1.jpg" alt="">
            <p>Uniwersalne i proste zastosowanie</p>

            <ul>
                <li>do aplikacji zarówno w wannie, jak i pod prysznicem</li>
                <li>na włosy, usta, skórę głowy i całego ciała</li>
                <li>maska, peeling, odżywka</li>
            </ul>
        </div>
    </div>

    <div class="homeComponents-wrapper py-5"
        style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/produkty.jpg)"
        class="container-fluid">
        <div class="container">
            <p class="heading-caveat d-block">Z czego zrobiony jest nasz kosmetyk</p>
            <p class="heading-roboto-small">Kosmetyki „ bathbee” produkujemy z naturalnych surowców</p>
            <p class="normal-text text-left col-sm-12 col-md-9 mx-auto pt-5">Opracowaliśmy dla Państwa specjalną
                technologię
                umożliwiającą łączenie
                tłuszczy z
                miodem, co znacząco przyczyniło się do spowolnienia procesu rozwarstwiania się składników w naszych
                kosmetykach. Dzięki tej technologii kosmetyki zachowują trwałość i wartość poszczególnych składników.
                Nasze produkty nie zawierają żadnych sztucznych konserwantów, a kosmetyki przeszły testy konserwacyjne,
                dermatologiczne i kompatybilności z opakowaniami dające gwarancję najwyższej jakości.</p>
        </div>
    </div>

    <?php require('newHomeComponents.php'); ?>

    <div class="container-fluid dla-kogo-przeznaczone">
        <div class="row container mx-auto ">
            <div class="col-sm-12 col-md-6 pt-5"><img
                    src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/dla-kogo.jpg" alt=""></div>
            <div class="col-sm-12 col-md-6 d-flex align-content-center flex-wrap pt-5">
                <div class="wrap">
                    <p class="heading-caveat d-block">Dla kogo przeznaczone są nasze produkty?</p>
                    <p class="heading-roboto-small after-zigzag smaller">Dla młodych, starszych i najstarszych,
                        dla małych, większych i tych całkiem dużych,
                        dla zdrowych i chorych,
                        dla tych z problemami (nie tylko skórnymi) i bez,
                        dla nowoczesnych i tych bardziej tradycyjnych.
                    </p>
                    <p class="normal-text text-left mx-auto pt-5 ">Linia kosmetyków Bathbee to produkty dla ludzi
                        świadomych swojego ciała oraz jego potrzeb. Ludzi, którzy rozumieją jak szkodliwy wpływ na nasze
                        ciało mają kosmetyki syntetyczne i zawarte w nich konserwanty. Ludzi kochających naturę,
                        aktywnych, proekologicznych oraz ceniących swój czas. Ludzi zabieganych i tych, którzy lubią
                        celebrować rytuał kąpieli.
                    </p>
                    <p class="info-check">*Oczywiście jeśli jesteś alergikiem, powinieneś sprawdzić, czy nie jesteś
                        uczulony na któryś ze składników</p>
                    <!-- <div class="home-for__box">
                        <p class="home-for__box__item">Dla ludzi młodych</p>
                        <p class="home-for__box__item">Dla osób z problemami skórnymi</p>
                        <p class="home-for__box__item">Dla ludzi dojrzałych i aktywnych</p>
                        <p class="home-for__box__item">Dla ludzi starszych ze skórą wysuszoną i zniszczoną</p>
                        <p class="home-for__box__item">Dla ludzi z oparzeniami skórnymi wywołanymi słońcem</p>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid jak-stosowac-produkty"
        style="background:url(<?php echo get_site_url()?>/wp-content/uploads/2019/07/bg_work.jpg)">
        <div class="container py-5">
            <p class="heading-caveat-big pt-5">Jak stosować produkty Bathbee</p>
        </div>
        <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center">
            <div class="col-md-3 col-sm-12"><img
                    src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-1.png" alt=""></div>
            <div class="col-md-9 col-sm-12 ">
                <p class="heading-roboto-small after-zigzag">Maska</p>
                <p class="normal-text text-left">Umyłeś ciało swoim ulubionym mydłem? Teraz rozsmaruj na nim 100%
                    naturalną
                    miodową maskę do ciała. Jeśli masz czas zrelaksuj się leżąc w wannie, jeśli nie- użyj maski pod
                    prysznicem. Po zakończonej kąpieli wystarczy, że dokładnie spłuczesz skórę i osuszysz ją delikatnie
                    ręcznikiem. Po zakończonym zabiegu od razu możesz się ubierać, ponieważ po aplikacji maski nie
                    potrzeba stosować niczego więcej. Nasza maska zarówno odżywia, regeneruje jak i nawilża skórę.</p>
            </div>
        </div>

        <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center">

            <div class="col-md-9 col-sm-12 order-2 order-md-1">
                <p class="heading-roboto-small after-zigzag">Peeling</p>
                <p class="normal-text text-left">Umyj ciało ulubionym mydłem. Nałóż peeling na dłoń i rozetrzyj na
                    skórze. Usuwając martwy naskórek jednocześnie wmasowujesz w skórę substancje odżywcze zawarte w
                    naszym preparacie. Spłucz peeling i osusz delikatnie ciało ręcznikiem. Twoja skóra jest oczyszczona
                    i nawilżona. Nie wymaga nakładania dodatkowych preparatów.</p>
                <p class="normal-text text-left">Jeśli Twoja skóra jest bardzo przesuszona możesz dodatkowo użyć podczas
                    kąpieli miodowej maski batbee, która dodatkowo zabezpieczy, odżywi i nawilży skórę całego ciała.</p>
            </div>
            <div class="col-md-3 col-sm-12 order-1 order-md-2"><img
                    src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-2.png" alt=""></div>
        </div>

        <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center">
            <div class="col-md-3 col-sm-12"><img
                    src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-3.png" alt=""></div>
            <div class="col-md-9 col-sm-12 ">
                <p class="heading-roboto-small after-zigzag">Wzmocnienie włosów</p>
                <p class="normal-text text-left">W celu wzmocnienia włosów i leczenia chorób skóry głowy maskę należy
                    nakładać regularnie na włosy i wcierać w skórę przed ich umyciem. W miarę możliwości pozostawić jak
                    najdłużej. Pierwsze efekty odczuwalne są już po jednorazowej aplikacji. Jeśli masz czas na głębszą i
                    dłuższą kurację umyj włosy szamponem i osusz ręcznikiem. Nałóż maskę na osuszone włosy i wetrzyj w
                    skórę głowy. Pozostaw na włosach jak najdłużej. W celu wzmocnienia działania maski można owinąć
                    głowę folią i ciepłym ręcznikiem. Następnie zmyj maskę i wysusz włosy.</p>
            </div>
        </div>

        <div class="row container mx-auto">
            <div class="col-md-6 col-sm-12">
                <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center">

                    <div class="col-md-9 col-sm-12 order-2 order-md-1">
                        <p class="heading-roboto-small after-zigzag">Dla dzieci</p>
                        <p class="normal-text text-left">Do pielęgnacji i kąpieli małych dzieci używać dodając ok. 10-15
                            ml do wody, po uprzednim sprawdzeniu czy nie są uczulone na któryś ze składników</p>
                    </div>
                    <div class="col-md-3 col-sm-12 d-flex align-content-center px-md-0 order-1 order-md-2"><img
                            class="responsive-img"
                            src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-4.png" alt=""></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center ">

                    <div class="col-md-9 col-sm-12 order-2 order-md-1">
                        <p class="heading-roboto-small after-zigzag">Na skóre rąk</p>
                        <p class="normal-text text-left">Jako maska regenerująca na zniszczoną skórę dłoni- po umyciu
                            rąk rozmasowujemy maskę na wilgotnej skórze. Spłukujemy i osuszamy delikatnie ręcznikiem.
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-12 px-0 order-1 order-md-2"><img class="responsive-img"
                            src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-7.png" alt=""></div>
                </div>
            </div>
        </div>

        <div class="row container mx-auto">
            <div class="col-md-6 col-sm-12">
                <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center ">

                    <div class="col-md-9 col-sm-12 order-2 order-md-1">
                        <p class="heading-roboto-small after-zigzag">Na usta</p>
                        <p class="normal-text text-left">Jako kurację regenerującą na spierzchnięte usta- dostanie się
                            kosmetyku do wnętrza jamy ustnej jest nieszkodliwe, wszystkie surowce są jadalne a smak
                            wyjątkowy.</p>
                    </div>
                    <div class="col-md-3 col-sm-12 d-flex align-content-center px-0 order-1 order-md-2"><img
                            class="responsive-img"
                            src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-6.png" alt=""></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="container row d-flex flex-wrap mx-auto py-5 align-content-center">

                    <div class="col-md-9 col-sm-12 order-2 order-md-1">
                        <p class="heading-roboto-small after-zigzag">Przed goleniem i po goleniu</p>
                        <p class="normal-text text-left">Przed golenie jako preparat zmiękczający usuwany włos, po
                            goleniu jako maska na podrażnioną skórę.
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-12 px-0 order-1 order-md-2"><img class="responsive-img"
                            src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/usage-5.png" alt=""></div>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid relaks-dla-twojego-ciala py-5">
        <div class="container row mx-auto py-5 ">
            <div class="col-md-6 col-sm-12 order-2 order-md-1 py-5 d-flex flex-wrap align-content-center">
                <div class="wrapp">
                    <p class="heading-roboto-small after-zigzag">Relaks dla Twojego ciała</p>
                    <p class="normal-text text-left">Aromaterapeutyczne właściwości naszych produktów wpływają
                        relaksująco
                        na nasze ciało. W celu zmaksymalizowania doznań zalecamy kąpiel przy świecach. Chcemy by czas
                        przeznaczony dla naszego ciała był wyjątkowy jak kosmetyk, który Państwu oferujemy.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 order-1 order-md-2"> <img
                    src="<?php echo get_site_url()?>/wp-content/uploads/2019/07/graph.jpg" alt=""></div>
        </div>

    </div>

    <?php require('recommendedProductsOnFront.php'); ?>

    <?php get_footer(); ?>