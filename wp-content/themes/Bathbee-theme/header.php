<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>

    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <link href="https://fonts.googleapis.com/css?family=Caveat|Roboto+Slab:400,700&display=swap&subset=latin-ext"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700&display=swap&subset=cyrillic-ext"
        rel="stylesheet">



    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/fav.jpg" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/fav.jpg" rel="apple-touch-icon-precomposed">

    <?php 
    function ogimage() {
        if(is_product()){
         global $post;
         $product = wc_get_product( $post->ID );
        return wp_get_attachment_url( $product->get_image_id() );
        } else {
            return get_template_directory_uri().'/img/ogimage.jpg';
        }
    }
    ?>

    <meta property="og:url" content="<?php echo get_permalink()?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo get_bloginfo( 'name' );?> - <?php echo get_the_title();?>" />
    <meta property="og:description" content="<?php echo $desc?>" />
    <meta property="og:image" content="<?php echo ogimage()?>" />


    <meta http-equiv=" X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <!-- wrapper -->
    <div class="wrapper">

        <!-- header -->
        <header class="container-fluid header">
            <div id="wrapp-logo-menu" class="wrapp-logo-menu">
                <div class="logo">
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo content_url(); ?>/uploads/2019/07/BATHBEE_LOGO_200.png" alt="Logo"
                            class="logo-img">
                    </a>
                </div>

                <div id="primary-menu" class="primary-menu">
                    <?php wp_nav_menu( array( 'theme_location' => 'menu-glowne' ) ); ?>
                </div>
            </div>
            <div class="featured-text">
                <div class="login"> <?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"
                        title="<?php _e('Moje konto','woothemes'); ?>"><?php _e('Moje Konto','woothemes'); ?></a>
                    <?php } else { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"
                        title="<?php _e('Zaloguj się / Zarejestruj','woothemes'); ?>"><?php _e('Zaloguj się / Zarejestruj','woothemes'); ?></a>
                    <?php } ?>
                </div>

                <div id="koszyk" class="koszyk">
                    <?php 
                    $link = wc_get_checkout_url();
					echo '<a href="'.$link.'" class="dropdown-back" data-toggle="dropdown"> ';
					echo '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
					echo '<div class="basket-item-count" style="display: inline;">';
						echo '<span class="cart-items-count count">';
							echo WC()->cart->get_cart_contents_count();
						echo '</span>';
					echo '</div>';
					echo '</a>';
					echo '<ul class="dropdown-menu dropdown-menu-mini-cart">';
						echo '<li> <div class="widget_shopping_cart_content">';
								  woocommerce_mini_cart();
							echo '</div></li></ul>';
					?>
                </div>

            </div>






        </header>
        <!-- /header -->