<?php
session_start();

add_action( 'wp_head', 'remove_product_bundle');
function remove_product_bundle(){

// Bundled item wrapper open.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_details_wrapper_open', 0);

// Bundled item image.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_thumbnail', 5);

// Bundled item details container open.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_details_open', 10);

// Bundled item title.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_title', 15);

// Bundled item description.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_description', 20);

// Bundled product details template.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_product_details', 25);

// Bundled item details container close.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_bundled_item_details_close', 30);

// Bundled item qty template in tabular layout.
remove_action( 'woocommerce_bundled_item_details', 'wc_pb_template_tabular_bundled_item_qty', 35);

}

function setCity()
{
	$_SESSION['city'] = sanitize_text_field($_POST['city']);
}
function setVoivodeship()
{
	$_SESSION['voivodeship'] = sanitize_text_field($_POST['voivodeship']);
}
function setPoint()
{
	$_SESSION['point'] = sanitize_text_field($_POST['point']);
}

add_action('wp_ajax_setCity', 'setCity');
add_action('wp_ajax_nopriv_setCity', 'setCity');

add_action('wp_ajax_setPoint', 'setPoint');
add_action('wp_ajax_nopriv_setPoint', 'setPoint');


add_action('wp_ajax_setVoivodeship', 'setVoivodeship');
add_action('wp_ajax_nopriv_setVoivodeship', 'setVoivodeship');


function getSystemClient($user_id)
{
	global $wpdb;
	$user = get_user_by('ID', $user_id);
	$email = $user->user_email;
	return $wpdb->get_row($wpdb->prepare('SELECT * FROM Client WHERE email = %s', [$email]));
}

function getDiscounts()
{
	global $wpdb;
	//	$id = $client->id;
		$sql = $wpdb->get_results("SELECT value, value_type FROM configurations WHERE type in ('group', 'coupon')");
		return $sql;
	
	return false;
}

function getDiscountsForClient()
{
	global $wpdb;
	if ($client = getSystemClient(get_current_user_id())) {
		$id = $client->id;
		$sql = $wpdb->get_results("SELECT value, value_type FROM configurations, User, user_discounts WHERE type in ('group', 'coupon') AND configurations.id = user_discounts.discount_id AND user_discounts.user_id = ".$id);
		return $sql;
	}
	return false;
}

function getClientRoles()
{
    
}


function calculateDiscount($product_id)
{
	
	if (is_object($product_id)) {
		$product_id = $product_id->get_id();
	}

	$product = wc_get_product($product_id);
	$price = $product->get_regular_price();
	if ($product->is_on_sale()) {
		$price = $product->get_sale_price();
	}
	$price = (float) $price;
	$basePrice = $price;
	if ($price < getMinimalCartAmountValue()) {
		return $price;
	}
	if (get_field('brak_ceny_preferencyjnej',$product_id) == true) {
		return $price;
	}
	if (is_array($GLOBALS['discounts'])) {
	foreach ($GLOBALS['discounts'] as $discount) {
		if ($discount->value_type == 'percent') {
			$price = $price - ($basePrice * $discount->value / 100);
		} else {
			$price -= $discount->value;
		}
	}
}
	return number_format($price, 2);
}

$GLOBALS['discounts'] = getDiscounts();
function addToCartWithDiscount($product_id, $price) 
{

}

function calculateCashback($price)
{
	
	global $wpdb;
	$price = $price / 1.23;
	$value = $wpdb->get_var("SELECT value FROM configurations WHERE type = 'cashback'");
	$value_type = $wpdb->get_var("SELECT value_type FROM configurations WHERE type = 'cashback'");

	$cashback = $price * ($value_type == 'percent' ? ($value / 100) : $value); 
	return number_format($cashback, 2);
}

function calculateSP($price)
{
	
	global $wpdb;
		$price = $price / 1.23;
	$value = $wpdb->get_var("SELECT value FROM configurations WHERE type = 'sp_value'");
	$value_type = $wpdb->get_var("SELECT value_type FROM configurations WHERE type = 'sp_value'");
	
	$sp = $price / 450 * ($value_type == 'percent' ? ($value / 100) : $value); 
	return number_format($sp, 2);
}

function getMinimalCartAmountValue()
{
	
	global $wpdb;
	return $wpdb->get_var('SELECT value FROM configurations WHERE type = "min_turnover"');
}

function isActiveClient()
{
	
	return (is_user_logged_in() && getSystemClient(get_current_user_id()));
}

function sv_change_product_price_display( $price ) {
	global $post;
	
	$product = wc_get_product($post->ID);
	if (is_bool($product)) {
		return $price;
	}
	if (get_field('brak_ceny_preferencyjnej', $post->ID)) {
		return $price;
	}
	$basePrice = $product->get_regular_price();
	if ($product->is_on_sale()) {
		$basePrice = $product->get_sale_price();
	}
	$basePrice = (float) $basePrice;

	if ($basePrice < getMinimalCartAmountValue() ) {
		return $basePrice;
	}

	$price = '<span class="preferencyjna">'.calculateDiscount($post->ID).' zł*</span>'.$price;
	return $price;
}
add_filter( 'woocommerce_get_price_html', 'sv_change_product_price_display' );
add_filter( 'woocommerce_cart_item_price', 'sv_change_product_price_display' );
/*
 add_action( 'woocommerce_before_calculate_totals',   'update_price'  );
 function update_price( $cart_object ) {
     
    global $woocommerce;
        $items = $woocommerce->cart->get_cart();

if ($items) {
        foreach($items as $item => $values) { 
            $product =  wc_get_product( $values['data']->get_id()); 
         	$basePrice = $product->get_regular_price();
			if ($product->is_on_sale()) {
				$basePrice = $product->get_sale_price();
			}
			$basePrice = (float) $basePrice;

			if ($basePrice < getMinimalCartAmountValue() || !isActiveClient()) {
				continue;
			} 
			$values['data']->set_price(calculateDiscount($product->get_id()));
        } 
        
      }
   }
*/


add_action( 'woocommerce_before_cart', '_coupon_active_user' );


function custom_registration_redirect() {
	
    if (isset($_POST['bathbee_cashback'])) {
    	   $coupon_code = 'rejestracja'; 
   //if ( WC()->cart->has_discount( $coupon_code ) || !isActiveClient()) return;

    WC()->cart->add_discount( $coupon_code );
    		return home_url('/zamowienie');	
    }
    
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);


function woo_login_redirect( $redirect, $user ) {

    if( isset($_POST['bathbee_cashback'])) {
        return get_page_by_path('zamowienie');
    }
 
    return $redirect;
}
 
add_filter( 'woocommerce_login_redirect', 'woo_login_redirect' );

 
function _coupon_active_user() {
	
	if (!is_cart() && !is_checkout())
		return;

    $coupon_code = 'rejestracja'; 
   if ( WC()->cart->has_discount( $coupon_code ) || !isActiveClient()) return;

    WC()->cart->add_discount( $coupon_code );
    
   wc_print_notices();
}
function ajaxCalculateCashback()
{
	echo calculateCashback(WC()->cart->cart_contents_total);
	wp_die();
}


function ajaxCalculateSP()
{

	echo calculateSP(WC()->cart->cart_contents_total);
	wp_die();
}

add_action('wp_ajax_calculateCashback', 'ajaxCalculateCashback');
add_action('wp_ajax_nopriv_calculateCashback', 'ajaxCalculateCashback');

add_action('wp_ajax_calculateSP', 'ajaxCalculateSP');
add_action('wp_ajax_nopriv_calculateSP', 'ajaxCalculateSP');

add_action('wp_footer', function() {
?>
<script>
jQuery('body').on('updated_cart_totals', function() {
    jQuery.post('<?=admin_url('admin-ajax.php');?>', {
        action: 'calculateCashback'
    }, function(res) {
        jQuery('#cashback').text(res);
    });
    jQuery.post('<?=admin_url('admin-ajax.php');?>', {
        action: 'calculateSP'
    }, function(res) {
        jQuery('#sp').text(res);
    });
});
</script>
<?php
});


 function _remove_company($fields)
 {
 	unset($fields['billing']['billing_company']);
 	return $fields;
 }
add_filter( 'woocommerce_checkout_fields' , '_remove_company' );
 


function _display_pickup_locations($method) {
global $wpdb;

if ($method->method_id != 'local_pickup') {

	return;
}
$res = $wpdb->get_results("SELECT Voivodeship.id, Voivodeship.name FROM Point, Address, Voivodeship  WHERE Point.isActive = 1 AND Point.addressId = Address.id AND Address.voivodeshipId = Voivodeship.id order by name ASC");

$voivodeships = [''=>'Wybierz'];
if ($res) {

	foreach ($res as $r) {
		$voivodeships[$r->id] = $r->name;
	}
}

$output = '<div id="spinner"></div>
<div id="local_pickup" style="display:none">
<p class="form-row form-row-wide" id="voivodeships_field" data-priority=""><label for="voivodeships" class="">Województwo</label><span class="woocommerce-input-wrapper">
	<select name="voivodeships" id="voivodeships" class="select " data-allow_clear="true" data-placeholder="Województwo" tabindex="-1" aria-hidden="true">';
		
		foreach ($voivodeships as $id => $name) {
			$output .= "<option ".($_SESSION['voivodeship'] == $id ? "selected" : "")."value={$id}>{$name}</option>";
		}
		

	 $output .= '</select></p>

	<p class="form-row form-row-wide" id="city_field" data-priority=""><label for="city" class="">Miasto</label><span class="woocommerce-input-wrapper">
	<select name="city" id="city" class="select " data-allow_clear="true" data-placeholder="Miasto" tabindex="-1" aria-hidden="true">
		';

if ($_SESSION['city'] != '') {
			global $wpdb;
	$res = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT Address.city FROM Address, Point WHERE Point.isActive = 1 AND Address.id = Point.addressId AND Address.VoivodeshipId = %d", [$_SESSION['voivodeship']]));
	if ($res) {
		$output .= '<option value=\'\' selected>Wybierz</option>';
		foreach ($res as $r) {
			$output .= "<option ".($_SESSION['city'] == $r->city ? "selected" : "")."value=\"{$r->city}\">{$r->city}</option>\r\n";
		}
	}
	}

	$output .= '</select></p>

	<p class="form-row form-row-wide" id="pick_up_locations_field" data-priority=""><label for="pick_up_locations" class="">Punkt odbioru</label><span class="woocommerce-input-wrapper">
	<select name="pick_up_locations" id="pick_up_locations" class="select " data-allow_clear="true" data-placeholder="Punkt odbioru" tabindex="-1" aria-hidden="true">
		';

	$res = $wpdb->get_results($wpdb->prepare("SELECT Point.*, Address.* FROM Address, Point WHERE Point.isActive = 1 AND Address.id = Point.addressId AND Address.city = %s", [$_SESSION['city']]));
	if ($res) {
		$output .= '<option value=\'\' selected>Wybierz</option>';
		foreach ($res as $r) {
			$house = (is_null($r->apartmentNumber)) ? $r->houseNumber : $r->houseNumber.'/'.$r->apartmentNumber;
			$output .= "<option ".($_SESSION['point'] == $r->name ? "selected" : "")." value=\"{$r->name}\" data-city=\"{$r->city}\" data-street=\"{$r->street} {$house}\" data-postCode=\"{$r->postCode}\">{$r->name}</option>\r\n";
		}
	}

		
	$output .='</select></p></div> 

	<script style="display:none">
					if ($("#shipping_method input:checked").val() != "local_pickup:1") {
						$("#local_pickup").hide();
					}else {
						$("#local_pickup").show();
					}
					console.log(window.voivodeship, window.city, window.pick_up_locations);
					
					if (window.voivodeship != "") {
						$("#voivodeships").val(window.voivodeship).trigger("change");
					}
					if (window.city != "") {
						$("#city").val(window.city).trigger("change");
					}
					if (window.pick_up_locations != "") {
						$("#pick_up_locations").val(window.pick_up_locations).trigger("change");
					}
					</script>';
	echo $output;

}
 
//add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

 add_action('woocommerce_after_shipping_rate', '_display_pickup_locations');
add_action( 'woocommerce_review_order_after_shipping', '_checkout_update_pickup_address', 10);
 
function _checkout_update_pickup_address() {
         
        ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $("#spinner").hide();
    $('#city, #pick_up_locations, #voivodeships').select2();

    if ($('#city').val() == "") {
        $('#city, #pick_up_locations').attr('disabled', 'disabled');
    }

    jQuery('#voivodeships').change(function(ev) {
        voivodeship = $(this).val();
        if (voivodeship == '') {
            window.voivodeship = '';
            return;
        }
        window.voivodeship = voivodeship;
        $("#spinner").show();

        jQuery.post('<?=admin_url('admin-ajax.php');?>', {
            action: 'setVoivodeship',
            'voivodeship': voivodeship
        }, function(res) {});
        jQuery.post('<?=admin_url('admin-ajax.php');?>', {
            action: 'getCities',
            'voivodeship': voivodeship
        }, function(res) {

            if (res == '') {
                $('#city, #pick_up_locations').attr('disabled', 'disabled');
                return false;
            }
            $('#city').removeAttr('disabled');
            $("#city").select2("destroy");

            $("#city").select2();
            jQuery('#city').html(res);
            $("#spinner").hide();
        });
    });

    jQuery('#pick_up_locations').change(function(ev) {
        pick_up_locations = $(this).val();
        if (pick_up_locations == '') {
            window.pick_up_locations = '';
            return;
        }
        jQuery.post('<?=admin_url('admin-ajax.php');?>', {
            action: 'setPoint',
            'point': pick_up_locations
        }, function(res) {});
        window.pick_up_locations = pick_up_locations;
    });


    jQuery('#city').change(function(ev) {
        city = $(this).val();
        if (city == '') {
            window.city = '';
            return;
        }
        window.city = city;
        $("#spinner").show();
        jQuery.post('<?=admin_url('admin-ajax.php');?>', {
            action: 'setCity',
            'city': city
        }, function(res) {});
        jQuery.post('<?=admin_url('admin-ajax.php');?>', {
            action: 'getPoints',
            'city': city
        }, function(res) {
            console.log(res);
            if (res == '') {
                $('#pick_up_locations').attr('disabled', 'disabled');
                return false;
            }
            $('#pick_up_locations').removeAttr('disabled');
            jQuery('#pick_up_locations').html(res);
            $("#pick_up_locations").select2("destroy");

            $("#pick_up_locations").select2();
            $("#spinner").hide();
        });
    });



    var opts = {
        lines: 13, // The number of lines to draw
        length: 38, // The length of each line
        width: 17, // The line thickness
        radius: 45, // The radius of the inner circle
        scale: 1, // Scales overall size of the spinner
        corners: 1, // Corner roundness (0..1)
        color: '#ffffff', // CSS color or array of colors
        fadeColor: 'transparent', // CSS color or array of colors
        speed: 1, // Rounds per second
        rotate: 0, // The rotation offset
        animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
        direction: 1, // 1: clockwise, -1: counterclockwise
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        className: 'spinner', // The CSS class to assign to the spinner
        top: '50%', // Top position relative to parent
        left: '50%', // Left position relative to parent
        shadow: '0 0 1px transparent', // Box-shadow for the lines
        position: 'absolute' // Element positioning
    };

    var target = document.getElementById('spinner');
    var spinner = new Spinner(opts).spin(target);
    $("#spinner").hide();


});
</script>
<script type="text/javascript">
jQuery('select#pick_up_locations').on('change', function() {

    var location = jQuery(this).find('option:selected');

    if (location.data('city') != undefined) {

        jQuery('select#shipping_country').val('PL').change();
        jQuery('#shipping_city_field input').val(location.data('city')).change();
        jQuery('#shipping_address_1_field input').val(location.data('street')).change();
        jQuery('#shipping_postcode').val(location.data('postcode')).change();
        jQuery(".shipping_address #shipping_city, #shipping_country, #shipping_postcode, #shipping_address_1")
            .prop("readonly", true);



    } else {


        jQuery(".shipping_address #shipping_city, #shipping_country, #shipping_postcode, #shipping_address_1")
            .prop("readonly", false);

    }

});
</script>
<?php
                      
}

add_action('wp_ajax_getCities', 'ajaxGetCities');
add_action('wp_ajax_nopriv_getCities', 'ajaxGetCities');
function ajaxGetCities()
{
	global $wpdb;
	$res = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT Address.city FROM Address, Point WHERE Point.isActive = 1 AND Address.id = Point.addressId AND Address.VoivodeshipId = %d", [$_POST['voivodeship']]));
	if ($res) {
		$output = '<option value=\'\' selected>Wybierz</option>';
		foreach ($res as $r) {
			$output .= "<option value=\"{$r->city}\">{$r->city}</option>\r\n";
		}
	}
	echo $output;
	wp_die();
}


add_action('wp_ajax_getPoints', 'ajaxGetPoints');
add_action('wp_ajax_nopriv_getPoints', 'ajaxGetPoints');
function ajaxGetPoints()
{
	global $wpdb;
	$res = $wpdb->get_results($wpdb->prepare("SELECT Point.*, Address.* FROM Address, Point WHERE Point.isActive = 1 AND Address.id = Point.addressId AND Address.city = %s", [$_POST['city']]));
	if ($res) {
		$output = '<option value=\'\' selected>Wybierz</option>';
		foreach ($res as $r) {
			$house = (is_null($r->apartmentNumber)) ? $r->houseNumber : $r->houseNumber.'/'.$r->apartmentNumber;
			$output .= "<option value=\"{$r->name}\" data-city=\"{$r->city}\" data-street=\"{$r->street} {$house}\" data-postCode=\"{$r->postCode}\">{$r->name}</option>\r\n";
		}
	}
	echo $output;
	wp_die();
}

function woo_extra_login_fields() {
		if (isset($_GET['bathbee_cashback'])) {
		?>
<input type="hidden" name="bathbee_cashback" value="1">
<?php
	}
}

add_action('woocommerce_login_form', 'woo_extra_login_fields');

function wooc_extra_register_fields() {

	if (isset($_GET['bathbee_cashback'])) {
		?>
<input type="hidden" name="bathbee_cashback" value="1">
<?php
	}

	?>
<div class="woocommerce-billing-fields__field-wrapper">
    <p class="form-row form-row-first validate-required" id="billing_first_name_field" data-priority="10">
        <label for="billing_first_name" class="">Imię&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_first_name" id="billing_first_name" placeholder="" value=""
                autocomplete="given-name"></span></p>
    <p class="form-row form-row-last validate-required" id="billing_last_name_field" data-priority="20">
        <label for="billing_last_name" class="">Nazwisko&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_last_name" id="billing_last_name" placeholder="" value=""
                autocomplete="family-name"></span></p>
    <p class="form-row form-row-wide" id="billing_company_field" data-priority="30">
        <label for="billing_company" class="">Nazwa firmy&nbsp;<span class="optional">(opcjonalne)</span></label><span
            class="woocommerce-input-wrapper"><input type="text" class="input-text " name="billing_company"
                id="billing_company" placeholder="" value="" autocomplete="organization"></span></p>
    <p class="form-row form-row-wide address-field update_totals_on_change validate-required" id="billing_country_field"
        data-priority="40">
        <label for="billing_country" class="">Kraj&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><select
                name="billing_country" id="billing_country" class="country_to_state country_select "
                autocomplete="country" tabindex="-1" aria-hidden="true">
                <option value="">Wybierz kraj…</option>
                <option value="AF">Afganistan</option>
                <option value="AL">Albania</option>
                <option value="DZ">Algieria</option>
                <option value="AD">Andora</option>
                <option value="AO">Angola</option>
                <option value="AI">Anguilla</option>
                <option value="AQ">Antarktyda</option>
                <option value="AG">Antigua i Barbuda</option>
                <option value="SA">Arabia Saudyjska</option>
                <option value="AR">Argentyna</option>
                <option value="AM">Armenia</option>
                <option value="AW">Aruba</option>
                <option value="AU">Australia</option>
                <option value="AT">Austria</option>
                <option value="AZ">Azerbejdżan</option>
                <option value="BS">Bahamy</option>
                <option value="BH">Bahrajn</option>
                <option value="BD">Bangladesz</option>
                <option value="BB">Barbados</option>
                <option value="PW">Belau</option>
                <option value="BE">Belgia</option>
                <option value="BZ">Belize</option>
                <option value="BJ">Benin</option>
                <option value="BM">Bermudy</option>
                <option value="BT">Bhutan</option>
                <option value="BY">Białoruś</option>
                <option value="MM">Birma</option>
                <option value="BO">Boliwia</option>
                <option value="BQ">Bonaire, Saint Eustatius i Saba</option>
                <option value="BW">Botswana</option>
                <option value="BV">Bouvet Island</option>
                <option value="BA">Bośnia i Hercegowina</option>
                <option value="BR">Brazylia</option>
                <option value="BN">Brunei</option>
                <option value="IO">Brytyjskie Terytorium Oceanu Indyjskiego</option>
                <option value="VG">Brytyjskie Wyspy Dziewicze</option>
                <option value="BF">Burkina Faso</option>
                <option value="BI">Burundi</option>
                <option value="BG">Bułgaria</option>
                <option value="CL">Chile</option>
                <option value="CN">Chiny</option>
                <option value="HR">Chorwacja</option>
                <option value="CW">Curaçao</option>
                <option value="CY">Cypr</option>
                <option value="TD">Czad</option>
                <option value="ME">Czarnogóra</option>
                <option value="CZ">Czechy</option>
                <option value="UM">Dalekie Wyspy Mniejsze Stanów Zjednoczonych</option>
                <option value="DK">Dania</option>
                <option value="CD">Demokratyczna Republika Konga</option>
                <option value="DM">Dominika</option>
                <option value="DO">Dominikana</option>
                <option value="DJ">Dżibuti</option>
                <option value="EG">Egipt</option>
                <option value="EC">Ekwador</option>
                <option value="ER">Erytrea</option>
                <option value="EE">Estonia</option>
                <option value="ET">Etiopia</option>
                <option value="FK">Falklandy</option>
                <option value="FJ">Fidżi</option>
                <option value="PH">Filipiny</option>
                <option value="FI">Finlandia</option>
                <option value="FR">Francja</option>
                <option value="TF">Francuskie Terytoria Południowe i Antarktyczne</option>
                <option value="GA">Gabon</option>
                <option value="GM">Gambia</option>
                <option value="GS">Georgia Południowa i Sandwich Południowy</option>
                <option value="GH">Ghana</option>
                <option value="GI">Gibraltar</option>
                <option value="GR">Grecja</option>
                <option value="GD">Grenada</option>
                <option value="GL">Grenlandia</option>
                <option value="GE">Gruzja</option>
                <option value="GU">Guam</option>
                <option value="GG">Guernsey</option>
                <option value="GY">Gujana</option>
                <option value="GF">Gujana Francuska</option>
                <option value="GP">Gwadelupa</option>
                <option value="GT">Gwatemala</option>
                <option value="GN">Gwinea</option>
                <option value="GW">Gwinea Bissau</option>
                <option value="GQ">Gwinea Równikowa</option>
                <option value="HT">Haiti</option>
                <option value="ES">Hiszpania</option>
                <option value="NL">Holandia</option>
                <option value="HN">Honduras</option>
                <option value="HK">Hongkong</option>
                <option value="IN">Indie</option>
                <option value="ID">Indonezja</option>
                <option value="IQ">Irak</option>
                <option value="IR">Iran</option>
                <option value="IE">Irlandia</option>
                <option value="IS">Islandia</option>
                <option value="IL">Izrael</option>
                <option value="JM">Jamajka</option>
                <option value="JP">Japonia</option>
                <option value="YE">Jemen</option>
                <option value="JE">Jersey</option>
                <option value="JO">Jordania</option>
                <option value="KY">Kajmany</option>
                <option value="KH">Kambodża</option>
                <option value="CM">Kamerun</option>
                <option value="CA">Kanada</option>
                <option value="QA">Katar</option>
                <option value="KZ">Kazachstan</option>
                <option value="KE">Kenia</option>
                <option value="KG">Kirgistan</option>
                <option value="KI">Kiribati</option>
                <option value="CO">Kolumbia</option>
                <option value="KM">Komory</option>
                <option value="CG">Kongo</option>
                <option value="CR">Kostaryka</option>
                <option value="CU">Kuba</option>
                <option value="KW">Kuwejt</option>
                <option value="LA">Laos</option>
                <option value="LS">Lesotho</option>
                <option value="LB">Liban</option>
                <option value="LR">Liberia</option>
                <option value="LY">Libia</option>
                <option value="LI">Liechtenstein</option>
                <option value="LT">Litwa</option>
                <option value="LU">Luksemburg</option>
                <option value="MK">Macedonia</option>
                <option value="MG">Madagaskar</option>
                <option value="YT">Majotta</option>
                <option value="MO">Makau</option>
                <option value="MW">Malawi</option>
                <option value="MV">Malediwy</option>
                <option value="MY">Malezja</option>
                <option value="ML">Mali</option>
                <option value="MT">Malta</option>
                <option value="MP">Mariany Północne</option>
                <option value="MA">Maroko</option>
                <option value="MQ">Martynika</option>
                <option value="MR">Mauretania</option>
                <option value="MU">Mauritius</option>
                <option value="MX">Meksyk</option>
                <option value="FM">Mikronezja</option>
                <option value="MC">Monako</option>
                <option value="MN">Mongolia</option>
                <option value="MS">Montserrat</option>
                <option value="MZ">Mozambik</option>
                <option value="MD">Mołdawia</option>
                <option value="NA">Namibia</option>
                <option value="NR">Nauru</option>
                <option value="NP">Nepal</option>
                <option value="DE">Niemcy</option>
                <option value="NE">Niger</option>
                <option value="NG">Nigeria</option>
                <option value="NI">Nikaragua</option>
                <option value="NU">Niue</option>
                <option value="NO">Norwegia</option>
                <option value="NC">Nowa Kaledonia</option>
                <option value="NZ">Nowa Zelandia</option>
                <option value="OM">Oman</option>
                <option value="PK">Pakistan</option>
                <option value="PS">Palestyna</option>
                <option value="PA">Panama</option>
                <option value="PG">Papua-Nowa Gwinea</option>
                <option value="PY">Paragwaj</option>
                <option value="PE">Peru</option>
                <option value="PN">Pitcairn</option>
                <option value="PF">Polinezja Francuska</option>
                <option value="PL" selected="selected">Polska</option>
                <option value="PR">Portoryko</option>
                <option value="PT">Portugalia</option>
                <option value="KR">Południowa Korea</option>
                <option value="KP">Północna Korea</option>
                <option value="CF">Republika Południowej Afryki</option>
                <option value="ZA">Republika Południowej Afryki</option>
                <option value="CV">Republika Zielonego Przylądka</option>
                <option value="RE">Reunion</option>
                <option value="RU">Rosja</option>
                <option value="RO">Rumunia</option>
                <option value="RW">Rwanda</option>
                <option value="EH">Sahara Zachodnia</option>
                <option value="KN">Saint Kitts i Nevis</option>
                <option value="LC">Saint Lucia</option>
                <option value="SX">Saint Martin (część Holenderska)</option>
                <option value="VC">Saint Vincent i Grenadyny</option>
                <option value="BL">Saint-Barthélemy</option>
                <option value="MF">Saint-Martin</option>
                <option value="PM">Saint-Pierre i Miquelon</option>
                <option value="SV">Salwador</option>
                <option value="WS">Samoa</option>
                <option value="AS">Samoa Amerykańskie</option>
                <option value="SM">San Marino</option>
                <option value="SN">Senegal</option>
                <option value="RS">Serbia</option>
                <option value="SC">Seszele</option>
                <option value="SL">Sierra Leone</option>
                <option value="SG">Singapur</option>
                <option value="SO">Somalia</option>
                <option value="LK">Sri Lanka</option>
                <option value="US">Stany Zjednoczone</option>
                <option value="SZ">Suazi</option>
                <option value="SD">Sudan</option>
                <option value="SS">Sudan Południowy</option>
                <option value="SR">Surinam</option>
                <option value="SJ">Svalbard i Jan Mayen</option>
                <option value="SY">Syria</option>
                <option value="CH">Szwajcaria</option>
                <option value="SE">Szwecja</option>
                <option value="SK">Słowacja</option>
                <option value="SI">Słowenia</option>
                <option value="TJ">Tadżykistan</option>
                <option value="TH">Tajlandia</option>
                <option value="TW">Tajwan</option>
                <option value="TZ">Tanzania</option>
                <option value="TL">Timor Wschodni</option>
                <option value="TG">Togo</option>
                <option value="TK">Tokelau</option>
                <option value="TO">Tonga</option>
                <option value="TT">Trinidad i Tobago</option>
                <option value="TN">Tunezja</option>
                <option value="TR">Turcja</option>
                <option value="TM">Turkmenistan</option>
                <option value="TC">Turks i Caicos</option>
                <option value="TV">Tuvalu</option>
                <option value="UG">Uganda</option>
                <option value="UA">Ukraina</option>
                <option value="UY">Urugwaj</option>
                <option value="UZ">Uzbekistan</option>
                <option value="VU">Vanuatu</option>
                <option value="WF">Wallis i Futuna</option>
                <option value="VA">Watykan</option>
                <option value="VE">Wenezuela</option>
                <option value="GB">Wielka Brytania</option>
                <option value="VN">Wietnam</option>
                <option value="CI">Wybrzeże Kości Słoniowej</option>
                <option value="IM">Wyspa Man</option>
                <option value="NF">Wyspa Norfolk</option>
                <option value="CX">Wyspa Wielkanocna</option>
                <option value="SH">Wyspa Świętej Heleny</option>
                <option value="AX">Wyspy Alandzkie</option>
                <option value="CK">Wyspy Cooka</option>
                <option value="VI">Wyspy Dziewicze Stanów Zjednoczonych</option>
                <option value="HM">Wyspy Heard i McDonalda</option>
                <option value="CC">Wyspy Kokosowe</option>
                <option value="MH">Wyspy Marshalla</option>
                <option value="FO">Wyspy Owcze</option>
                <option value="SB">Wyspy Salomona</option>
                <option value="ST">Wyspy Świętego Tomasza i Książęca</option>
                <option value="HU">Węgry</option>
                <option value="IT">Włochy</option>
                <option value="ZM">Zambia</option>
                <option value="ZW">Zimbabwe</option>
                <option value="AE">Zjednoczone Emiraty Arabskie</option>
                <option value="LV">Łotwa</option>
            </select></p>
    <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50">
        <label for="billing_address_1" class="">Ulica&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_address_1" id="billing_address_1"
                placeholder="Nazwa ulicy, numer budynku / numer lokalu" value="" autocomplete="address-line1"></span>
    </p>
    <p class="form-row form-row-wide address-field" id="billing_address_2_field" data-priority="60">
        <label for="billing_address_2" class="screen-reader-text">Nr mieszkania, lokalu, itp.&nbsp;<span
                class="optional">(opcjonalne)</span></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_address_2" id="billing_address_2"
                placeholder="Ciąg dalszy adresu (opcjonalnie)" value="" autocomplete="address-line2"></span></p>
    <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70"
        data-o_class="form-row form-row-wide address-field validate-required">
        <label for="billing_city" class="">Miasto&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_city" id="billing_city" placeholder="" value=""
                autocomplete="address-level2"></span></p>
    <p class="form-row form-row-wide address-field validate-state" id="billing_state_field" style="display: none"
        data-o_class="form-row form-row-wide address-field validate-state">
        <label for="billing_state" class="">Województwo / Region&nbsp;<span
                class="optional">(opcjonalne)</span></label><span class="woocommerce-input-wrapper"><input type="hidden"
                class="hidden" name="billing_state" id="billing_state" value="" autocomplete="address-level1"
                placeholder="" readonly="readonly"></span></p>
    <p class="form-row form-row-wide address-field validate-required validate-postcode" id="billing_postcode_field"
        data-priority="90" data-o_class="form-row form-row-wide address-field validate-required validate-postcode">
        <label for="billing_postcode" class="">Kod pocztowy&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text"
                class="input-text " name="billing_postcode" id="billing_postcode" placeholder="" value=""
                autocomplete="postal-code"></span></p>
    <p class="form-row form-row-wide validate-required validate-phone" id="billing_phone_field" data-priority="100">
        <label for="billing_phone" class="">Telefon&nbsp;<abbr class="required"
                title="pole wymagane">*</abbr></label><span class="woocommerce-input-wrapper"><input type="tel"
                class="input-text " name="billing_phone" id="billing_phone" placeholder="" value=""
                autocomplete="tel"></span></p>
</div>

<div class="wrapp-justify">
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <!-- Copyright mWS myWorld Solutions AG 2017 -->
        <img src="https://s.lyoness.tv/communication/official-cashback-partner-logo-web-cbw_150x102.png"
            title="Official Partner CBW" alt="Official Partner CBW" />
        <br>
        <label>
            <input type="radio" required class="woocommerce-Input woocommerce-Input--text" name="konto_cashback"
                id="nowe_konto" value="2">Chcę założyć konto i kupować taniej w programie Bathbee Cashback</label>
        <label>
            <input type="radio" required class="woocommerce-Input woocommerce-Input--text" name="konto_cashback"
                id="mam_konto" value="1"> Mam już konto w programie lojalnościowym w sieci Cashback World</label>

    </p>
    <div id="tab_mam_konto" style="display: none">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <label for="numer_karty_cashback">Identyfikacja w sieci Cashback World:(Numer ID typu 048.000.XXX.XXX lub
                Numer Karty Cashback)&nbsp;<span class="required">*</span></label>
            <input type="text" class="woocommerce-Input woocommerce-Input--text " name="numer_karty_cashback"
                id="numer_karty_cashback" autocomplete="" value=""> </p>
    </div>
    <div id="tab_nowe_konto" style="display: none">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="data_urodzenia">Podaj datę urodzenia&nbsp;<span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text datepicker-here"
                    data-language="pl" name="data_urodzenia" id="data_urodzenia" autocomplete="" value=""> </p>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                <label>
                    <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                        name="zaznacz_wszystko" id="zaznacz_wszystko" value="1">&nbsp;Zaznacz wszystko</label>
            </p>

            <div class="wrapp-rodo">
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_przystapienie_bathbee_cashback" id="zgoda_przystapienie_bathbee_cashback"
                            value="1">&nbsp;<span class="required">*</span> Chcę dołączyć do programu Bathbee
                        Cashback</label>
                </p>


                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_owu" id="zgoda_owu" value="1">&nbsp;<span class="required">*</span>Tak,
                        przeczytałem
                        i
                        akceptuję <a
                            href="https://l.lyocdn.com/large/download/public/agreements/cbw/pl/pl_cbw_gtc_2017_11.pdf">Ogólne
                            Warunki Umów.</a>w celu uczestnictwa w Programie Cashback World od myWorld.</label>
                </p>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_przetwarzanie_danych" id="zgoda_przetwarzanie_danych" value="1">&nbsp;<span
                            class="required">*</span>Przeczytałem i akceptuję <a
                            href="https://beta.bathbee.pl/wp-content/uploads/2019/04/pl_cashbackworld_com_dataprivacypolicy_2018_05.pdf">Oświadczenie
                            o ochronie danych osobowych</a> oraz <a
                            href="https://beta.bathbee.pl/wp-content/uploads/2019/04/pl_cbw_consentunderdataprotectionlaw_leag_2018_05.pdf">
                            Zgodę na przetwarzanie danych osobowych.</a></label>
                </p>


                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_przystapienie_cashback" id="zgoda_przystapienie_cashback" value="1">&nbsp;<span
                            class="required">*</span> Wyrażam zgodę na przetwarzanie moich danych osobowych w celu
                        przystąpienia
                        do Cashback World. Administratorem danych jest mWS myWorld Solutions AG, przedstawicielem na
                        terenie
                        Polski jest myWorld Poland Sp. z o.o., ul. Gen. Bohdana Zielińskiego 22, 30-320 Kraków</label>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_przystapienie_cashback" id="zgoda_przystapienie_cashback" value="1">&nbsp;<span
                            class="required">*</span>
                        Zapoznałem/am się z pouczeniem dotyczącym prawa dostępu do treści moich danych i możliwości ich
                        poprawiania. Jestem świadom/a, iż moja zgoda może być odwołana w każdym czasie, co skutkować
                        będzie
                        usunięciem danych z Programu Cashback</label>
                </p>


                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_przesylanie_informacji" id="zgoda_przesylanie_informacji" value="1">&nbsp;<span
                            class="required">*</span>
                        Tak, wyrażam zgodę, która może być w każdym momencie odwołana, by moje dane osobowe były
                        przetwarzane
                        przez myWorld Poland Sp. z o.o. celem identyfikacji i przesyłania mi spersonalizowanych
                        informacji,
                        a
                        także kontaktu drogą pocztową i osobistego ze strony myWorld Poland Sp. z o.o. przez telefon lub
                        wiadomości Push, celem reklamowania Programu Cashback World oraz ofert Partnerów Handlowych
                    </label>
                </p>


                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_rozszerzony_program_lojalnosciowy" id="zgoda_rozszerzony_program_lojalnosciowy"
                            value="1">&nbsp;<span class="required">*</span>Tak, biorę udział w rozszerzonym programie
                        lojalnościowym Partnera Handlowego i niniejszym akceptuję <a
                            href="<?=home_url('/');?>/wp-content/uploads/2019/02/pl_cashbackworld_com_dataprivacypolicy_2017_11.pdf">Zgodę
                            na przekazanie danych.</a></label>
                </p>


                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_partner_marketing" id="zgoda_partner_marketing" value="1">&nbsp;

                        Tak, wyrażam zgodę na przekazywanie Partnerowi Handlowemu przez myWorld Poland Sp. z o.o., w
                        związku
                        z
                        moim udziałem w indywidualnym programie lojalnościowym Partnera Handlowego moich danych
                        osobowych,
                        aby
                        Partner Handlowy mógł wykorzystywać te dane do celów marketingowych: kontaktowania się drogą
                        elektroniczną (np. poprzez e-mail lub SMS), poprzez media społecznościowe (takie jak Facebook
                        etc.),
                        przez fax, przez telefon lub listownie, w celu przekazywania informacji o produktach i akcjach
                        handlowych Partnera Handlowego, do informowania o ofertach, które odpowiadają moim
                        zainteresowaniom,
                        jak
                        również do przeprowadzenia badań satysfakcji klienta. </label>
                </p>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_newsletter" id="zgoda_newsletter" value="1">&nbsp;Tak, chcę otrzymywać
                        informacje
                        drogą
                        elektroniczną poprzez newsletter, SMS lub e-mail.</label>
                </p>



                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

                    <label>
                        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
                            name="zgoda_sledzenie_aktywnosci" id="zgoda_sledzenie_aktywnosci" value="1">&nbsp;<span
                            class="required">*</span>Wyrażam niniejszym zgodę - odwołalną w każdym czasie - że myWorld
                        Poland
                        Sp. z o.o. udostępni mojemu bezpośredniemu rekomendującemu oraz jego osobie rekomendującej
                        (zwanej
                        dalej
                        "pośrednim rekomendującym") następujące dane do prześledzenia aktywności zakupowej Uczestnika w
                        celach
                        rozliczeniowych: data zakupu, godzina zakupu, imię i nazwisko, kwota i obrót zakupowy, waluta.
                        Pośredniemu rekomendującemu wyświetlać się będzie tylko imię i pierwsza litera nazwiska.
                    </label>
                </p>





                <p>Swoją zgodę(zgody) (także każdą oddzielnie) na powyższe przetwarzanie danych mogę odwołać w każdym
                    czasie
                    pisemnie (na adres do korespondencji: myWorld Poland Sp. z o.o., ul. Gen. Bohdana Zielińskiego 22,
                    30-320
                    Kraków) lub e-mailem na adres service.pl@cashbackworld.com Przetwarzanie danych dokonane do momentu
                    odwołania pozostaje na podstawie niniejszej zgody zgodne z prawem</p>

            </div>
    </div>
</div>
<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">

    <label>
        <input type="checkbox" class="woocommerce-Input woocommerce-Input--checkbox input-checkbox"
            name="zgoda_regulamin" id="zgoda_regulamin" value="1">&nbsp;<span class="required">*</span>Zapoznałem się z
        <a href="<?=home_url('/');?>/regulaminy/">regulaminem sklepu internetowego</a> i akceptuję jego treść.</label>
</p>

<?php
 }
 add_action( 'woocommerce_register_form', 'wooc_extra_register_fields' );

function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
	 if ($_POST['billing_first_name'] == '') {
             $validation_errors->add( 'billing_first_name_error', "Imie jest wymagane" );
      }
       if ($_POST['billing_last_name'] == '') {
             $validation_errors->add( 'billing_last_name_error', "Nazwisko jest wymagane" );
      }
       if ($_POST['billing_country'] == '') {
             $validation_errors->add( 'billing_country_error', "Kraj jest wymagany" );
      }
       if ($_POST['billing_address_1'] == '') {
             $validation_errors->add( 'billing_address_1_error', "Ulica jest wymagana" );
      }
       if ($_POST['billing_city'] == '') {
             $validation_errors->add( 'billing_city_error', "Miasto jest wymagane" );
      }
       if ($_POST['billing_postcode'] == '') {
             $validation_errors->add( 'billing_postcode_error', "Kod pocztowy jest wymagany" );
      }
      if ($_POST['billing_phone'] == '') {
             $validation_errors->add( 'billing_phone_error', "Telefon jest wymagany" );
      }

      if ($_POST['konto_cashback'] == 2) {
      		 if ( $_POST['numer_karty_cashback'] == "") {
             $validation_errors->add( 'numer_konta_cashback_error', "Numer konta cashback jest wymagany" );
      }
      }
      if ($_POST['konto_cashback'] == 1) {

      if ( !isset( $_POST['zgoda_cashback'] ) ) {
             $validation_errors->add( 'zgoda_cashback_error', "Zapoznanie z pouczeniem dotyczącym prawa dostępu do treści jest wymagane" );
      }

       if ( !isset( $_POST['zgoda_przystapienie_cashback'] ) ) {
             $validation_errors->add( 'zgoda_przystapienie_cashback_error', "Zapoznanie na przetwarzanie danych  w celu przystąpienia do programu Cashback jest wymagane" );
      }

       if ( !isset( $_POST['zgoda_bathbee_cashback'] ) ) {
             $validation_errors->add( 'zgoda_bathbee_cashback_error', "Uczestniczenie w programie Bathbee Cashback jest wymagane" );
      }

       if ( !isset( $_POST['zgoda_przystapienie_bathbee_cashback'] ) ) {
             $validation_errors->add( 'zgoda_przystapienie_bathbee_cashback_error', "Zapoznanie przystąpienia do programu Bathbee Cashback jest wymagana" );
      }

       if ( !isset( $_POST['zgoda_newsletter'] ) ) {
             $validation_errors->add( 'zgoda_newsletter_error', "Zgoda na newsletter jest wymagana" );
      }

    

       if ( !isset( $_POST['data_urodzenia'] ) ) {
             $validation_errors->add( 'data_urodzenia_error', "Data urodzenia jest wymagana" );
      }

       if ( !isset( $_POST['zgoda_rozszerzony_program_lojalnosciowy'] ) ) {
             $validation_errors->add( 'zgoda_rozszerzony_program_lojalnosciowy_error', "Zgoda na rozszerzony program rojalnościowy jest wymagana" );
      }

 if ( !isset( $_POST['zgoda_owu'] ) ) {
             $validation_errors->add( 'zgoda_owu_error', "Zapoznanie i akceptacja OWU jest wymagana" );
      }

  if ( !isset( $_POST['zgoda_przetwarzanie_danych'] ) ) {
             $validation_errors->add( 'zgoda_przetwarzanie_danych_error', "Zgoda na przetwarzanie danych jest wymagana" );
      }

        if ( !isset( $_POST['zgoda_sledzenie_aktywnosci'] ) ) {
             $validation_errors->add( 'zgoda_sledzenie_aktywnosci_error', "Zgoda na śledzenie aktywności jest wymagana" );
      }

        if ( !isset( $_POST['zgoda_przesylanie_informacji'] ) ) {
             $validation_errors->add( 'zgoda_przesylanie_informacji_error', "Zgoda na przetwrzesyłanie informacji jest wymagana" );
      }

    
}
     

  
       if ( !isset( $_POST['zgoda_regulamin'] ) ) {
             $validation_errors->add( 'zgoda_regulamin_error', "Akceptacja regulaminu jest wymagana" );
      
     }
  
  
  return $validation_errors;
}
add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );


function wooc_save_extra_register_fields( $customer_id ) {

	update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));

	update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));

	update_user_meta($customer_id, 'billing_country', sanitize_text_field($_POST['billing_country']));

	update_user_meta($customer_id, 'billing_address_1', sanitize_text_field($_POST['billing_address_1']));

update_user_meta($customer_id, 'billing_address_2', sanitize_text_field($_POST['billing_address_1']));

	update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));

	update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));

	update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));

	update_user_meta($customer_id, 'billing_company', sanitize_text_field($_POST['billing_company']));

	update_user_meta($customer_id, 'numer_karty_cashback', sanitize_text_field($_POST['numer_karty_cashback']));
		
		if ($_POST['numer_karty_cashback'] == "") {

    if ( isset( $_POST['zgoda_cashback'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_cashback', 1 );
                 update_user_meta( $customer_id, 'zgoda_cashback_time', time() );
          }
       if ( isset( $_POST['zgoda_przystapienie_cashback'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_przystapienie_cashback', 1 );
                 update_user_meta( $customer_id, 'zgoda_przystapienie_cashback_time', time() );
          }
            if ( isset( $_POST['zgoda_bathbee_cashback'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_bathbee_cashback', 1 );
                 update_user_meta( $customer_id, 'zgoda_bathbee_cashback_time', time() );
          }
            if ( isset( $_POST['zgoda_przystapienie_bathbee_cashback'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_przystapienie_bathbee_cashback', 1 );
                 update_user_meta( $customer_id, 'zgoda_przystapienie_bathbee_cashback_time', time() );
          }
            if ( isset( $_POST['zgoda_newsletter'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_newsletter', 1 );
                 update_user_meta( $customer_id, 'zgoda_newsletter_time', time() );
          }

            if ( isset( $_POST['zgoda_owu'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_owu', 1 );
                 update_user_meta( $customer_id, 'zgoda_owu_time', time() );
          }

            if ( isset( $_POST['data_urodzenia'] ) ) {
                
                 update_user_meta( $customer_id, 'data_urodzenia', 1 );
                 update_user_meta( $customer_id, 'data_urodzenia_time', time() );
          }
            if ( isset( $_POST['zgoda_rozszerzony_program_lojalnosciowy'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_rozszerzony_program_lojalnosciowy', 1 );
                 update_user_meta( $customer_id, 'zgoda_rozszerzony_program_lojalnosciowy_time', time() );
          }
            if ( isset( $_POST['zgoda_przetwarzanie_danych'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_przetwarzanie_danych', 1 );
                 update_user_meta( $customer_id, 'zgoda_przetwarzanie_danych_time', time() );
          }

            if ( isset( $_POST['zgoda_przesylanie_informacji'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_przesylanie_informacji', 1 );
                 update_user_meta( $customer_id, 'zgoda_przesylanie_informacji_time', time() );
          }

            if ( isset( $_POST['zgoda_sledzenie_aktywnosci'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_sledzenie_aktywnosci', 1 );
                 update_user_meta( $customer_id, 'zgoda_sledzenie_aktywnosci_time', time() );
          }
            if ( isset( $_POST['zgoda_regulamin'] ) ) {
                
                 update_user_meta( $customer_id, 'zgoda_regulamin', 1 );
                 update_user_meta( $customer_id, 'zgoda_regulamin_time', time() );
          }
      }
}
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );


add_shortcode('generateGmaps', 'generateGmaps');
function generateGmaps()
{
    global $wpdb;

    $voivodeships = $wpdb->get_results("SELECT id, name FROM Voivodeship");

    $data = [];
    if ($voivodeships) {

        foreach ($voivodeships as $voivodeship) {

            $points = $wpdb->get_results(
                "SELECT DISTINCT address.*, point.name, role.name as role_name, role.identifier FROM address, point_role, point, role WHERE Point.isActive = 1 AND point.id = point_role.pointId and role.id = point_role.roleId AND Address.id = Point.addressId AND Address.VoivodeshipId =  ".$voivodeship->id
            );

            if ($points) {

                foreach ($points as $point) {
                    $house   = (is_null(
                        $point->apartmentNumber
                    )) ? $point->houseNumber : $point->houseNumber.'/'.$point->apartmentNumber;
                    $address = $point->city.' '.$point->street.' '.$house;
                   $data[$voivodeship->name][] = ['name' => strtolower($point->name), 'address' => strtolower($address), 'type'=>strtolower($point->role_name), 'identifier'=>strtolower($point->identifier), 'lat'     => strtolower($point->gmapLat),
                                                  'len'     => strtolower($point->gmapLen),];
                }
            } else {
                $data[$voivodeship->name] = [];
            }

        }

    }
    add_action('wp_footer', function () use ($data) {
        ?>
<script>
window.points = <?=json_encode($data);?>;
</script>
<?php
    });

    return;
}

add_action('wp_footer', function() {
	global $post;

	if (is_page() && has_shortcode($post->post_content, 'generateGmaps')) {
		generateGmapsScripts();
	}

	if (is_account_page()) {
		?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $("#zaznacz_wszystko").click(function() {
        if ($(this).prop("checked")) {
            $('input[type="checkbox"]').attr("checked", "checked");
        }
    });
    $("input[type='checkbox']").click(function() {
        if (!$(this).prop("checked")) {
            $('#zaznacz_wszystko').removeAttr("checked");
        }
    });
    $('.tab_button').click(function(ev) {
        ev.preventDefault();
        $('.tab.tab-active').removeClass('tab-active');
        $($(this).attr("href")).addClass('tab-active');
    });
});
</script>
<!-- } -->

<!-- if (is_cart()) { -->
<!-- ?> -->
<script>
jQuery(document).ready(function($) {
    $('body').on('click', 'a[href="#addCoupon"]', function(ev) {
        ev.preventDefault();
        <?php
							if (is_user_logged_in()) {
						?>

        $('td.actions .coupon').toggle();

        <?php } else { ?>

        window.location.href = "<?=get_permalink(5740);?>#registerForm";
        <?php } ?>
    });
    /*
    					$('body').on('click', "#zastosuj", function(ev) {
    						ev.preventDefault();

    						$('#coupon_code').val($("#kupon").val());
    						setTimeout(function() {$('button[name="apply_coupon"]').trigger('click');}, 500);
    						SPU.hide(6012);
    					});*/
});
</script>
<?php
	}

		if (is_checkout()) {
		?>
<script>
jQuery(document).ready(function($) {
    if ($("#invoice-checkbox").prop("checked")) {
        $("#invoice_fields").show();
    } else {
        $("#invoice_fields").hide();
    }

    $("#invoice-checkbox").change(function() {
        if ($("#invoice-checkbox").prop("checked")) {
            $("#invoice_fields").show();
        } else {
            $("#invoice_fields").hide();
        }
    });


    $(document.body).on('updated_checkout', function() {
        if ($("#shipping_method input:checked").val() != "local_pickup:1") {
            $("local_pickup").hide();
        } else {
            $("local_pickup").show();
        }

        if ($("#shipping_method input:checked").val() != "local_pickup:1") {
            $("#local_pickup").hide();
        } else {
            $("#local_pickup").show();
        }



    });

    $('body').on("change", "#shipping_method input", function() {
        if ($("#shipping_method input:checked").val() != "local_pickup:1") {
            $("local_pickup").hide();
        } else {
            $("local_pickup").show();
        }

    });
});
</script>
<?php
	}

	if (is_page(5740)) {
		?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/i18n/datepicker.pl.min.js"></script>
<script>
jQuery(document).ready(function($) {
    if ($("input[name=\"konto_cashback\"]:checked").val() == 1) {

        $('#tab_nowe_konto').hide();
        $('#tab_mam_konto').show();

    } else if ($("input[name=\"konto_cashback\"]:checked").val() == 2) {
        $('#tab_nowe_konto').show();
        $('#tab_mam_konto').hide();
    }

    $("input[name=\"konto_cashback\"]").change(function() {
        if ($("input[name=\"konto_cashback\"]:checked").val() == 1) {

            $('#tab_nowe_konto').hide();
            $('#tab_mam_konto').show();

        } else if ($("input[name=\"konto_cashback\"]:checked").val() == 2) {
            $('#tab_nowe_konto').show();
            $('#tab_mam_konto').hide();
        }
    });
});
</script>


<?php
	}
	?>

<script>
$("#pobierzKupon").click(function(ev) {
    ev.preventDefault();
    $("#ri-widget div img").trigger('click');
});
</script><?php
});

add_action('woocommerce_register_form_tag', function() {
echo "id=\"registerForm\"";
});


function custom_woocommerce_login_redirect_to_checkout_page() {

  // Caso 1: Usuario no identificado intenta acceder a Finalizar Compra
    if ( !is_user_logged_in() && is_checkout() && isset($_GET['bathbee_cashback']) ) {
        wp_redirect( get_permalink( get_page_by_path('moje-konto') )."?bathbee_cashback=1" );

	}


/*
  // Caso 2: Si en la página de identificarse, el cliente ha accedido o se ha registrado, o tiene el carrito vacío, continuar a Finalizar Compra.
  if ( is_page('iniciar-sesion-registrarse') ) {
    if( is_user_logged_in() || WC()->cart->is_empty() ) {
        wp_redirect( get_permalink( get_page_by_path('finalizar-comprar') ) );
  }
  }*/
}
add_action( 'wp', 'custom_woocommerce_login_redirect_to_checkout_page' );



/*add_shortcode('woo_register', function() {

ob_start();
do_action( 'woocommerce_before_customer_login_form' ); ?>



<h2><?php esc_html_e( 'Register', 'woocommerce' ); ?></h2>

<form method="post" class="woocommerce-form woocommerce-form-register register"
    <?php do_action( 'woocommerce_register_form_tag' ); ?>>
    <input type="hidden" name="register_cashback" value="1">
    <?php do_action( 'woocommerce_register_form_start' ); ?>

    <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span
                class="required">*</span></label>
        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
            id="reg_username" autocomplete="username"
            value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
    </p>

    <?php endif; ?>

    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span
                class="required">*</span></label>
        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email"
            autocomplete="email"
            value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
    </p>

    <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
        <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span
                class="required">*</span></label>
        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password"
            id="reg_password" autocomplete="new-password" />
    </p>

    <?php endif; ?>

    <?php do_action( 'woocommerce_register_form' ); ?>

    <p class="woocommerce-FormRow form-row">
        <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
        <button type="submit" class="woocommerce-Button button" name="register"
            value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
    </p>

    <?php do_action( 'woocommerce_register_form_end' ); ?>

</form>
<?php

$output = ob_get_clean();
return $output;
});*/

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { 
	if (current_user_can('manage_options')) { ?>
<h3><?php _e("Zgody RODO", "blank"); ?></h3>

<table class="form-table">
    <tr>
        <th>Zgoda Cashback</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_cashback_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda przystapienie do Cashback</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_przystapienie_cashback_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda Bathbee cashback</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_bathbee_cashback_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda na przystapienie do Bathbee cashback</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_przystapienie_bathbee_cashback_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda na newsletter</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_newsletter_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda OWU</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_owu_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda na przetwarzanie danych</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_przetwarzanie_danych_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda na rozszerzony program lojalnościowy</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_rozszerzony_program_lojalnosciowy_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Zgoda na przesyłanie informacji</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_przesylanie_informacji_time', true)); ?></span>
        </td>
    </tr>


    <tr>
        <th>Zgoda na śledzenie aktywności</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_sledzenie_aktywnosci_time', true)); ?></span>
        </td>
    </tr>

    <tr>
        <th>Akceptacja regulaminu</th>
        <td>

            <span><?php echo date('d m Y H:i', get_user_meta($user->ID, 'zgoda_regulamin_time', true)); ?></span>
        </td>
    </tr>


</table>
<?php } }

add_action('woocommerce_review_order_after_order_total', function() {
	?>
<tr>
    <th>
        Zwrot cashback
    </th>
    <td>
        <span id="cashback"><?=calculateCashback(WC()->cart->cart_contents_total);?></span> zł
    </td>
</tr>
<tr>
    <th>
        punkty SP
    </th>
    <td><span id="sp"><?=calculateSP(WC()->cart->cart_contents_total);?></span></td>
</tr>

<?php
});


add_action('woocommerce_checkout_process', 'company_checkout_field_process');
function company_checkout_field_process()
{
  if ($_POST['_invoice'] == 1) {
	  if ($_POST['_billing_company_street'] == "") { wc_add_notice(__('Ulica jest wymagana.') , 'error'); }
 	  if ($_POST['_billing_company_postcode'] == "") { wc_add_notice(__('Kod pocztowy jest wymagany.') , 'error'); } 
	  if ($_POST['_billing_nip'] == "") { wc_add_notice(__('NIP jest wymagany.') , 'error'); }
 	  if ($_POST['_billing_company_city'] == "") { wc_add_notice(__('Miasto jest wymagane') , 'error'); }
 	  if ($_POST['_billing_company'] == "") { wc_add_notice(__('Nazwa firmy jest wymagana.') , 'error'); } 
 
	}
}


add_action('woocommerce_checkout_update_order_meta', 'company_checkout_field_update_order_meta');
function company_checkout_field_update_order_meta($order_id)
{
  
     if ($_POST['_invoice'] == 1) {
     	update_post_meta($order_id, '_invoice', 1);
	  	update_post_meta($order_id,'_billing_company_street', sanitize_text_field($_POST['_billing_company_street']));
	  	update_post_meta($order_id,'_billing_company_postcode', sanitize_text_field($_POST['_billing_company_postcode']));

	  	update_post_meta($order_id,'_billing_nip', sanitize_text_field($_POST['_billing_nip']));
	  	update_post_meta($order_id,'_billing_company_city', sanitize_text_field($_POST['_billing_company_city']));
	  	update_post_meta($order_id,'_billing_company', sanitize_text_field($_POST['_billing_company']));

 
	
  }
}