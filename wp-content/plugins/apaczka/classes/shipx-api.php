<?php

class shipxApi
{

    const API_URL_PRODUCTION_PL = 'http://api-shipx-pl.easypack24.net';

    const API_URL_SANDBOX_PL = 'https://sandbox-api-shipx-pl.easypack24.net';

    const API_URL_PRODUCTION_UK = 'https://api-shipx-uk.easypack24.net/v1/';

    const API_URL_SANDBOX_UK = 'https://sandbox-api-shipx-uk.easypack24.net/v1/';

    const API_GEOWIDGET_URL_PRODUCTION_PL = 'https://api-pl-points.easypack24.net/v1';

    const API_GEOWIDGET_URL_SANDBOX_PL = 'https://sandbox-api-pl-points.easypack24.net/v1';

    const API_GEOWIDGET_URL_PRODUCTION_UK = 'https://api-uk-points.easypack24.net/v1';

    const API_GEOWIDGET_URL_SANDBOX_UK = 'https://sandbox-api-uk-points.easypack24.net/v1';

    const API_GEOWIDGET_URL_PRODUCTION_CSS = 'https://geowidget.easypack24.net/css/easypack.css';

    const API_GEOWIDGET_URL_SANDBOX_CSS = 'https://sandbox-geowidget.easypack24.net/css/easypack.css';


    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_front_scripts'], 75);
        add_action('admin_enqueue_scripts', [$this, 'include_geowidget_settings'], 76);
        add_action('admin_enqueue_scripts', [$this, 'include_geowidget_metabox'], 77);
        add_action('wp_footer', [$this, 'frontFooter'], 75);
        add_action('admin_footer', [$this, 'adminFooter'], 77);



        add_action('woocommerce_review_order_after_shipping',
            [$this, 'woocommerce_review_order_after_shipping']);
        add_action('woocommerce_checkout_update_order_meta',
            array($this, 'woocommerce_checkout_update_order_meta'));
    }


    public function include_geowidget_settings()
    {
        if (is_admin()
            && isset($_GET[ 'section' ])
            && 'apaczka' === $_GET[ 'section' ] ) {

            wp_enqueue_script('geowidget-admin',
                WPDesk_Apaczka_Plugin::get_instance()->getPluginUrl()
                . 'assets/js/admin-geowidget-settings.js');

            wp_enqueue_style('geowidget-4.5-css',
                self::API_GEOWIDGET_URL_PRODUCTION_CSS);
        }
    }

    public function include_geowidget_metabox()
    {
        if (is_admin()
            && isset($_GET[ 'post' ])
            && 'shop_order' === get_post_type($_GET[ 'post' ]) ) {
            wp_enqueue_script('geowidget-admin',
                WPDesk_Apaczka_Plugin::get_instance()->getPluginUrl()
                . 'assets/js/admin-geowidget-metabox.js');

            wp_enqueue_style('geowidget-4.5-css',
                self::API_GEOWIDGET_URL_PRODUCTION_CSS);
        }
    }

    public function enqueue_front_scripts()
    {
        if (true === is_checkout()){
            wp_enqueue_style('woocommerce-apaczka-front',
                WPDesk_Apaczka_Plugin::get_instance()->getPluginUrl()
                . 'assets/css/admin.css');

            wp_enqueue_script('woocommerce-apaczka-front',
                WPDesk_Apaczka_Plugin::get_instance()->getPluginUrl()
                . 'assets/js/front.js');

            wp_enqueue_style('geowidget-4.5-css',
                self::API_GEOWIDGET_URL_PRODUCTION_CSS);
        }
    }

    /**
     * @param int $order_id
     */
    public function woocommerce_checkout_update_order_meta($order_id)
    {
        if ( $_POST[ 'parcel_machine_id' ] ) {
            update_post_meta($order_id, '_parcel_machine_id',
                esc_attr($_POST[ 'parcel_machine_id' ]));
            update_post_meta($order_id, '_is_parcel_locker', true);
        }
    }


    /**
     * @throws \Exception
     */
    public function woocommerce_review_order_after_shipping()
    {
        $selected_method_in_cart
            = flexible_shipping_method_selected_in_cart('apaczka');

        $selected_method_in_cart_cod
            = flexible_shipping_method_selected_in_cart('apaczka_cod');

        if ( false === $selected_method_in_cart
            && false == $selected_method_in_cart_cod
        ) {
            return;
        }

        $method = new WPDesk_Apaczka_Shipping();

        $service = $method->get_option('service');
        if ('PACZKOMAT' !== $service) {
            return;
        }

        $args[ 'parcel_machine_id' ]
            = WC()->session->get('parcel_machine_id');
        $args[ 'geowidget_src' ] = self::API_GEOWIDGET_URL_PRODUCTION_PL;


        wc_get_template('geowidget-review-order-after-shipping.php',
            $args, '',
            WPDesk_Apaczka_Plugin::get_instance()->getPluginDirectory()
            . DIRECTORY_SEPARATOR
            . 'templates/');

    }



    public
    function frontFooter()
    {
        if (true === is_checkout()){
            echo '<script async src="https://geowidget.easypack24.net/js/sdk-for-javascript.js"></script>';
        }

    }
    public
    function adminFooter()
    {
        echo '<script async src="https://geowidget.easypack24.net/js/sdk-for-javascript.js"></script>';

    }

}