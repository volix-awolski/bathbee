<?php
/**
 * Review Order After Shipping EasyPack
 *
 * @author
 * @package    EasyPack/Templates
 * @version
 */

$parcel_machine_selected = false;
$selected = '';


?>
<tr class="easypack-parcel-machine">
    <th class="easypack-parcel-machine-label">
        <?php __('Wybierz paczkomat', 'apaczka'); ?>
    </th>
    <td class="easypack-parcel-machine-select">

        <?php
        $cod = isset($_cod) && true === $_cod;

        $countryCode = 'pl';
        $lon = '51.507351';
        $lat = '-0.127758';
        $loc = '';

        ?>

        <?php //var_dump(EasyPack::EasyPack()->getGeowidgetApiUrl());die;?>


        <?php if (defined('DOING_AJAX') && true === DOING_AJAX): ?>

            <?php $randomId = 'id'.rand(1, 9999); ?>
            <a id="popup-btn"></a>


            <button class="button alt"
                    name="geowidget_show_map"
                    id="geowidget_show_map"
                    value="<?php echo __('Wybierz paczkomat', 'apaczka'); ?>"
                    data-value="<?php echo __('Wybierz paczkomat',
                        'apaczka'); ?>">
                <?php echo __('Wybierz paczkomat', 'apaczka'); ?></button>

            <script type="text/javascript">
                var initiated = false;

                window.easyPackAsyncInit = (function () {
                    easyPack.init({});
                });
                //var button = document.getElementById('geowidget_show_map');
                jQuery('#geowidget_show_map').click(function (e) {
                    e.preventDefault();
                    easyPack.init({
                        apiEndpoint: '<?php echo shipxApi::API_GEOWIDGET_URL_PRODUCTION_PL?>',
                        defaultLocale: 'pl',
                        closeTooltip: false,
                        points: {
                            types: ['parcel_locker', 'pop']
                        },
                        map: {
                            <?php echo $loc?>
                            useGeolocation: true
                        }
                    });
                    easyPack.modalMap(function (point) {
                        this.close();   // Close modal with map, must be called from inside modalMap() callback.
                        var parcelMachineAddressDesc = getAddressByPoint(point);
                        jQuery('#parcel_machine_id').val(point.name);
                        jQuery('#parcel_machine_desc').val(parcelMachineAddressDesc);
                        //jQuery('#easypack-map').addClass('hidden');
                        jQuery('#selected-parcel-machine').removeClass('hidden');
                        jQuery('#selected-parcel-machine-id').html(parcelMachineAddressDesc);
                    }, {width: 500, height: 600});

                    setTimeout(function () {
                        jQuery("html, body").animate({scrollTop: jQuery('#widget-modal').offset().top}, 1000);

                    }, 0);


                });


                jQuery(document).ready(function () {
                    if (false === initiated) {
                        if (typeof(easyPack) !== 'undefined') {
                            easyPack.init({
                                apiEndpoint: '<?php shipxApi::API_GEOWIDGET_URL_PRODUCTION_PL?>',
                                defaultLocale: 'pl',
                                closeTooltip: false,
                                points: {
                                    types: ['parcel_locker']
                                },
                                map: {
                                    <?php echo $loc?>
                                    useGeolocation: true
                                }
                            });
                        }


                        initiated = true;
                    }
                });

            </script>


            <div id="selected-parcel-machine" class="hidden">
                <div><span class="font-height-600">
                <?php echo __('Selected Parcel Locker:', 'apaczka'); ?>
                </span></div>
                <span class="italic" id="selected-parcel-machine-id"></span>
            </div>

            <!--
            <div id="easypack-map" style="min-width: 400px; height: 400px; margin: 20px auto!important;">
                <div id="<?php /*echo $randomId */ ?>"></div>
            </div>-->
            <input type="hidden" id="parcel_machine_id"
                   name="parcel_machine_id"/>
            <input type="hidden" id="parcel_machine_desc"
                   name="parcel_machine_desc"/>

        <?php else: ?>

        <?php endif ?>


    </td>
</tr>
