<div id="rwpp-container">

	<h3><?php _e('Rearrange Products', 'rwpp');?></h3>

	<?php
	// Query Products
	$params = array(
				'post_type' 		=> 'product',
				'posts_per_page' 	=> -1,
				'orderby' 			=> 'menu_order',
				'order' 			=> 'ASC'
			);
	$wc_query = new WP_Query($params);
	?>
	
	<?php if ($wc_query->have_posts()) : ?>

	<form id="frm_rwpp" action="<?php echo get_admin_url();?>admin-ajax.php" method='post'>
		<input type="hidden" name="action" value="save_rwpp" />
		<ul id="sortable">
			<?php $counter = 0;?>
			<?php while ($wc_query->have_posts()) : $wc_query->the_post(); ?>
			
			<li>
				<?php
				global $post;
				?>
				<input type="hidden" name="product_id[]" id="product_id[]" value="<?php echo $post->ID;?>">
				
				<div class="menu-item-bar">
					<div class="menu-item-handle">
						<span class="item-title"><span class="menu-item-title"><?php the_title(); ?></span></span>
						<input type="hidden" name="current_menu_order[]" id="current_menu_order[]" value="<?php echo $post->menu_order;?>">
						<input type="hidden" name="new_menu_order[]" id="new_menu_order[]" value="<?php echo $counter;?>">
					</div>
				</div>
			</li>

			<?php $counter++;?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>

		<div class="submit-btn-wrapper">
			<?php submit_button( __('Update Products', 'rwpp'), 'primary', '', '', array('id' => 'btn_save_rwpp')); ?> <div class="spinner"></div>
			<div class="response"></div>
		</div>

		<div class="notice notice-warning is-dismissible">
			<p><?php _e('<strong>Note:</strong> Products rearranging can not be undone after deactivating or deleting the plugin.', 'rwpp');?></p>
		</div>

	</form> 

	<?php else:  ?>

	<div class="notice notice-warning">
		<p><?php _e('No products found.', 'rwpp');?></p>
	</div>

	<?php endif; ?>

</div>