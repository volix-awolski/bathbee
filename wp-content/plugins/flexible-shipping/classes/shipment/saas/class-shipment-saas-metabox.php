<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Saas_Metabox
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Metabox {

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Ajax constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function __construct( WPDesk_Flexible_Shipping_Shipment_Saas $shipment ) {
		$this->shipment = $shipment;
	}

	/**
	 * Prepare collection points options.
	 *
	 * @return array
	 */
	private function prepare_collection_points_options() {
		$shipping_service                   = $this->shipment->get_shipping_service();
		$shipping_service_collection_points = $shipping_service->get_collection_points();

		$order   = $this->shipment->get_order();
		$address = new \WPDesk\SaasPlatformClient\Model\Shipment\Address();
		$address->setAddressLine1( $order->get_shipping_address_1() );
		$address->setAddressLine2( $order->get_shipping_address_2() );
		$address->setPostalCode( $order->get_shipping_postcode() );
		$address->setCity( $order->get_shipping_city() );
		$address->setStateCode( $order->get_shipping_state() );
		$address->setCountryCode( $order->get_shipping_country() );

		return $shipping_service_collection_points->prepare_collection_points_options_for_address( $address );
	}


	/**
	 * Display fieldset in order metabox.
	 *
	 * @param array $fieldset Fieldset.
	 * @param int   $id Shipment ID.
	 * @param bool  $disabled Disabled.
	 * @param array $values Values.
	 */
	private function display_order_metabox_fieldset( array $fieldset, $id, $disabled, $values ) {
		$fields        = $fieldset['fields'];
		$is_replicable = false;
		foreach ( $fields as $field_key => $field ) {
			if ( 'parcel_collection_points' === $field['type'] ) {
				$fields[ $field_key ]['access_points_options'] = $this->prepare_collection_points_options();
			}
		}
		if ( isset( $fieldset['replicable'] ) ) {
			$is_replicable = true;
			include 'views/order-metabox-fieldset-replicable.php';
		} else {
			include 'views/order-metabox-fieldset.php';
		}
	}

	/**
	 * Display order metabox attachments.
	 */
	private function display_order_metabox_attachments() {
		$shipment_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Attachments( $this->shipment );
		if ( $shipment_attachments->are_attachments_avaliable() ) {
			$metabox_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Metabox_Attachments( $this->shipment );
			$metabox_attachments->display_order_metabox_attachments();
		}
	}

	/**
	 * Get order metabox content.
	 *
	 * @return string
	 */
	public function get_order_metabox_content() {

		/** Global $post is required for woocommerce_wp_text_input used in metabox. */
		global $post;
		if ( empty( $post ) ) {
			$post = $this->shipment->get_post();
		}

		ob_start();

		$disabled = true;
		if ( $this->shipment->get_status() === 'fs-new' || $this->shipment->get_status() === 'fs-failed' ) {
			$disabled = false;
		}
		$order           = $this->shipment->get_order();
		$id              = $this->shipment->get_id();
		$integration     = $this->shipment->get_integration();
		$values          = $this->shipment->get_meta();
		$label_url       = $this->shipment->get_label_url();
		$tracking_number = $this->shipment->get_tracking_number();
		$tracking_url    = $this->shipment->get_tracking_url();

		$shipping_service = $this->shipment->get_shipping_service();

		try {
			$fields = $this->shipment->get_fields_for_targets();
			foreach ( $fields as $fieldset ) {
				if ( isset( $fieldset['show-in'] ) && in_array( 'metabox', $fieldset['show-in'], true ) ) {
					$this->display_order_metabox_fieldset( $fieldset, $id, $disabled, $values );
				}
			}

			$this->display_order_metabox_attachments();

			$supports_label  = $shipping_service->supports( 'supportsShipmentLabel' );
			$supports_cancel = $shipping_service->supports( 'supportsShipmentCancel' );

			$status      = $this->shipment->get_status();
			$status_text = get_post_status_object( $status )->label;

			$shipment_charges      = '';
			$show_shipping_charges = false;
			if ( ! empty( $this->shipment->get_shipment_cost_as_money() ) ) {
				$shipment_charges      = $this->shipment->get_shipment_cost_amount();
				$show_shipping_charges = true;
			}

			include 'views/order-metabox-actions.php';

		} catch ( Exception $e ) {
			if ( $e instanceof  WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception ) {
				$error_message = $e->getMessage();
			} elseif ( $e instanceof  WPDesk_Flexible_Shipping_SaaS_Service_Settings_Not_Found ) {
				$error_message = $shipping_service->prepare_settings_not_found_message();
			} else {
				$saas_connection = new WPDesk_Flexible_Shipping_SaaS_Connection();
				if ( $e instanceof WPDesk_Flexible_Shipping_SaaS_Maintenance_Mode_Exception ) {
					$maintenance_context = new \WPDesk\SaasPlatformClient\Response\Maintenance\MaintenanceResponseContext( $e->get_response() );
					$error_message       = $saas_connection->prepare_maintenance_notice(
						$maintenance_context->getMaintenanceMessage(),
						$maintenance_context->getMaintenanceTill()
					);
				} else {
					$error_message = $saas_connection->prepare_fatal_error_notice();
				}
			}
			include 'views/order-metabox-connection-error.php';
		}

		$content = ob_get_contents();
		ob_end_clean();

		return $content;

	}

}
