<?php

/**
 * Handle documents for shipments.
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Attachments_Ajax implements \WPDesk\PluginBuilder\Plugin\Hookable {

	const RESPONSE_STATUS         = 'status';
	const RESPONSE_MESSAGE        = 'message';
	const RESPONSE_STATUS_FAIL    = 'fail';
	const RESPONSE_STATUS_SUCCESS = 'success';

	const FLEXIBLE_SHIPPING_SHIPMENT_NONCE = 'flexible_shipping_shipment_nonce';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wp_ajax_flexible_shipping_add_attachment_to_shipment', array( $this, 'handle_ajax_add_attachment' ) );
		add_action( 'wp_ajax_flexible_shipping_delete_attachment_for_shipment', array( $this, 'handle_ajax_delete_attachment' ) );
		add_action( 'wp_ajax_flexible_shipping_send_attachments_for_shipment', array( $this, 'handle_ajax_send_attachments' ) );
	}

	/**
	 * Get value from request parameter.
	 *
	 * @param string $parameter Parameter name.
	 *
	 * @return string|null
	 */
	private function get_from_request( $parameter ) {
		if ( isset( $_REQUEST[ $parameter ] ) ) { // WPCS: CSRF ok, input var ok.
			return $_REQUEST[ $parameter ]; // WPCS: CSRF ok, input var ok.
		}
		return '';
	}

	/**
	 * Get shipment.
	 *
	 * @param int $shipment_id Shipment ID.
	 *
	 * @return WPDesk_Flexible_Shipping_Shipment_Saas|null
	 */
	private function get_shipment( $shipment_id ) {
		$shipment = fs_get_shipment( $shipment_id );
		if ( $shipment instanceof WPDesk_Flexible_Shipping_Shipment_Saas ) {
			return $shipment;
		}
		return null;
	}

	/**
	 * Handle AJAX add attachment request.
	 */
	public function handle_ajax_add_attachment() {
		$response = array(
			self::RESPONSE_STATUS  => self::RESPONSE_STATUS_FAIL,
			self::RESPONSE_MESSAGE => __( 'Unknown error!', 'flexible-shipping' ),
		);
		if ( ! wp_verify_nonce( $this->get_from_request( 'nonce' ), self::FLEXIBLE_SHIPPING_SHIPMENT_NONCE ) ) {
			$response[ self::RESPONSE_STATUS ]  = self::RESPONSE_STATUS_FAIL;
			$response[ self::RESPONSE_MESSAGE ] = __( 'Nonce verification error! Invalid request.', 'flexible-shipping' );
		} else {
			$shipment_id     = $this->get_from_request( 'shipment_id' );
			$attachment_id   = $this->get_from_request( 'attachment_id' );
			$attachment_type = $this->get_from_request( 'attachment_type' );

			$shipment = $this->get_shipment( intval( $shipment_id ) );

			$attachment = wp_get_attachment_metadata( $attachment_id );

			if ( empty( $attachment_type ) ) {
				$response[ self::RESPONSE_MESSAGE ] = __( 'Attachment type not selected!', 'flexible-shipping' );
			} elseif ( false === $attachment ) {
				$response[ self::RESPONSE_MESSAGE ] = __( 'Attachment not found!', 'flexible-shipping' );
			} elseif ( empty( $shipment ) ) {
				$response[ self::RESPONSE_MESSAGE ] = __( 'Shipment not found!', 'flexible-shipping' );
			} else {

				$shipment_document_request = new \WPDesk\SaasPlatformClient\Model\ShipmentDocument\ShipmentDocumentRequest();
				$shipment_document_request->setDocumentType( $attachment_type );
				$shipment_document_request->setFileContent( base64_encode( file_get_contents( get_attached_file( $attachment_id ) ) ) );
				$ext = '.' . pathinfo( get_attached_file( $attachment_id ), PATHINFO_EXTENSION );
				$shipment_document_request->setFileName( get_the_title( $attachment_id ) . $ext );

				try {
					$shipment_document = $shipment->get_platform()->requestPostShipmentDocument(
						$shipment_document_request,
						$shipment->get_shipping_service()->get_id(),
						$shipment->get_saas_shipment_id()
					);

					if ( $shipment_document->isError() ) {
						throw new RuntimeException( $shipment_document->getShipmentDocument()->getMessage() );
					}

					$shipment_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Attachments( $shipment );
					$shipment_attachments->add_attachment( $attachment_type, $attachment_id, $shipment_document->getShipmentDocument()->getId() );

					$shipment->save();

					$response[ self::RESPONSE_STATUS ] = self::RESPONSE_STATUS_SUCCESS;
					$response['shipment_id']           = $shipment_id;
					$response['attachment_id']         = $attachment_id;

					$response[ self::RESPONSE_MESSAGE ] = __( 'Attachment added', 'flexible-shipping' );
					$metabox_attachments                = new WPDesk_Flexible_Shipping_Shipment_Saas_Metabox_Attachments( $shipment );
					$response['content']                = $metabox_attachments->get_order_metabox_content();
				} catch ( Exception $e ) {
					$response[ self::RESPONSE_MESSAGE ] = $e->getMessage();
				}
			}
		}
		wp_send_json( $response );
	}

	/**
	 * Handle AJAX delete attachment request.
	 */
	public function handle_ajax_delete_attachment() {
		$response = array(
			self::RESPONSE_STATUS  => self::RESPONSE_STATUS_FAIL,
			self::RESPONSE_MESSAGE => __( 'Unknown error!', 'flexible-shipping' ),
		);
		if ( ! wp_verify_nonce( $this->get_from_request( 'nonce' ), self::FLEXIBLE_SHIPPING_SHIPMENT_NONCE ) ) {
			$response[ self::RESPONSE_STATUS ]  = self::RESPONSE_STATUS_FAIL;
			$response[ self::RESPONSE_MESSAGE ] = __( 'Nonce verification error! Invalid request.', 'flexible-shipping' );
		} else {
			$shipment_id        = $this->get_from_request( 'shipment_id' );
			$saas_attachment_id = $this->get_from_request( 'saas_attachment_id' );

			$shipment = $this->get_shipment( intval( $shipment_id ) );

			if ( empty( $shipment ) ) {
				$response[ self::RESPONSE_MESSAGE ] = __( 'Shipment not found!', 'flexible-shipping' );
			} else {

				try {
					$delete_response = $shipment->get_platform()->requestDeleteShipmentDocument( $saas_attachment_id );

					if ( $delete_response->isError() ) {
						throw new RuntimeException( $delete_response->getResponseMessage() );
					}

					$shipment_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Attachments( $shipment );

					$deleted = $shipment_attachments->delete_attachment_by_saas_attachment_id( $saas_attachment_id );

					if ( $deleted ) {
						$shipment->save();
						$response[ self::RESPONSE_STATUS ]  = self::RESPONSE_STATUS_SUCCESS;
						$response[ self::RESPONSE_MESSAGE ] = __( 'Attachment deleted', 'flexible-shipping' );
					} else {
						$response[ self::RESPONSE_MESSAGE ] = __( 'Attachment not found!', 'flexible-shipping' );
					}

					$metabox_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Metabox_Attachments( $shipment );
					$response['content'] = $metabox_attachments->get_order_metabox_content();

				} catch ( Exception $e ) {
					$response[ self::RESPONSE_MESSAGE ] = $e->getMessage();
				}
			}
		}
		wp_send_json( $response );
	}

	/**
	 * Handle AJAX send attachments request.
	 */
	public function handle_ajax_send_attachments() {
		$response = array(
			self::RESPONSE_STATUS  => self::RESPONSE_STATUS_FAIL,
			self::RESPONSE_MESSAGE => __( 'Unknown error!', 'flexible-shipping' ),
		);
		if ( ! wp_verify_nonce( $this->get_from_request( 'nonce' ), self::FLEXIBLE_SHIPPING_SHIPMENT_NONCE ) ) {
			$response[ self::RESPONSE_STATUS ]  = self::RESPONSE_STATUS_FAIL;
			$response[ self::RESPONSE_MESSAGE ] = __( 'Nonce verification error! Invalid request.', 'flexible-shipping' );
		} else {
			$shipment_id = $this->get_from_request( 'shipment_id' );

			$shipment = $this->get_shipment( intval( $shipment_id ) );

			if ( empty( $shipment ) ) {
				$response[ self::RESPONSE_MESSAGE ] = __( 'Shipment not found!', 'flexible-shipping' );
			} else {

				try {
					$send_documents = $shipment->get_platform()->requestPostSendDocuments(
						$shipment->get_saas_shipment_id(),
						$shipment->get_shipping_service()->get_id()
					);

					if ( $send_documents->isError() ) {
						// Translators: response code.
						throw new RuntimeException( sprintf( __( 'Unable to send documents: %1$s' ), $send_documents->getResponseMessage() ) );
					}


					$shipment_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Attachments( $shipment );
					$shipment_attachments->mark_attachments_as_sent( $send_documents->getDocumentStatusesResponse()->getDocumentStatuses() );

					$shipment->save();

					$response[ self::RESPONSE_STATUS ]  = self::RESPONSE_STATUS_SUCCESS;
					$response[ self::RESPONSE_MESSAGE ] = __( 'Attachments send', 'flexible-shipping' );

					$metabox_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Metabox_Attachments( $shipment );
					$response['content'] = $metabox_attachments->get_order_metabox_content();

				} catch ( Exception $e ) {
					$response[ self::RESPONSE_MESSAGE ] = $e->getMessage();
				}
			}
		}
		wp_send_json( $response );
	}

}
