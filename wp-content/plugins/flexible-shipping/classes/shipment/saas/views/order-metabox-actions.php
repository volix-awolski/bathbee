<?php if ( !$disabled ) : ?>
	<p>
		<button data-id="<?php echo $id; ?>" class="button button-primary fs-saas-button fs-saas-button-create button-shipping"><?php _e( 'Create', 'flexible-shipping' ); ?></button>
		<button data-id="<?php echo $id; ?>" class="button fs-saas-button fs-saas-button-save button-shipping"><?php _e( 'Save', 'flexible-shipping' ); ?></button>
		<span class="spinner fs-saas-spinner shipping-spinner"></span>
	</p>
<?php else : ?>
	<p>
	<p>
		<?php if ( $supports_label ) : ?>
			<a data-qa-id="get-label" target="_blank" href='<?php echo $label_url; ?>' data-id="<?php echo $id; ?>" class="button button-primary fs-saas-button fs-saas-button-get-label"><?php _e( 'Get Label', 'flexible-shipping' ); ?></a>
		<?php endif; ?>
		<?php if ( $supports_cancel ) : ?>
			<span class="spinner fs-saas-spinner shipping-spinner"></span>
			<a data-qa-id="cancel-created" data-id="<?php echo $id; ?>" class="fs-saas-button fs-saas-button-cancel-created button-shipping"><?php _e( 'Cancel', 'flexible-shipping' ); ?></a>
	    <?php endif; ?>
	</p>
	</p>
<?php endif; ?>
<?php if ( ! empty( $tracking_url ) ) : ?>
	<p class="fs-saas-shipment">
		<span><?php _e( 'Track:', 'flexible-shipping' ); ?></span> <a title="<?php _e( 'Click to track shipment', 'flexible-shipping' ); ?>" target="_blank" href="<?php echo $tracking_url; ?>"><?php echo $tracking_number; ?></a>
	</p>
<?php endif; ?>
<?php if ( $show_shipping_charges ) : ?>
	<p class="fs-saas-charges">
		<?php echo sprintf( __( '%1$sShipping charges:%2$s %3$s', 'flexible-shipping' ), '<span>', '</span>', $shipment_charges ); ?>
	</p>
<?php endif; ?>
<p class="fs-saas-status fs-saas-status-<?php echo $status; ?>">
	<?php echo sprintf( __( '%1$sShipment status:%2$s %3$s', 'flexible-shipping' ), '<span>', '</span>', $status_text ); ?>
</p>
