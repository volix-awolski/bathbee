<?php
/**
 * @package Flexible Shipping
 * @var array  $attachment_types Allowed document types.
 * @var string $type_value Type value.
 *
 * @var string $attachment_url Attachment URL.
 *
 * @var string $filename_value Attachment filename.
 *
 * @var string $shipment_id Shipment ID.
 *
 * @var string $saas_attachment_id_value SaaS attachment ID.
 */
?>
<div class="flexible-shipping-shipment-attachments-single-content">
	<tr>
		<td><?php echo $attachment_types[ $type_value ]; ?>:</td>
		<td>
			<!--
				<a href="<?php echo $attachment_url; ?>" target="_blank"><?php echo $filename_value; ?></a>
			-->
			<?php echo $filename_value; ?>
		</td>
		<td>
			<?php echo $sent_value; ?>
		</td>
		<td>
			<a
				class="fs-saas-button-delete-attachment"
				href="#"
				data-saas_attachment_id="<?php echo esc_attr( $saas_attachment_id_value ); ?>"
				data-shipment_id="<?php echo esc_attr( $shipment_id ); ?>"
			>
				<?php _e( 'Delete', 'flexible-shipping' ); ?>
			</a>
		</td>
	</tr>
</div>
