<?php

/**
 * Flexible Shipping SaaS Shipment Attachments.
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Attachments {

	const META_ATTACHMENTS = 'attachments';

	const SAAS_ATTACHMENT_ID = 'saas_attachment_id';
	const ATTACHMENT_ID      = 'attachment_id';
	const TYPE               = 'type';

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Attachments constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function __construct( $shipment ) {
		$this->shipment = $shipment;
	}

	/**
	 * Get attachments.
	 *
	 * @return array
	 */
	public function get_attachments() {
		$attachments = $this->shipment->get_meta( self::META_ATTACHMENTS, array() );
		if ( ! is_array( $attachments ) ) {
			$attachments = array( $attachments );
		}
		foreach ( $attachments as $attachment_key => $attachment ) {
			if ( ! is_array( $attachment ) ) {
				unset( $attachments[ $attachment_key ] );
			} else {
				$attachment_id = $attachment[ self::ATTACHMENT_ID ];
				$ext           = '.' . pathinfo( get_attached_file( $attachment_id ), PATHINFO_EXTENSION );

				$attachments[ $attachment_key ]['filename'] = get_the_title( $attachment_id ) . $ext;
			}
		}
		return $attachments;
	}

	/**
	 * Delete attachment by SaaS attachment ID.
	 *
	 * @param int $saas_attachment_id SaaS attachment id.
	 * @return bool
	 */
	public function delete_attachment_by_saas_attachment_id( $saas_attachment_id ) {
		$attachments = $this->get_attachments();
		$deleted     = false;
		foreach ( $attachments as $attachment_key => $attachment ) {
			if ( intval( $attachment[ self::SAAS_ATTACHMENT_ID ] ) === intval( $saas_attachment_id ) ) {
				$deleted = true;
				unset( $attachments[ $attachment_key ] );
				$this->shipment->set_meta( self::META_ATTACHMENTS, $attachments );
			}
		}

		return $deleted;
	}

	/**
	 * Add attachment.
	 *
	 * @param string $attachment_type Attachment type.
	 * @param int    $attachment_id Attachment id.
	 * @param int    $saas_attachment_id SaaS attachment id.
	 */
	public function add_attachment( $attachment_type, $attachment_id, $saas_attachment_id ) {
		$attachments   = $this->get_attachments();
		$attachments[] = array(
			self::TYPE               => $attachment_type,
			self::ATTACHMENT_ID      => $attachment_id,
			self::SAAS_ATTACHMENT_ID => $saas_attachment_id,
		);
		$this->shipment->set_meta( self::META_ATTACHMENTS, $attachments );
	}

	/**
	 * Mark attachments as sent.
	 *
	 * @param array $attachment_statuses Attachment statuses.
	 */
	public function mark_attachments_as_sent( $attachment_statuses ) {
		$attachments = $this->get_attachments();
		foreach ( $attachment_statuses as $attachment_status ) {
			$document_id   = intval( $attachment_status['documentId'] );
			$document_sent = intval( $attachment_status['documentSent'] );
			foreach ( $attachments as $attachment_key => $attachment ) {
				if ( $document_id === $attachment[ self::SAAS_ATTACHMENT_ID ] ) {
					if ( $document_sent ) {
						$attachment['sent'] = 1;
					} else {
						unset( $attachment['sent'] );
					}
					$attachments[ $attachment_key ] = $attachment;
				}
			}
		}

		$this->shipment->set_meta( self::META_ATTACHMENTS, $attachments );

	}

	/**
	 * Are attachments available.
	 *
	 * @return bool
	 */
	public function are_attachments_avaliable() {
		return 'yes' === $this->shipment->get_meta( 'documents', 'no' );
	}

}
