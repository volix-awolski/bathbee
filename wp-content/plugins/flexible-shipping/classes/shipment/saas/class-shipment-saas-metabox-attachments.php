<?php

/**
 * Metabox attachments content.
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Metabox_Attachments {

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Ajax constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function __construct( WPDesk_Flexible_Shipping_Shipment_Saas $shipment ) {
		$this->shipment = $shipment;
	}


	/**
	 * Display order metabox attachments.
	 */
	public function display_order_metabox_attachments() {
		global $thepostid;

		$thepostid = $this->shipment->get_order()->get_id();

		$shipment_id         = $this->shipment->get_id();
		$type_field_id       = 'attachment_type';
		$attachment_field_id = 'attachment_attachment_id';

		$field_label = __( 'Attachments', 'flexible-shipping' );

		$shipment_attachments = new WPDesk_Flexible_Shipping_Shipment_Saas_Attachments( $this->shipment );

		$attachments = $shipment_attachments->get_attachments();

		$attachment_types = array(
			'COMMERCIAL_INVOICE'          => 'Invoice',
			'CERTIFICATE_OF_ORIGIN'       => 'Origin cert',
			'NAFTA_CERTIFICATE_OF_ORIGIN' => 'NAFTA cert',
			'PRO_FORMA_INVOICE'           => 'Pro Forma Inv',
			'OTHER'                       => 'Other',
		);
		if ( $this->shipment->get_status() === WPDesk_Flexible_Shipping_Shipment::STATUS_FS_CONFIRMED ) {
			include 'views/order-metabox-shipment-attachments.php';
		}
	}

	/**
	 * Get order metabox content.
	 *
	 * @return string
	 */
	public function get_order_metabox_content() {
		ob_start();
		$this->display_order_metabox_attachments();
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

}
