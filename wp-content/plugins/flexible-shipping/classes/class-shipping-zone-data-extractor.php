<?php

/**
 * Class WPDesk_Flexible_Shipping_UK_States
 */
class WPDesk_Flexible_Shipping_Shipping_Zone_Data_Extractor {

	/**
	 * Shipping zone.
	 *
	 * @var WC_Shipping_Zone
	 */
	private $shipping_zone;

	/**
	 * WPDesk_Flexible_Shipping_Shipping_Zone_Data_Extractor constructor.
	 *
	 * @param WC_Shipping_Zone $shipping_zone Shipping zone.
	 */
	public function __construct( $shipping_zone ) {
		$this->shipping_zone = $shipping_zone;
	}

	/**
	 * Get zone countries.
	 *
	 * @return array
	 */
	public function get_countries() {
		$countries = array();
		$locations = $this->shipping_zone->get_zone_locations();
		foreach ( $locations as $location ) {
			if ( 'country' === $location->type ) {
				$countries[ $location->code ] = $location->code;
			}
			if ( 'continent' === $location->type ) {
				$continents = WC()->countries->get_continents();
				$countries  = $continents[ $location->code ]['countries'];
				foreach ( $countries as $country ) {
					$countries[ $country ] = $country;
				}
			}
		}
		if ( 0 === count( $countries ) ) {
			$countries[] = 'other';
		}
		return $countries;
	}

}
