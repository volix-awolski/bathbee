<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Free_Shipping_Metadata_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Free_Shipping_Metadata_Handler  implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const META_FS_FREE_SHIPPING = '_fs_free_shipping';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'woocommerce_order_shipping_method', [ $this, 'add_free_shipping_to_shipping_method_name' ], 10, 2 );
		add_filter( 'woocommerce_hidden_order_itemmeta', [ $this, 'hide_free_shipping_meta' ] );
		add_action( 'woocommerce_before_order_itemmeta', [ $this, 'display_free_shipping_in_order_shipping' ], 10, 3 );
	}

	/**
	 * Add free shipping to shipping method name.
	 *
	 * @param string   $names Names.
	 * @param WC_Order $order Order.
	 *
	 * @return string
	 */
	public function add_free_shipping_to_shipping_method_name( $names, $order ) {
		$names = array();
		foreach ( $order->get_shipping_methods() as $shipping_method ) {
			$name     = $shipping_method->get_name();
			$fallback = $shipping_method->get_meta( self::META_FS_FREE_SHIPPING );
			if ( '' !== $fallback ) {
				// Translators: shipping method name.
				$name = sprintf( __( '%1$s (free shipping)', 'flexible-shipping' ), $name );
			}
			$names[] = $name;
		}
		return implode( ', ', $names );
	}

	/**
	 * Hide free shipping meta.
	 *
	 * @param array $items Items to hide.
	 *
	 * @return array
	 */
	public function hide_free_shipping_meta( array $items ) {
		$items[] = self::META_FS_FREE_SHIPPING;
		return $items;
	}

	/**
	 * Display free shipping in order shipping.
	 *
	 * @param string          $item_id Item Id.
	 * @param WC_Order_Item   $item Item.
	 * @param WC_Product|null $product Produuct.
	 */
	public function display_free_shipping_in_order_shipping( $item_id, $item, $product ) {
		if ( $item->is_type( 'shipping' ) ) {
			$free_shipping = $item->get_meta( self::META_FS_FREE_SHIPPING );
			if ( '' !== $free_shipping ) {
				include 'views/html-order-free-shipping.php';
			}
		}
	}

}
