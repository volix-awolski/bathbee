<?php

/**
 * Displays additional shipping checkout fields for shipment.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Shipment
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Shipment {

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Service collection points.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points
	 */
	private $service_collection_points;

	/**
	 * Renderer.
	 *
	 * @var WPDesk\View\Renderer\Renderer;
	 */
	private $renderer;

	/**
	 * Template folder.
	 *
	 * @var string;
	 */
	private $folder;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Checkout constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service                   $shipping_service Shipping service.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points Service collection points.
	 * @param \WPDesk\View\Renderer\Renderer                                   $renderer Renderer.
	 * @param string                                                           $folder Template folder.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points,
		WPDesk\View\Renderer\Renderer $renderer,
		$folder
	) {
		$this->shipping_service          = $shipping_service;
		$this->service_collection_points = $service_collection_points;
		$this->renderer                  = $renderer;
		$this->folder                    = $folder;
	}

	/**
	 * Display access point.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 * @param array                                  $field Field.
	 */
	private function display_access_point( WPDesk_Flexible_Shipping_Shipment_Saas $shipment, $field ) {
		$access_point = new WPDesk_Flexible_Shipping_SaaS_Checkout_Field_Collection_Points(
			$this->shipping_service,
			$this->service_collection_points,
			$this->renderer,
			null,
			$field
		);
		$access_point->display_collection_point_field_for_shipment( $this->folder, $shipment );
	}

	/**
	 * Maybe display parcel collection points field.
	 *
	 * @param array                                  $field Field.
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	private function maybe_display_parcel_collection_points_field( array $field, WPDesk_Flexible_Shipping_Shipment_Saas $shipment ) {
		$visible_when = WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Shipment::create_for_field( $field, $shipment );
		if ( $visible_when->is_visible() ) {
			$this->display_access_point( $shipment, $field );
		}
	}

	/**
	 * Maybe display fields from fieldset for shipment.
	 *
	 * @param array                                  $fieldset Fieldset.
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	private function maybe_display_fields_from_fieldset_for_shipment(
		array $fieldset,
		WPDesk_Flexible_Shipping_Shipment_Saas $shipment
	) {
		if ( in_array( 'checkout', $fieldset['show-in'], true ) ) {
			foreach ( $fieldset['fields'] as $field ) {
				if ( 'parcel_collection_points' === $field['type'] ) {
					$this->maybe_display_parcel_collection_points_field( $field, $shipment );
				}
			}
		}
	}

	/**
	 * Maybe display fields for shipment.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas $shipment Shipment.
	 */
	public function maybe_display_fields_for_shipment( WPDesk_Flexible_Shipping_Shipment_Saas $shipment ) {
		try {
			$request_fields = $this->shipping_service->get_fields_for_targets( [ $shipment->get_order()->get_shipping_country() ] );
			foreach ( $request_fields as $fieldset ) {
				$this->maybe_display_fields_from_fieldset_for_shipment( $fieldset, $shipment );
			}
		} catch ( WPDesk_Flexible_Shipping_SaaS_Service_Settings_Not_Found $e ) {
			null;
		}
	}

}
