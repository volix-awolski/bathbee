<?php

/**
 * Handles conditional logic.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When
 */
abstract class WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When {

	/**
	 * Conditions.
	 *
	 * @var array
	 */
	private $visible_when_conditions;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When constructor.
	 *
	 * @param array $visible_when_conditions Conditions.
	 */
	public function __construct( array $visible_when_conditions ) {
		$this->visible_when_conditions = $visible_when_conditions;
	}

	/**
	 * Get service type.
	 *
	 * @return string
	 */
	abstract protected function get_service_type();

	/**
	 * Is condition meet?
	 *
	 * @param array $condition Condition.
	 *
	 * @return bool
	 */
	private function is_condition_meet( array $condition ) {
		$is_condition_meet = false;

		$field  = $condition['field'];
		$cond   = $condition['cond'];
		$values = $condition['values'];

		$field_value = '';
		if ( 'service_type' === $field ) {
			$field_value = $this->get_service_type();
		}
		if ( 'in' === $cond ) {
			$is_condition_meet = in_array( $field_value, $values, true );
		}
		return $is_condition_meet;
	}

	/**
	 * Is visible?
	 *
	 * @return bool
	 */
	public function is_visible() {
		$is_visible = true;
		foreach ( $this->visible_when_conditions as $condition ) {
			$is_visible = $is_visible && $this->is_condition_meet( $condition );
		}
		return $is_visible;
	}

}
