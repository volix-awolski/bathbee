<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Add_Shipping_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Add_Shipping_Handler {

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Add_Shipping_Handler constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 */
	public function __construct( WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service ) {
		$this->shipping_service = $shipping_service;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'flexible_shipping_add_shipping_options', array( $this, 'handle_add_shipping_options' ) );
	}

	/**
	 * Handle add shipping service options.
	 *
	 * @param array $options Options.
	 *
	 * @return array
	 */
	public function handle_add_shipping_options( array $options ) {
		$options[ $this->shipping_service->get_integration_id() ] = $this->shipping_service->get_name();
		return $options;
	}

}
