<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings_Connection
 */
class WPDesk_Flexible_Shipping_SaaS_Auth_Params {


	/**
	 * Returns WooCommerce shop domain
	 *
	 * @return string
	 */
	public function get_user_domain() {
		if (!defined("SAAS_FLEXIBLESHIPPING_URL_SHOP_DOMAIN")) {
			$parsed_url = wp_parse_url( site_url() );
			$domain = $parsed_url['host'];
		} else {
			$domain = SAAS_FLEXIBLESHIPPING_URL_SHOP_DOMAIN;
		}

		return apply_filters('flexible_shipping_saas_domain', $domain);
	}

	/**
	 * Returns default user locale
	 *
	 * @return string
	 */
	public function get_user_locale() {
		return get_locale();
	}
}

