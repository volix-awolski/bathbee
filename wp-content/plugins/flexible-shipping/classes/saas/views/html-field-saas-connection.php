<?php
/** @var $integration_checkbox bool */
?>

</table>

<div class="fs-page-wrap fs-connect-integration-box <?php echo ! $integration_checkbox ? 'hide-fs-connect-integration-box' : ''; ?>">
	<div class="fs-box">
		<h3 class="wc-settings-sub-title"><?php _e( 'Flexible Shipping Connect', 'flexible-shipping' ); ?></h3>

		<?php if ( ! $saas_connected ) : ?>
			<p>
				<?php
					printf(
						/* Translators: url */
						__( 'Find out more about the possibilities of %1$sFlexible Shipping Connect%2$s.', 'flexible-shipping' ),
						'<a target="_blank" href="https://flexibleshipping.com/connect/">',
						'</a>'
					);
				?>
			</p>
			<p></p>
			<p>
				<?php printf(
					/* Translators: url */
					__( '%1$sRegister%3$s for a new Flexible Shipping Connect account or log in to %2$syour account%3$s to get your key.', 'flexible-shipping' ),
					'<a href="' . admin_url( 'admin.php?page=' . WPDesk_Flexible_Shipping_SaaS_User_Registration::REGISTER_PAGE_SLUG ) . '">',
					'<a href="' . $saas_platform_my_account_url . '">',
					'</a>'
				); ?>
			</p>
		<?php else : ?>
			<p><a href="<?php echo $saas_platform_docs_url; ?>" target="_blank"><?php _e( 'Check how to start with FS Connect &rarr;', 'flexible-shipping' ); ?></a></p>
		<?php endif; ?>

		<?php $saas_connection->display_connection_form(); ?>
	</div>

<?php
/* Fake saas_connected - always true for debug mode settings. */
$saas_connected = true;
?>
	<?php if ( $saas_connected ) : ?>
		<div class="fs-box fs-services">
			<table>
	<?php endif; ?>
