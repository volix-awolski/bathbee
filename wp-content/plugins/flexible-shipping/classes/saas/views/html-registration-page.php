<?php if ( $saas_connected ) : ?>
	<div class="fs-page-wrap">
		<div class="fs-box">
			<p><?php printf( __( 'You are connected. %1$sGo to Flexible Shipping Connect settings &rarr;%2$s', 'flexible-shipping' ), '<a href="' . $settings_url . '">', '</a>' ); ?>
		</div>
	</div>
<?php else : ?>
	<div id="register" class="wrap">
		<div class="fs-page-wrap">
			<h1><?php _e( 'Join Flexible Shipping Connect Now!', 'flexible-shipping' ); ?></h1>

			<div class="fs-box">
				<form id="flexible_shipping_signup_form" action="<?php echo $action; ?>" method="POST">
					<?php wp_nonce_field( WPDesk_Flexible_Shipping_SaaS_User_Registration::NONCE_REGISTRATION_ACTION, WPDesk_Flexible_Shipping_SaaS_User_Registration::NONCE_REGISTRATION_NAME ); ?>

					<h3><?php printf( __( 'Sign up for automatic shipments, label printing, live rates and more!', 'flexible-shipping' ), '<a href="' . $url_fs_com . '" target="_blank">', '</a>' ); ?></h3>

					<p><small><?php printf( __( 'By signing up, you agree to the %1$sTerms of Service%3$s and to %2$sPrivacy Policy%3$s', 'flexible-shipping' ), '<a href="' . $url_terms . '" target="_blank">', '<a href="' . $url_privacy . '" target="_blank">', '</a>' ); ?></small></p>

					<p><input class="regular-text" type="email" required="required" name="user_email" value="<?php echo $user_email; ?>" /></p>

					<p class="fs-actions">
						<button id="flexible_shipping_signup" class="button button-primary button-large" type="submit"><?php _e( 'Sign Up', 'flexible-shipping' ); ?></button>

						<span class="spinner"></span>
					</p>

				</form>
			</div>

			<?php
				$button_class = '';
				$button_link  = '#register';
				include( 'html-boxes-content.php' );
			?>
		</div>
	</div>

	<script type="text/javascript">
		jQuery('#flexible_shipping_signup_form').on('submit', function(e){
			jQuery('#flexible_shipping_signup').prop('disabled', 'disabled');
			jQuery('.spinner').css('visibility', 'visible');
		});
	</script>
<?php endif; ?>
