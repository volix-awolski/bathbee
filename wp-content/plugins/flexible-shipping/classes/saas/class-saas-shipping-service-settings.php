<?php

/**
 * Integration settings in WooCommerce settings tab.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Method
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings extends WC_Shipping_Method implements WPDesk_Flexible_Shipping_Saas_Connection_Aware {

	const OPTION_SHIPPING_METHOD_TYPE = 'fs_method_instance_type_';

	/**
	 * Settings already saved.
	 *
	 * @var bool
	 */
	private static $settings_saved = false;

	/**
	 * Shipping methods manager.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Services_Manager
	 */
	private $shipping_methods_manager;

	/**
	 * Saas connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection = null;

	/**
	 * Shipping method has settings in SaaS?
	 *
	 * @var bool
	 */
	private $has_saas_settings = false;

	/**
	 * Shipping service id.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Save settings error.
	 *
	 * @var bool
	 */
	private $save_settings_error = false;

	/**
	 * WPDesk_Flexible_Shipping_Connect constructor.
	 *
	 * @param int                                            $instance_id Instance id.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 */
	public function __construct(
		$instance_id = 0,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service = null
	) {
		parent::__construct( $instance_id );

		if ( ! empty( $shipping_service ) ) {
			$this->shipping_service = $shipping_service;

			$this->id                 = $this->shipping_service->get_integration_id();
			$this->method_title       = $shipping_service->get_name();
			$this->title              = $shipping_service->get_name();
			$this->method_description = $shipping_service->get_description();

			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

		} else {
			$this->title            = $this->get_option( 'title' );
			$parent_shipping_method = $this->get_parent_method();
			if ( ! empty( $parent_shipping_method ) ) {
				$this->shipping_service   = $parent_shipping_method->get_shipping_service();
				$this->method_description = $parent_shipping_method->method_description;
				$this->id                 = $this->shipping_service->get_integration_id();

				$this->instance_form_fields['title']['default'] = $parent_shipping_method->method_title;
				$this->method_title                             = $parent_shipping_method->method_title;
			}
		}

		$this->enabled = 'no';

		$this->supports = array(
			'settings',
			'flexible-shipping',
		);

		$this->init();

	}

	/**
	 * Set shipping methods manager.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Services_Manager $shipping_methods_manager Shipping methods manager.
	 */
	public function set_shipping_methods_manager( $shipping_methods_manager ) {
		$this->shipping_methods_manager = $shipping_methods_manager;
	}

	/**
	 * Set SaaS Connection.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection SaaS connection.
	 */
	public function set_saas_connection( $saas_connection ) {
		$this->saas_connection = $saas_connection;
		$this->init();
	}

	/**
	 * Get has saas settings.
	 *
	 * @return bool
	 */
	public function get_has_saas_settings() {
		return $this->has_saas_settings;
	}

	/**
	 * Display admin error messages.
	 */
	public function display_errors() {
		if ( $this->get_errors() ) {
			echo '<div id="notice-error" class="error notice is-dismissible">';
			foreach ( $this->get_errors() as $error ) {
				echo '<p>' . wp_kses_post( $error ) . '</p>';
			}
			echo '</div>';
		}
	}

	/**
	 * Generate settings HTML.
	 *
	 * @param array $form_fields Form fields.
	 * @param bool  $echo Echo.
	 *
	 * @return string
	 * @throws RuntimeException Exception.
	 */
	public function generate_settings_html( $form_fields = array(), $echo = true ) {
		if ( 0 === $this->instance_id ) {
			$this->init_settings();
			if ( ! $this->save_settings_error ) {
				try {
					$this->saas_connection->clear_saas_client_cache();
					$platform                  = $this->saas_connection->get_platform();
					$shipping_service_settings = $platform->requestGetSettings( $this->get_shipping_service_id() );
					if ( $shipping_service_settings->isNotExists() ) {
						$this->settings = array( '' => '' );
					} elseif ( $shipping_service_settings->isError() ) {
						throw new RuntimeException( $shipping_service_settings->getResponseCode() );
					} else {
						$this->settings      = $this->cast_settings_types( $shipping_service_settings->getSetting()->getJsonValue() );
						$country_state_field = WPDesk_Flexible_Shipping_SaaS_Country_State_Field::create_from_settings( $this->settings );

						$this->settings[ WPDesk_Flexible_Shipping_SaaS_Country_State_Field::COUNTRY_STATE_CODE_FIELD ] = $country_state_field->get_as_single_field();

						$this->has_saas_settings = true;
					}
				} catch ( Exception $e ) {
					$this->saas_connection->check_connection_and_show_notice_on_error();
					add_action( 'admin_notices', [ $this, 'hide_save_changes_button_on_connection_error' ] );
					return '';
				}
			}
		}
		return parent::generate_settings_html( $form_fields, $echo );
	}

	/**
	 * Admin notices.
	 */
	public function hide_save_changes_button_on_connection_error() {
		?>
		<style>
			p.submit {
				display: none;
			}
		</style>
		<?php
	}

	/**
	 * Get not empty array.
	 *
	 * @return array
	 */
	private function create_not_empty_array() {
		return array( '' );
	}

	/**
	 * Admin notices.
	 */
	public function admin_notices() {
		?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#message').remove();
            })
        </script>
        <style>
            #message {
                display: none;
            }
        </style>
		<?php
	}

	/**
	 * Save settings fields to platform.
	 *
	 * @param array $settings Settings.
	 *
	 * @return mixed
	 */
	private function save_settings_fields_to_platform( array $settings ) {
		$this->saas_connection->clear_saas_client_cache();
		$platform = $this->saas_connection->get_platform();

		$settings_for_platform = $this->shipping_service->prepare_settings_for_platform( $settings );

		if ( getenv( 'SAAS_FLEXIBLESHIPPING_URL' ) ) {
			$settings_for_platform['production'] = false;
		}
		if ( defined( 'SAAS_FLEXIBLESHIPPING_URL' ) ) {
			$settings_for_platform['production'] = false;
		}

		$shipping_service_settings = new \WPDesk\SaasPlatformClient\Model\ShippingServiceSetting();
		$shipping_service_settings->setJsonValue( $this->shipping_service->cast_raw_service_settings_to_valid_types( $settings_for_platform ) );
		$shipping_service_settings->setShippingService( $this->shipping_service->get_id() );
		$response = $platform->requestSaveSettings( $shipping_service_settings );

		if ( $response->isBadRequest() ) {
			add_action( 'admin_notices', array( $this, 'admin_notices' ) );
			$error_message = __( 'Invalid format - contact with flexibleshipping.com administrator.', 'flexible-shipping' );
			new \WPDesk\Notice\Notice( $error_message, 'error' );
			$this->save_settings_error = true;
		} elseif ( $response->isError() ) {
			add_action( 'admin_notices', array( $this, 'admin_notices' ) );
			$error_message = sprintf(
				// Translators: response code.
				__( 'Ups, something is wrong. Error code: %1$s.', 'flexible-shipping' ),
				$response->getResponseCode()
			);
			new \WPDesk\Notice\Notice( $error_message, 'error' );
			$this->save_settings_error = true;
		}
		return $settings;
	}

	/**
	 * Process admin options.
	 *
	 * @return bool
	 */
	public function process_admin_options() {
		$process_admin_options_return = parent::process_admin_options();
		if ( 0 === $this->instance_id ) {
			if ( false === self::$settings_saved ) {
				self::$settings_saved = true;
				$this->save_settings_fields_to_platform( $this->settings );
			}
		}

		return $process_admin_options_return;
	}


	/**
	 * Get shipping service id.
	 *
	 * @return int
	 */
	public function get_shipping_service_id() {
		return $this->shipping_service->get_id();
	}

	/**
	 * Get shipping service.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	public function get_shipping_service() {
		return $this->shipping_service;
	}

	/**
	 * Init user set variables.
	 */
	public function init() {
		$this->init_form_fields();
	}

	/**
	 * Get parent method instance id.
	 *
	 * @return bool|string
	 */
	private function get_parent_method_type() {
		return get_option( self::OPTION_SHIPPING_METHOD_TYPE . $this->instance_id, false );
	}

	/**
	 * Get parent method.
	 *
	 * @return null|WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings
	 */
	private function get_parent_method() {
		$shipping_methods = WC()->shipping()->get_shipping_methods();
		/**
		 * IDE type hint.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings $shipping_method
		 */
		$shipping_method = null;
		if ( isset( $shipping_methods[ $this->get_parent_method_type() ] ) ) {
			$shipping_method = $shipping_methods[ $this->get_parent_method_type() ];
		}
		return $shipping_method;
	}

	/**
	 * In settings screen?
	 *
	 * @return bool
	 */
	public function is_in_settings() {
		if ( function_exists( 'get_current_screen' ) ) {
			$current_screen = get_current_screen();
			if ( isset( $current_screen ) ) {
				$section_parameter = 'section';
				if ( 'woocommerce_page_wc-settings' === $current_screen->id
				     && isset( $_GET[ $section_parameter ] )
				     && $this->id === $_GET[ $section_parameter ]
				) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Init form fields.
	 */
	public function init_form_fields() {
		if ( ! empty( $this->shipping_service ) ) {
			$this->form_fields = $this->shipping_service->prepare_shipping_method_settings_fields();
		}
	}

	/**
	 * Generate Connection Status HTML.
	 *
	 * @param string $key Field key.
	 * @param array  $data Field data.

	 * @return string
	 */
	public function generate_connection_status_html( $key, $data ) {
		$status_field = new WPDesk_Flexible_Shipping_SaaS_Connection_Status_Field( $this );
		return $status_field->generate_html( $key, $data );
	}

	/**
	 * Prepare country state options.
	 *
	 * @return array
	 */
	private function prepare_country_state_options() {
		$countries = WC()->countries->get_countries();
		$options   = array();
		foreach ( $countries as $country_key => $country ) {
			$states = WC()->countries->get_states( $country_key );
			if ( $states ) {
				foreach ( $states as $state_key => $state ) {
					$options[ $country_key . ':' . $state_key ] = $country . ' &mdash; ' . $state;
				}
			} else {
				$options[ $country_key ] = $country;
			}
		}
		return $options;
	}

	/**
	 * Generate select country state HTML.
	 *
	 * @param string $key Field key.
	 * @param array  $data Field data.
	 *
	 * @return string
	 */
	public function generate_select_country_state_html( $key, array $data ) {
		$data['options'] = $this->prepare_country_state_options();
		$class           = isset( $data['class'] ) ? $data['class'] . ' ' : '';
		$class          .= 'fs_select2';
		$data['class']   = $class;
		return $this->generate_select_html( $key, $data );
	}

	/**
	 * Cast settings types.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	private function cast_settings_types( $settings ) {
		foreach ( $settings as $key => $value ) {
			if ( is_bool( $value ) ) {
				if ( $value ) {
					$settings[ $key ] = 'yes';
				} else {
					$settings[ $key ] = 'no';
				}
			}
		}
		return $settings;
	}

	/**
	 * Is connected.
	 *
	 * @return bool
	 */
	private function is_connected() {
		return $this->saas_connection->is_connected();
	}

}

