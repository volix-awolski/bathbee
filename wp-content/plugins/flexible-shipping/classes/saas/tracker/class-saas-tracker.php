<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings
 */
class WPDesk_Flexible_Shipping_SaaS_Tracker implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const PRIORITY_AFTER_DEFAULT = 11;

	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Services manager.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Services_Manager $services_manager
	 */
	private $services_manager;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Tracker constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS connection.
	 * @param WPDesk_Flexible_Shipping_SaaS_Services_Manager $services_manager Services manager.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection,
		WPDesk_Flexible_Shipping_SaaS_Services_Manager $services_manager
	) {
		$this->saas_connection  = $saas_connection;
		$this->services_manager = $services_manager;
	}

	/**
	 * Hooks
	 */
	public function hooks() {
		// @TODO: not now, probably to remove: https://trello.com/c/xv9Rm0YG/2804-trackowanie-i-analiza-danych
		//add_filter( 'wpdesk_tracker_data', array( $this, 'collect_tracker_data' ), self::PRIORITY_AFTER_DEFAULT );
	}

	/**
	 * Collect tracked data.
	 *
	 * @param array $tracked_data Tracked data.
	 * @return array
	 */
	public function collect_tracker_data( array $tracked_data ) {
		$saas_data = array();
		if ( $this->saas_connection->is_connected() ) {
			$sass_data['connected'] = '1';
			$services_data          = $this->collect_sass_services_data();
			$saas_data['services']  = $services_data;
			$saas_data['summaries'] = $this->compute_summaries( $services_data );
		} else {
			$sass_data['connected'] = '0';
		}
		$tracked_data['flexible-shipping-saas'] = $saas_data;
		return $tracked_data;
	}

	/**
	 * Min first shipment.
	 *
	 * @param array $array1 Array 1.
	 * @param array $array2 Array 2.
	 *
	 * @return mixed
	 */
	private function min_first_shipment_from_arrays( array $array1, array $array2 ) {
		if ( ! isset( $array1[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ] ) ) {
			return $array2[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ];
		} else {
			return min(
				$array1[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ],
				$array2[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ]
			);
		}
	}

	/**
	 * Max last shipment.
	 *
	 * @param array $array1 Array 1.
	 * @param array $array2 Array 2.
	 *
	 * @return mixed
	 */
	private function max_last_shipment_from_arrays( array $array1, array $array2 ) {
		if ( ! isset( $array1[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ] ) ) {
			return $array2[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ];
		} else {
			return max(
				$array1[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ],
				$array2[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ]
			);
		}
	}

	/**
	 * Compute summaries.
	 *
	 * @param array $services_data Services data.
	 * @return array
	 */
	private function compute_summaries( array $services_data ) {
		$summaries = array(
			WPDesk_Flexible_Shipping_Shipment_Counter::ALL_SHIPMENTS => 0,
			WPDesk_Flexible_Shipping_Shipment_Counter::STATUS        => array(),
		);
		foreach ( $services_data as $service_data ) {
			$summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::ALL_SHIPMENTS ]
				+= $service_data[ WPDesk_Flexible_Shipping_Shipment_Counter::ALL_SHIPMENTS ];

			$summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ] =
				$this->min_first_shipment_from_arrays( $summaries, $service_data );

			$summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ] =
				$this->max_last_shipment_from_arrays( $summaries, $service_data );

			foreach ( $services_data[ WPDesk_Flexible_Shipping_Shipment_Counter::STATUS ] as $status => $status_counters ) {
				if ( ! isset( $summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::STATUS ][ $status ] ) ) {
					$summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::STATUS ][ $status ] = $status_counters;
				} else {
					$summary_status_counters = $summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::STATUS ][ $status ];

					$summary_status_counters[ WPDesk_Flexible_Shipping_Shipment_Counter::ALL_SHIPMENTS ]
						+= $status_counters[ WPDesk_Flexible_Shipping_Shipment_Counter::ALL_SHIPMENTS ];

					$summary_status_counters[ WPDesk_Flexible_Shipping_Shipment_Counter::FIRST_SHIPMENT ] =
						$this->min_first_shipment_from_arrays( $summary_status_counters, $status_counters );

					$summary_status_counters[ WPDesk_Flexible_Shipping_Shipment_Counter::LAST_SHIPMENT ] =
						$this->max_last_shipment_from_arrays( $summary_status_counters, $status_counters );

					$summaries[ WPDesk_Flexible_Shipping_Shipment_Counter::STATUS ][ $status ] = $summary_status_counters;
				}
			}
		}
		return $summaries;
	}

	/**
	 * Collect tracker SaaS data.
	 *
	 * @return array
	 */
	private function collect_sass_services_data() {
		$services_data = array();
		$all_services  = $this->services_manager->get_all_services();
		foreach ( $all_services as $service_id => $saas_service ) {
			$service = $this->services_manager->get_saas_shiping_service( $service_id );
			if ( ! empty( $service ) ) {
				$services_data[ $service->get_integration_id() ] = $this->collect_saas_service_data( $service );
			}
		}
		return $services_data;
	}

	/**
	 * Collect service data.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $service Service.
	 *
	 * @return array
	 */
	private function collect_saas_service_data( $service ) {
		$service_data   = array();
		$service_id     = $service->get_id();
		$integration_id = $service->get_integration_id();
		if ( $this->services_manager->is_enabled_saas_service( $service_id ) ) {
			$enabled = '1';
		} else {
			$enabled = '0';
		}
		$service_data['enabled'] = $enabled;

		global $wpdb;
		$shipment_counter = new WPDesk_Flexible_Shipping_Shipment_Counter( $integration_id, $wpdb );

		$service_data['counters'] = $shipment_counter->count_shipments();

		return $service_data;
	}

}
