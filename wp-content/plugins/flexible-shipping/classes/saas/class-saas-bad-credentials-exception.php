<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception
 */
class WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception extends RuntimeException {


	/**
	 * WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Platform links.
	 */
	public function __construct( WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links ) {
		$notice_content = sprintf(
			/* Translators: url */
			__( 'The Connect Key that you entered is invalid. Please enter a valid key. %1$sGet your Connect Key →%2$s', 'flexible-shipping' ),
			'<a href="' . $saas_platform_links->get_base_url() . '">',
			'</a>'
		);
		parent::__construct( $notice_content );
	}
}
