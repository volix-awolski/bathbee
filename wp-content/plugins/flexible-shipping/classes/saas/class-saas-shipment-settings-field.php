<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Shipment_Settings_Field
 */
class WPDesk_Flexible_Shipping_SaaS_Shipment_Settings_Field {

	/**
	 * Saas field.
	 *
	 * @var array
	 */
	private $saas_field;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Settings_Field constructor.
	 *
	 * @param array $saas_field SaaS field.
	 */
	public function __construct( array $saas_field ) {
		$this->saas_field = $saas_field;
	}

	/**
	 * Add fields to settings.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	public function add_fields_to_settings( array $settings ) {
		$order_statuses = wc_get_order_statuses();

		$field_id = $this->saas_field['id'];

		$settings[ $field_id . '_auto_create' ]    = array(
			'title'       => __( 'Create shipments', 'flexible-shipping' ),
			'type'        => 'select',
			'class'       => 'fs-settings-shipments',
			'default'     => 'manual',
			'options'     => array(
				'manual' => __( 'Manually', 'flexible-shipping' ),
				'auto'   => __( 'Automatically', 'flexible-shipping' ),
			),
			'description' => __( 'Choose to create shipments manually or automatically based on the order status.', 'flexible-shipping' ),
			'desc_tip'    => true,
		);
		$settings[ $field_id . '_order_status' ]   = array(
			'title'       => __( 'Order status', 'flexible-shipping' ),
			'type'        => 'select',
			'class'       => 'fs-settings-shipments',
			'default'     => 'wc-completed',
			'options'     => $order_statuses,
			'description' => __( 'Select order status for automatic shipment creation.', 'flexible-shipping' ),
			'desc_tip'    => true,
		);
		$settings[ $field_id . '_complete_order' ] = array(
			'title'       => __( 'Complete order', 'flexible-shipping' ),
			'type'        => 'checkbox',
			'class'       => 'fs-settings-shipments',
			'label'       => __( 'Enable automatic order status change', 'flexible-shipping' ),
			'default'     => 'no',
			'description' => __( 'Automatically change order status to completed after creating shipment.', 'flexible-shipping' ),
			'desc_tip'    => true,
		);

		return $settings;
	}

	/**
	 * Add settings for platform.
	 *
	 * @param array $settings Settings.
	 * @param array $settings_for_platform Settings for platform.
	 *
	 * @return array
	 */
	public function add_settings_for_platform( array $settings, array $settings_for_platform ) {
		$field_id = $this->saas_field['id'];

		$field_key                           = $field_id . '_auto_create';
		$settings_for_platform[ $field_key ] = $settings[ $field_key ];

		$field_key                           = $field_id . '_order_status';
		$settings_for_platform[ $field_key ] = $settings[ $field_key ];

		$field_key                           = $field_id . '_complete_order';
		$settings_for_platform[ $field_key ] = $settings[ $field_key ];

		return $settings_for_platform;
	}

}
