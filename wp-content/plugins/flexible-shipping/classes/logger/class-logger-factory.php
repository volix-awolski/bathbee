<?php

/**
 * Class WPDesk_Flexible_Shipping_Logger_Factory
 */
class WPDesk_Flexible_Shipping_Logger_Factory {

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private static $logger;

	/**
	 * Create logger.
	 *
	 * @return \Psr\Log\LoggerInterface
	 */
	public static function create_logger() {
		if ( null === self::$logger ) {
			$logger_settings = new WPDesk_Flexible_Shipping_SaaS_Logger_Settings();

			self::$logger = new WPDesk_Flexible_Shipping_WooCommerce_Context_Logger(
				@\WPDesk\Logger\LoggerFacade::get_logger( $logger_settings->get_logger_channel_name() ),
				$logger_settings->get_logging_context()
			);

			if ( ! $logger_settings->is_enabled() ) {
				\WPDesk\Logger\LoggerFacade::set_disable_log( $logger_settings->get_logger_channel_name() );
			}
		}
		return self::$logger;
	}

}
