<?php

namespace WPDesk\SaasPlatformClient\Response\Fields;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class GetFieldsResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->getResponseBody()['fieldsJson'];
    }
}
