<?php

namespace WPDesk\SaasPlatformClient\Response\ShippingServicesSettings;

use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class GetSettingsResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @param array $data
     * @return array
     */
    private function convertHalReferencesToJson(array $data)
    {
        foreach ($data['_links'] as $key => $item) {
            $data[$key] = $item['href'];
        }
        return $data;
    }

    /**
     * @return ShippingServiceSetting
     */
    public function getSetting()
    {
        return new ShippingServiceSetting($this->convertHalReferencesToJson($this->getResponseBody()));
    }
}
