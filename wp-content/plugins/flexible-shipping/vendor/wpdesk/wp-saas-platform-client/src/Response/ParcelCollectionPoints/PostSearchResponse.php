<?php

namespace WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SearchResponse;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostSearchResponse implements ApiResponse
{

    use AuthApiResponseDecorator;

    const RESPONSE_CODE_COLLECTION_POINT_FAILED = 476;

    /**
     * @return SearchResponse
     */
    public function getPoints()
    {
        return new SearchResponse($this->getResponseBody());
    }

    /**
     * Platform can't respond in meaningful way about rates
     *
     * @return bool
     */
    public function isCollectionPointError()
    {
        return $this->rawResponse->getResponseCode() === self::RESPONSE_CODE_COLLECTION_POINT_FAILED;
    }


}