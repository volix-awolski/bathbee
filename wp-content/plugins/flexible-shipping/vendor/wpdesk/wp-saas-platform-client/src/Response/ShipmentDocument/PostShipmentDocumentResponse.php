<?php

namespace WPDesk\SaasPlatformClient\Response\ShipmentDocument;

use WPDesk\SaasPlatformClient\Model\ShipmentDocument\ShipmentDocument;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostShipmentDocumentResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return ShipmentDocument
     */
    public function getShipmentDocument()
    {
        return new ShipmentDocument($this->getResponseBody());
    }

}