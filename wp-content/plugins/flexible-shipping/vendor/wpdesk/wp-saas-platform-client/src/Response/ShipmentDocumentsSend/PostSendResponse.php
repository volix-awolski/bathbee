<?php

namespace WPDesk\SaasPlatformClient\Response\ShipmentDocumentsSend;

use WPDesk\SaasPlatformClient\Model\ShipmentDocument\SendDocumentsResponse;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostSendResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return SendDocumentsResponse
     */
    public function getDocumentStatusesResponse()
    {
        return new SendDocumentsResponse($this->getResponseBody());
    }

}