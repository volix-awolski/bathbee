<?php

namespace WPDesk\SaasPlatformClient\Response\Users;

use WPDesk\SaasPlatformClient\Model\User;
use WPDesk\SaasPlatformClient\Response\Response;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

final class GetUserResponse implements Response
{
    use AuthApiResponseDecorator;

    /**
     * @return User
     */
    public function getUser()
    {
        return new User($this->getResponseBody());
    }
}