<?php

namespace WPDesk\SaasPlatformClient\Response\Authentication;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\ApiResponseDecorator;

final class ConnectKeyInfoResponse implements ApiResponse
{
    use ApiResponseDecorator;

    /**
     * @return int
     */
    public function getUserId()
    {
        return (int)$this->getResponseBody()['owner_id'];
    }

    /**
     * @return \DateTimeImmutable
     * @throws \Exception
     */
    public function getLastUsed()
    {
        $lastUsed = $this->getResponseBody()['lastUsed'];
        return new \DateTimeImmutable($lastUsed['date']);
    }

    /**
     * @return array[string]
     */
    public function getDomains()
    {
        return $this->getResponseBody()['domains'];
    }
}