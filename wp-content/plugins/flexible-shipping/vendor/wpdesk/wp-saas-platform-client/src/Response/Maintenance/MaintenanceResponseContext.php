<?php

namespace WPDesk\SaasPlatformClient\Response\Maintenance;

use WPDesk\SaasPlatformClient\Response\Response;

class MaintenanceResponseContext
{

    /**
     * Response.
     *
     * @var Response
     */
    private $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * Get maintenance message.
     *
     * @return string|null
     */
    public function getMaintenanceMessage()
    {
        $responseBody = $this->response->getResponseBody();
        if (isset($responseBody['message'])) {
            return $responseBody['message'];
        }
        return null;
    }

    /**
     * Get maintenance till.
     *
     * @return int|null
     */
    public function getMaintenanceTill()
    {
        $responseBody = $this->response->getResponseBody();
        if (isset($responseBody['maintenance_till'])) {
            return intval($responseBody['maintenance_till']);
        }
        return null;
    }

}