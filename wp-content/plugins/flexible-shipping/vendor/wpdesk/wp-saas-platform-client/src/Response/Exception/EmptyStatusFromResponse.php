<?php

namespace WPDesk\SaasPlatformClient\Response\Exception;

class EmptyStatusFromResponse extends \RuntimeException
{
    /**
     * Exception factory
     *
     * @param string $request
     * @param int $errorCode
     * @return TriedExtractDataFromErrorResponse
     */
    public static function createWithClassInfo($request, $errorCode)
    {
        return new EmptyStatusFromResponse("Tried to extract status from {$request} when an error occured. Response code: {$errorCode}",
            $errorCode);
    }
}