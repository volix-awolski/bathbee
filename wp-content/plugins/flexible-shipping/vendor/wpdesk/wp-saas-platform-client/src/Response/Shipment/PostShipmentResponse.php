<?php

namespace WPDesk\SaasPlatformClient\Response\Shipment;

use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentResponse;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostShipmentResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    const RESPONSE_CODE_SHIPMENT_PLAN_EXCEEDED = 472;

    /**
     * @return bool
     */
    public function isShipmentPlanExceeded()
    {
        return $this->getResponseCode() === self::RESPONSE_CODE_SHIPMENT_PLAN_EXCEEDED;
    }

    /**
     * @return ShipmentResponse
     */
    public function getShipment()
    {
        return new ShipmentResponse($this->getResponseBody());
    }
}
