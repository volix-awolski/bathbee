<?php

namespace WPDesk\SaasPlatformClient\Response;

use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class AuthApiResponse implements ApiResponse
{
    const RESPONSE_CODE_BAD_CREDENTIALS = 401;

    const RESPONSE_CODE_NOT_EXISTS = 404;

    use AuthApiResponseDecorator;
}