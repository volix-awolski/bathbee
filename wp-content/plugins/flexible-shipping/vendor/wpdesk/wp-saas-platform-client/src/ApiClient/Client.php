<?php

namespace WPDesk\SaasPlatformClient\ApiClient;

use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\Request\Request;
use WPDesk\SaasPlatformClient\Response\Response;
use WPDesk\SaasPlatformClient\Serializer\Serializer;

interface Client
{
    /**
     * Send given request trough HttpClient
     *
     * @param Request $request
     * @return Response
     */
    public function sendRequest(Request $request);

    /**
     * @return HttpClient
     */
    public function getHttpClient();

    /**
     * @param HttpClient $client
     */
    public function setHttpClient(HttpClient $client);

    /**
     * @return Serializer
     */
    public function getSerializer();

    /**
     * Returns api url. Always without ending /
     *
     * @return string
     */
    public function getApiUrl();
}