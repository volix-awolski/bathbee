<?php

namespace WPDesk\SaasPlatformClient\ApiClient;

use WPDesk\SaasPlatformClient\Cache\WordpressCache;
use WPDesk\SaasPlatformClient\HttpClient\HttpClientFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;
use WPDesk\SaasPlatformClient\Serializer\SerializerFactory;

class ClientFactory
{
    /**
     * @param PlatformFactoryOptions $options
     * @return Client
     */
    public function createClient(PlatformFactoryOptions $options)
    {
        $httpClientFactory = new HttpClientFactory();
        $serializerFactory = new SerializerFactory();

        $client = new ClientImplementation(
            $httpClientFactory->createClient($options),
            $serializerFactory->createSerializer($options),
            $options->getLogger(),
            $options->getApiUrl(),
            $options->getDefaultRequestHeaders()
        );

        if ($options->isCachedClient()) {
            $client = new CachedClient($client, new WordpressCache());
        }

        return $client;
    }
}