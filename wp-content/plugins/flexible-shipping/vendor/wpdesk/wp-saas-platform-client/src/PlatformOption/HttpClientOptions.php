<?php

namespace WPDesk\SaasPlatformClient\PlatformOption;

interface HttpClientOptions
{
    /**
     * @return string
     */
    public function getHttpClientClass();
}