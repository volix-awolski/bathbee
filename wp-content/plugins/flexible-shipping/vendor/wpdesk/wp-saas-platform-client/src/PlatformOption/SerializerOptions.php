<?php

namespace WPDesk\SaasPlatformClient\PlatformOption;

interface SerializerOptions
{
    /**
     * @return string
     */
    public function getSerializerClass();
}