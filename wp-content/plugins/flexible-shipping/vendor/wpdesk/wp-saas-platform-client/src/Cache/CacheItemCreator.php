<?php

namespace WPDesk\SaasPlatformClient\Cache;

/**
 * Cache item creator.
 *
 * Interface CacheItemCreator
 * @package WPDesk\SaasPlatformClient\Cache
 */
interface CacheItemCreator
{

    /**
     * Create item to cache.
     *
     * @param object $object
     * @return mixed
     */
    public function createCacheItem($object);

}