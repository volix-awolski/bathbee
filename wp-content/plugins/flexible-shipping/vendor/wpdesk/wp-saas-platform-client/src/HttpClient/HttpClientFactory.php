<?php

namespace WPDesk\SaasPlatformClient\HttpClient;


use WPDesk\SaasPlatformClient\PlatformOption\HttpClientOptions;

class HttpClientFactory
{
    /**
     * @param HttpClientOptions $options
     * @return HttpClient
     */
    public function createClient(HttpClientOptions $options)
    {
        $className = $options->getHttpClientClass();
        return new $className;
    }
}