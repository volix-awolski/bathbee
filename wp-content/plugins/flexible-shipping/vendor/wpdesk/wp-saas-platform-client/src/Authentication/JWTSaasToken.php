<?php

namespace WPDesk\SaasPlatformClient\Authentication;

class JWTSaasToken implements Token
{
    const SHOP_ID_PARAM = 'shop';
    const ROLES_PARAM = 'roles';

    const SHOP_ROLE = 'ROLE_SHOP';


    /** @var Token */
    private $token;

    /**
     * @param Token $token
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    public function getAuthString()
    {
        return $this->token->getAuthString();
    }

    public function isExpired()
    {
        return $this->token->isExpired();
    }

    public function isSignatureValid()
    {
        return $this->token->isSignatureValid();
    }

    public function __toString()
    {
        return $this->token->__toString();
    }

    /**
     * If there is shop id in the token
     *
     * @return bool
     */
    public function hasShopId()
    {
        if ($this->token instanceof JWTToken) {
            $info = $this->token->getDecodedPublicTokenInfo();
            return !empty($info[self::SHOP_ID_PARAM]) && !empty($info[self::ROLES_PARAM]) &&
                in_array(self::SHOP_ROLE, $info[self::ROLES_PARAM], true);
        }
        return false;

    }

    /**
     * Get shop id from token
     *
     * @return int
     */
    public function getShopId()
    {
        if ($this->token instanceof JWTToken && $this->hasShopId()) {
            $info = $this->token->getDecodedPublicTokenInfo();
            return (int)$info[self::SHOP_ID_PARAM];
        }
        return 0;
    }
}