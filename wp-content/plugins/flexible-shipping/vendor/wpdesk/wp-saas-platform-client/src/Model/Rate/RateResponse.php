<?php

namespace WPDesk\SaasPlatformClient\Model\Rate;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class RateResponse extends AbstractModel
{
    /**
     * @var SingleRate[]
     */
    public $rates = [];

    public $messageStack = [];

    /**
     * From array.
     *
     * @param array $data Data.
     */
    public function fromArray(array $data)
    {
        if ($data['rates'] && count($data['rates']) > 0) {
            foreach ($data['rates'] as $rate) {
                $this->rates[] = new SingleRate($rate);
            }
        }
        if (isset($data['messageStack'])) {
            $this->messageStack = $data['messageStack'];
        }
    }
}