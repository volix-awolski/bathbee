<?php

namespace WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\Shipment\Address;

class ClientCollectionPoint extends AbstractModel
{

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var Address
     */
    public $address;

    /**
     * @var ClientMapLocation
     */
    public $mapLocation;

    /**
     * ClientCollectionPoint constructor.
     *
     * @param array|null $data
     */
    public function __construct(array $data = null)
    {
        $this->address = new Address();
        $this->mapLocation = new ClientMapLocation();

        parent::__construct($data);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return ClientMapLocation
     */
    public function getMapLocation()
    {
        return $this->mapLocation;
    }

}