<?php

namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\Shipment\Actor;
use WPDesk\SaasPlatformClient\Model\Shipment\Package;

/**
 *
 * @package App\Dto
 */
class ShipmentRequest extends AbstractModel
{
    /**
     * @Constraints\NotBlank
     * @Constraints\Valid
     *
     * @var Actor
     */
    protected $shipTo;

    /**
     * @Constraints\NotBlank
     * @Constraints\Valid
     *
     * @var Package[]
     */
    protected $packages;

    /**
     * Full information about the goods sent
     *
     * @Constraints\Valid
     *
     * @var ClientCommodity[]
     */
    public $commodities;

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $serviceType;

    /**
     * @var array
     */
    public $additionalCustomFields;

    /**
     * @var string
     */
    public $referenceNumber;

    /**
     * @var ShopData
     */
    protected $shopData;

    /**
     * @var ShopFlow
     */
    protected $shopFlow;

    /**
     * Id of selected collection point
     *
     * @var string
     */
    protected $collectionPointId;

    /**
     * @param \WPDesk\SaasPlatformClient\Model\Shipment\Actor $shipTo
     */
    public function setShipTo($shipTo)
    {
        $this->shipTo = $shipTo;
    }

    /**
     * @param Package[] $packages
     */
    public function setPackages($packages)
    {
        $this->packages = $packages;
    }

    /**
     * @param ClientCommodity[] $commodities
     */
    public function setCommodities($commodities)
    {
        $this->commodities = $commodities;
    }

    /**
     * @param string $serviceType
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;
    }

    /**
     * @return \WPDesk\SaasPlatformClient\Model\Shipment\Actor
     */
    public function getShipTo()
    {
        return $this->shipTo;
    }

    /**
     * @return Package[]
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @return string
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * @return array
     */
    public function getAdditionalCustomFields()
    {
        return $this->additionalCustomFields;
    }

    /**
     * @param array $additionalCustomFields
     */
    public function setAdditionalCustomFields($additionalCustomFields)
    {
        $this->additionalCustomFields = $additionalCustomFields;
    }

    /**
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param string $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @param ShopData $shopData
     */
    public function setShopData($shopData)
    {
        $this->shopData = $shopData;
    }

    /**
     * @param ShopFlow $shopFlow
     */
    public function setShopFlow($shopFlow)
    {
        $this->shopFlow = $shopFlow;
    }

    /**
     * @return string
     */
    public function getCollectionPointId()
    {
        return $this->collectionPointId;
    }

    /**
     * @param string $collectionPointId
     */
    public function setCollectionPointId($collectionPointId)
    {
        $this->collectionPointId = $collectionPointId;
    }


}