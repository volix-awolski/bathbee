<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class Actor extends AbstractModel
{
    /**
     * @Constraints\Valid
     *
     * @var Address
     */
    protected $address;

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $companyName;

    /**
     * @var string
     */
    protected $phoneNumber;

    /**
     * @var string
     */
    protected $email;

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

}