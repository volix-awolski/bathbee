<?php

namespace WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class SingleRequest extends AbstractModel
{

    /**
     * @var string
     */
    public $id;

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}