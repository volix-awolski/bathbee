<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class ClientCommodity extends AbstractModel
{

    /**
     * Name of the commodity
     *
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $name;

    /**
     * Unit price
     *
     * @Constraints\NotBlank
     *
     * @var ClientMoney
     */
    protected $unitPrice;

    /**
     * Unit weight
     *
     * @Constraints\NotBlank
     *
     * @var ClientWeight
     */
    protected $unitWeight;

    /**
     * Quantity
     *
     * @Constraints\NotBlank
     *
     * @var int
     */
    protected $quantity;

    /**
     * Description ie 200 cm x 400 cm nylon carpet samples for demonstration
     * or Two steel springs for woodworking machine
     *
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $countryOfManufacture;

    /**
     * Harmonized Tariff System (HTS) codes to meet the U.S. and foreign governments' customs requirements
     *
     * @var string
     */
    protected $htsCode;

    /**
     * ClientCommodity constructor.
     *
     * @param string $name
     * @param ClientMoney $unitPrice
     * @param ClientWeight $unitWeight
     * @param int $quantity
     * @param string $description
     * @param string $countryOfManufacture
     * @param string $htsCode
     */
    public function __construct(
        $name,
        ClientMoney $unitPrice,
        ClientWeight $unitWeight,
        $quantity,
        $description,
        $countryOfManufacture,
        $htsCode
    ) {
        parent::__construct(null);

        $this->name = $name;

        $this->unitPrice = $unitPrice;

        $this->unitWeight = $unitWeight;

        $this->quantity = $quantity;

        $this->description = $description;

        $this->countryOfManufacture = $countryOfManufacture;

        $this->htsCode = $htsCode;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param ClientMoney $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @param ClientWeight $unitWeight
     */
    public function setUnitWeight($unitWeight)
    {
        $this->unitWeight = $unitWeight;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $countryOfManufacture
     */
    public function setCountryOfManufacture($countryOfManufacture)
    {
        $this->countryOfManufacture = $countryOfManufacture;
    }

    /**
     * @param string $htsCode
     */
    public function setHtsCode($htsCode)
    {
        $this->htsCode = $htsCode;
    }

}