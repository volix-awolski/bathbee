<?php

namespace WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\Shipment\Address;

class SearchRequest extends AbstractModel
{

    /**
     * @var Address
     */
    public $address;

    /**
     * @var string
     */
    public $serviceType;

    /**
     * How close to address should search for AP in KM
     *
     * @var int
     */
    public $radius;

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param string $serviceType
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
    }

}