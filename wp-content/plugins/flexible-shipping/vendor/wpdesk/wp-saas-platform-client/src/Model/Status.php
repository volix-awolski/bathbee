<?php

namespace WPDesk\SaasPlatformClient\Model;

class Status extends AbstractModel
{

    /** @var string */
    protected $serverStatus;

    /** @var string */
    protected $platformVersion;

    /** @var int */
    protected $currentTimestamp;

    /** @var string */
    protected $migrationVersion;

    /** @var string[] */
    protected $tokenInfo;

    /**
     * @return string
     */
    public function getServerStatus()
    {
        return $this->serverStatus;
    }

    /**
     * @return string
     */
    public function getPlatformVersion()
    {
        return $this->platformVersion;
    }

    /**
     * @return int
     */
    public function getCurrentTimestamp()
    {
        return $this->currentTimestamp;
    }

    /**
     * @return string
     */
    public function getMigrationVersion()
    {
        return $this->migrationVersion;
    }

    /**
     * @return string[]
     */
    public function getTokenInfo()
    {
        return $this->tokenInfo;
    }

}


