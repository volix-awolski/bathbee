<?php

namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\ShippingService;
use WPDesk\SaasPlatformClient\Model\Shop;

/**
 * Shipment as in fs.com shop context
 *
 * @package App\Dto
 */
final class ShipmentAdminContext extends AbstractModel
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $status;

    /** @var \DateTime */
    protected $createdAt;

    /** @var array  */
    protected $shop;

    /** @var array */
    protected $shippingService;

    /** @var string */
    protected $trackingNumbersAsString;

    public function __construct($data = null)
    {
        $this->shop = new Shop();
        $this->shippingService = new ShippingService();

        parent::__construct($data);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @return ShippingService
     */
    public function getShippingService()
    {
        return $this->shippingService;
    }

    /**
     * @return string
     */
    public function getTrackingNumbersAsString()
    {
        return $this->trackingNumbersAsString;
    }

    /**
     * @param string $createdAt
     */
    protected function setCreatedAt($createdAt)
    {
        $this->createdAt = new \DateTime($createdAt);
    }
}