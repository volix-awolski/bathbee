<?php

namespace WPDesk\SaasPlatformClient\Model;

class LoginData extends AbstractModel
{
    /** @var string */
    protected $connect_key;

    /** @var string */
    protected $domain;

    /** @var string */
    protected $locale;

    /**
     * @return string
     */
    public function getConnectKey()
    {
        return $this->connect_key;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
}


