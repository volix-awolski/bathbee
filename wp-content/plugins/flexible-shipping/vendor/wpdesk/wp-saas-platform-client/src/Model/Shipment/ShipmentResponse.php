<?php

namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

/**
 *
 * @package App\Dto
 */
final class ShipmentResponse extends AbstractModel
{
    /** @var int */
    protected $shipmentId;

    /** @var string */
    protected $status;

    /** @var string|null */
    protected $message;

    /** @var string */
    protected $trackingId;

    /** @var string */
    protected $trackingUrl;

    /** @var string */
    protected $shipmentCountSent;

    /** @var string */
    protected $shipmentCountLeft;

    /** @var ClientMoney|null */
    protected $shipmentCost;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->shipmentCost = new ClientMoney();
        parent::__construct($data);
    }

    /**
     * @return int
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return null|string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return $this->trackingUrl;
    }

    /**
     * @return string
     */
    public function getShipmentCountSent()
    {
        return $this->shipmentCountSent;
    }

    /**
     * @return string
     */
    public function getShipmentCountLeft()
    {
        return $this->shipmentCountLeft;
    }

    /**
     * @return bool
     */
    public function hasShipmentCost()
    {
        return $this->shipmentCost !== null;
    }

    /**
     * @return ClientMoney|null - Null when shipment cost is unknown => hasShipmentCost is false
     */
    public function getShipmentCost()
    {
        return $this->shipmentCost;
    }


}