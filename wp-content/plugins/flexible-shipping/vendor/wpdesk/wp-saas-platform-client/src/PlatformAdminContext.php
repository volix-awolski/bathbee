<?php

namespace WPDesk\SaasPlatformClient;

use WPDesk\SaasPlatformClient\Model\ShippingPlan;
use WPDesk\SaasPlatformClient\Request\Authentication\ConnectKeyInfoRequest;
use WPDesk\SaasPlatformClient\Request\Authentication\ConnectKeyResetRequest;
use WPDesk\SaasPlatformClient\Request\Shipment\GetUserListShipmentRequest;
use WPDesk\SaasPlatformClient\Request\ShippingPlan\PutShippingPlanRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PutSettingsRequest;
use WPDesk\SaasPlatformClient\Response\Authentication\ConnectKeyInfoResponse;
use WPDesk\SaasPlatformClient\Response\Authentication\ConnectKeyResetResponse;
use WPDesk\SaasPlatformClient\Response\ProtectedResponse;
use WPDesk\SaasPlatformClient\Response\Shipment\GetListShipmentAdminContextResponse;

class PlatformAdminContext extends Platform
{
    /**
     * Reset user connect key
     *
     * @param int $saas_user_id
     *
     * @return ConnectKeyResetResponse
     */
    public function requestConnectKeyReset($saas_user_id)
    {
        $connectKeyResetRequest = new ConnectKeyResetRequest($saas_user_id, $this->getToken());
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new ConnectKeyResetResponse(new ProtectedResponse($this->sendRequestWithInvalidTokenHandling($connectKeyResetRequest)));
    }

    /**
     * Reset user connect key
     *
     * @param $connectKey
     * @return ConnectKeyInfoResponse
     */
    public function requestConnectKeyInfoRequest($connectKey)
    {
        $connectKeyInfoRequest = new ConnectKeyInfoRequest($connectKey, $this->getToken());
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new ConnectKeyInfoResponse(new ProtectedResponse($this->sendRequestWithInvalidTokenHandling($connectKeyInfoRequest)));
    }

    /**
     * @param string $saas_user_id
     * @return GetListShipmentAdminContextResponse
     */
    public function requestGetListShipments($saas_user_id) {
        $listProvidersRequest = new GetUserListShipmentRequest($saas_user_id, $this->getToken());
        $response = $this->sendRequestWithInvalidTokenHandling($listProvidersRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetListShipmentAdminContextResponse(new ProtectedResponse($response));
    }

    /**
     * Save settings
     *
     * @param ShippingPlan $setting
     * @return ProtectedResponse
     */
    public function requestSavePlan(ShippingPlan $plan, $saas_user_id)
    {
        $previousPlan = $this->requestGetUser($saas_user_id)->getUser()->getShippingPlan();
        $plan->setId($previousPlan->getId());
        $putShippingPlanRequest = new PutShippingPlanRequest($this->getToken(), $plan);

        return new ProtectedResponse($this->sendRequestWithInvalidTokenHandling($putShippingPlanRequest));
    }
}