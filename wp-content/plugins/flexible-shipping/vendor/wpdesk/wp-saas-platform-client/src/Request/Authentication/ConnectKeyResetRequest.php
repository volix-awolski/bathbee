<?php

namespace WPDesk\SaasPlatformClient\Request\Authentication;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class ConnectKeyResetRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/users/{id}/connectKeyReset';


    public function __construct($user_id, Token $token)
    {
        parent::__construct($token);

        $this->endPoint = str_replace('{id}', $user_id, $this->endPoint);
    }
}