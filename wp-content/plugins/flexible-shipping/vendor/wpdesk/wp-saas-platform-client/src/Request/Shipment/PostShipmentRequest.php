<?php

namespace WPDesk\SaasPlatformClient\Request\Shipment;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class PostShipmentRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/shipments';

    /**
     * PostShipmentRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param ShipmentRequest $request
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        ShipmentRequest $request
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(['{shop}', '{service}'], [$shopId, $shippingServiceId], $this->endPoint);

        $this->data = $request->toArray();
    }
}