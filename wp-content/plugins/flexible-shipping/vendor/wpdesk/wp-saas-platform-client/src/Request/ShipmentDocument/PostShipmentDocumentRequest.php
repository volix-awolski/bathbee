<?php

namespace WPDesk\SaasPlatformClient\Request\ShipmentDocument;

use WPDesk\SaasPlatformClient\Model\ShipmentDocument\ShipmentDocumentRequest;
use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class PostShipmentDocumentRequest extends AuthRequest
{

    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/shipments/{shipment}/documents';

    /**
     * PostShipmentRequest constructor.
     * @param Token $token
     * @param int $shipmentId
     * @param int $shippingServiceId
     * @param int $shopId
     * @param ShipmentDocumentRequest $request
     */
    public function __construct(
        Token $token,
        $shipmentId,
        $shippingServiceId,
        $shopId,
        ShipmentDocumentRequest $request
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(
            ['{shop}', '{service}', '{shipment}'],
            [$shopId, $shippingServiceId, $shipmentId],
            $this->endPoint
        );

        $this->data = $request->toArray();
    }


}