<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServicesSettings;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Request\AuthRequest;
use WPDesk\SaasPlatformClient\Request\Traits\SettingsItemRequestHelper;

final class PutSettingsRequest extends AuthRequest
{
    use SettingsItemRequestHelper;

    /** @var string */
    protected $method = 'PUT';

    /** @var string */
    protected $endPoint = '/shipping_services/settings/{id}';

    /**
     * PutSettingsRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param string $settingType
     */
    public function __construct(Token $token, ShippingServiceSetting $setting)
    {
        parent::__construct($token);

        $params = $this->buildSettingsId($setting->getShippingService(), $setting->getShop(), $setting->getType());
        $this->endPoint = str_replace('{id}', $params, $this->endPoint);

        $this->data = $setting->toArray();
    }
}