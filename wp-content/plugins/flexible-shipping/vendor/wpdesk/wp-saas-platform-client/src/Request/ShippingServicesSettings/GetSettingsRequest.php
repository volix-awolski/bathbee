<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServicesSettings;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Request\AuthRequest;
use WPDesk\SaasPlatformClient\Request\Traits\SettingsItemRequestHelper;

final class GetSettingsRequest extends AuthRequest
{
    use SettingsItemRequestHelper;

    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shipping_services/settings/{id}';

    /**
     * PutSettingsRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param string $settingType
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        $settingType = ShippingServiceSetting::TYPE_CONNECTION_SETTINGS
    ) {
        parent::__construct($token);

        $params = $this->buildSettingsId($shippingServiceId, $shopId, $settingType);
        $this->endPoint = str_replace('{id}', $params, $this->endPoint);
    }
}