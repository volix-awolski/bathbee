<?php

namespace WPDesk\SaasPlatformClient\Request\ShipmentCancel;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class PostCancelRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/shipments/{shipment}/cancel';

    /**
     * PostShipmentRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param int $shipmentId
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        $shipmentId
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(
            ['{shop}', '{service}', '{shipment}'],
            [$shopId, $shippingServiceId, $shipmentId],
            $this->endPoint
        );

        $this->data = [];
    }
}