<?php

namespace WPDesk\SaasPlatformClient\Request\Users;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetUserRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/users/{id}';

    /**
     * getUserRequest constructor.
     *
     * @param $user_id
     * @param Token $token
     */
    public function __construct($user_id, Token $token)
    {
        parent::__construct($token);

        $this->endPoint = str_replace('{id}', $user_id, $this->endPoint);
    }
}