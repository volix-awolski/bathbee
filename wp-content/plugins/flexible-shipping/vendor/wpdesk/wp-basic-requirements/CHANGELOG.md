## [2.3.0] - 2019-03-25
### Fixed
- Backward compatibility

## [2.3.0] - 2019-03-25
### Added
- Factory
- Interface
### Changed
- Minor internal action renaming